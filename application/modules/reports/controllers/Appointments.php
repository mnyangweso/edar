<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Appointments extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->pbscms->_check_logged();
		$this->form_validation->set_error_delimiters('<div class="ferror">', '</div>');
		//session_start();
	}
	
	public function index()
	{
		$this->form_validation->set_rules('start_date','Start Date','trim|required');
				
		if ($this->form_validation->run() == FALSE){ 
			
			$data['title'] = "Appointment Report";
			$data['content'] = $this->load->view('single_date',$data,TRUE);
			$this->load->view('template',$data);

			
		}else{
			$sd = $this->input->post('start_date');
			$data['sd'] = $sd;

			$sql = "SELECT ROW_NUMBER() OVER (ORDER BY d.visit_date) AS rn,d.ccc_no,d.visit_date,d.dob,d.age,d.sex,d.pregnant,d.pre_art,d.tca,(select 'Yes' FROM dar c WHERE c.ccc_no = d.ccc_no and d.tca=c.visit_date ) as attended FROM dar d WHERE tca = '$sd'";
			$data['query'] = $this->db->query($sql);
			$data['sd'] = $sd;
			
			$data['title'] = "Appointment Report";
			$data['content'] = $this->load->view('appointment',$data,TRUE);
			$this->load->view('template',$data);
		}
	}
	
	
	

	
}
