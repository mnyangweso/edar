<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Moh366 extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->pbscms->_check_logged();
		$this->form_validation->set_error_delimiters('<div class="ferror">', '</div>');
		//session_start();
	}
	
	public function index()
	{
		

		$this->form_validation->set_rules('mntyr','Month Year','trim|required');
				
		if ($this->form_validation->run() == FALSE){ 
			
			$data['title'] = "MOH Daily Activity Register (366)";
			$data['content'] = $this->load->view('mnt_yr',$data,TRUE);
			$this->load->view('template',$data);

			
		}else{

			if($this->input->post('ftype') ){				
				$this->pbscms->_export($this->input->post('zz'),$this->input->post('ftype'),$this->input->post('title')." ".$this->input->post('full_date'),'landscape','a1');
			}

			$var = explode(" ",$this->input->post('mntyr'));
			$mnt = $var[0];
			$yr = $var[1];
			$siku = "01-$mnt-$yr";

			$data['full_date'] = date('F', mktime(0, 0, 0, $mnt, 10)). " ".$yr;

			$sql = "WITH mike AS (
		SELECT *,extract(YEAR FROM AGE(visit_date,dob)) AS yrs,extract (MONTH FROM AGE(tca,visit_date)) AS dur_mnths FROM dar WHERE DATE_PART('MONTH',visit_date) = $mnt  and DATE_PART('YEAR',visit_date) = $yr ORDER BY visit_date ASC 

		)SELECT ROW_NUMBER() OVER (ORDER BY ccc_no) AS rn,visit_date,visit_type,ccc_no,ccc_no AS a,
---- on ctx 
(SELECT 'Y' WHERE prophylaxis IS NOT NULL AND sex = 'male' AND  age < 15 ) AS b, 
(SELECT 'Y' WHERE prophylaxis IS NOT NULL AND sex = 'male' AND age > 14) AS c, 
(SELECT 'Y' WHERE prophylaxis IS NOT NULL AND sex = 'female' AND age < 15 ) AS d,
(SELECT 'Y' WHERE prophylaxis IS NOT NULL AND sex = 'female' AND age > 14) AS e,

--enrolled in care
(SELECT 'Y' WHERE enrolled_in_care = 't' AND age < 1) AS f,
(SELECT 'Y' WHERE enrolled_in_care = 't' AND age < 15 AND sex = 'male') AS g, 
(SELECT 'Y' WHERE enrolled_in_care = 't' AND age < 15 AND sex = 'female') AS h,
(SELECT 'Y' WHERE enrolled_in_care = 't' AND age > 14 AND sex = 'male') AS i,
(SELECT 'Y' WHERE enrolled_in_care = 't' AND age > 14 AND sex = 'female') AS j,

---- in care revist
(SELECT 'Y' WHERE visit_type = 'n' AND age < 1 AND prophylaxis IS NOT NULL AND enrolled_in_care='f') AS k,
(SELECT 'Y' WHERE visit_type = 'n' AND age < 15 AND sex = 'male' AND prophylaxis IS NOT NULL AND enrolled_in_care='f') AS l,
(SELECT 'Y' WHERE visit_type = 'n' AND age < 15 AND sex = 'female' AND prophylaxis IS NOT NULL AND enrolled_in_care='f') AS m, 
(SELECT 'Y' WHERE visit_type = 'n' AND age > 14 AND sex = 'male' AND prophylaxis IS NOT NULL AND enrolled_in_care='f') AS n,
(SELECT 'Y' WHERE visit_type = 'n' AND age > 14 AND sex = 'female' AND prophylaxis IS NOT NULL AND enrolled_in_care='f') AS o,

(SELECT dur_mnths WHERE prophylaxis IS NOT NULL ) AS p1,
(SELECT dur_mnths  ) AS p2,

----start art
(SELECT 'Y' WHERE started_on_art = 't' AND age < 1 ) AS q,
(SELECT 'Y' WHERE started_on_art = 't' AND age < 15 AND sex = 'male') AS r,
(SELECT 'Y' WHERE started_on_art = 't' AND age < 15 AND sex = 'female') AS s, 
(SELECT 'Y' WHERE started_on_art = 't' AND age > 14 AND sex = 'male') AS t, 
(SELECT 'Y' WHERE started_on_art = 't' AND age > 14 AND sex = 'female') AS u,
(SELECT 'Y' WHERE started_on_art = 't' AND pregnant='t') AS v,
(SELECT 'Y' WHERE started_on_art = 't' AND tb_tx = 't') AS w,

----continuation on theraphy
(SELECT 'Y' WHERE visit_type = 'n' AND age < 1 AND started_on_art != 't' AND enrolled_in_care != 't' AND pre_art != 't') AS x,
(SELECT 'Y' WHERE visit_type = 'n' AND age < 15 AND sex = 'male' AND started_on_art != 't' AND enrolled_in_care != 't' AND pre_art != 't') AS y, 
(SELECT 'Y' WHERE visit_type = 'n' AND age > 14 AND sex = 'female' AND started_on_art != 't' AND enrolled_in_care != 't' AND pre_art != 't') AS z, 
(SELECT 'Y' WHERE visit_type = 'n' AND age < 15 AND sex = 'male' AND started_on_art != 't' AND enrolled_in_care != 't' AND pre_art != 't') AS aa,
(SELECT 'Y' WHERE visit_type = 'n' AND age > 14 AND sex = 'female' AND started_on_art != 't' AND enrolled_in_care != 't' AND pre_art != 't') AS ab,
(SELECT dur_mnths) AS ac,

----tb screening
(SELECT 'Y' WHERE tb_screening = 't' AND age < 15 AND sex = 'male') AS ad,
(SELECT 'Y' WHERE tb_screening = 't' AND age < 15 AND sex = 'female') AS ae,
(SELECT 'Y' WHERE tb_screening = 't' AND age > 14 AND sex = 'male') AS af,
(SELECT 'Y' WHERE tb_screening = 't' AND age > 14 AND sex = 'female') AS ag,

----cervical cancer
(SELECT 'Y' WHERE cancer_screening = 't' AND age > 17 AND sex = 'female') AS ah,

---PWP
(SELECT 'Y' WHERE pwp_contraceptive = 't') AS ai,
(SELECT 'Y' WHERE pwp_condoms = 't') AS aj,

(SELECT 'Y' WHERE  age > 17 AND sex = 'female' AND visit_type='n') AS ak,
(CASE WHEN (scheduled_visit = 't') THEN 'S' ELSE 'U' END) AS al,

---------------------------------------------------------aggreagate-----------------------------------------------------

(SELECT count(ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND sex = 'male' AND visit_type='n' AND age < 15 ) AS sumb,
(SELECT count(ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND sex = 'male' AND visit_type='n' AND age > 14 ) AS sumc,
(SELECT count(ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND sex = 'female' AND visit_type='n' AND age < 15 ) AS sumd,
(SELECT count(ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND sex = 'female' AND visit_type='n' AND age > 14 ) AS sume,

(SELECT count(ccc_no) FROM mike WHERE enrolled_in_care = 't' AND age < 1 ) AS sumf,
(SELECT count(ccc_no) FROM mike WHERE enrolled_in_care = 't' AND age < 15 AND sex = 'male') AS sumg,
(SELECT count(ccc_no) FROM mike WHERE enrolled_in_care = 't' AND age < 15 AND sex = 'female') AS sumh,
(SELECT count(ccc_no) FROM mike WHERE enrolled_in_care = 't' AND age > 14 AND sex = 'male') AS sumi,
(SELECT count(ccc_no) FROM mike WHERE enrolled_in_care = 't' AND age > 14 AND sex = 'female') AS sumj,

---- in care revist
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND age < 1 AND prophylaxis IS NOT NULL AND enrolled_in_care='f') AS sumk,
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND age < 15 AND sex = 'male' AND prophylaxis IS NOT NULL AND enrolled_in_care='f') AS suml,
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND age < 15 AND sex = 'female' AND prophylaxis IS NOT NULL AND enrolled_in_care='f') AS summ, 
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND age > 14 AND sex = 'male' AND prophylaxis IS NOT NULL AND enrolled_in_care='f') AS sumn,
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND age > 14 AND sex = 'female' AND prophylaxis IS NOT NULL AND enrolled_in_care='f') AS sumo,

----start art
(SELECT count(ccc_no) FROM mike WHERE started_on_art = 't' AND age < 1 ) AS sumq,
(SELECT count(ccc_no) FROM mike WHERE started_on_art = 't' AND age < 15 AND sex = 'male') AS sumr, 
(SELECT count(ccc_no) FROM mike WHERE started_on_art = 't' AND age < 15 AND sex = 'female') AS sums,
(SELECT count(ccc_no) FROM mike WHERE started_on_art = 't' AND age > 14 AND sex = 'male') AS sumt, 
(SELECT count(ccc_no) FROM mike WHERE started_on_art = 't' AND age > 14 AND sex = 'female') AS sumu,
(SELECT count(ccc_no) FROM mike WHERE started_on_art = 't' AND pregnant='t') AS sumv,
(SELECT count(ccc_no) FROM mike WHERE started_on_art = 't' AND tb_tx = 't') AS sumw,

----continuation on theraphy
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND age < 1  AND started_on_art != 't' AND enrolled_in_care != 't' AND pre_art != 't') AS sumx,
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND age < 15 AND sex = 'male'  AND started_on_art != 't' AND enrolled_in_care != 't' AND pre_art != 't') AS sumy, 
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND age > 14 AND sex = 'female'  AND started_on_art != 't' AND enrolled_in_care != 't' AND pre_art != 't') AS sumz, 
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND age < 15 AND sex = 'male'  AND started_on_art != 't' AND enrolled_in_care != 't' AND pre_art != 't') AS sumaa,
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND age > 14 AND sex = 'female'  AND started_on_art != 't' AND enrolled_in_care != 't' AND pre_art != 't') AS sumab,


----tb screening
(SELECT  count(ccc_no) FROM mike WHERE tb_screening = 't' AND age < 15 AND sex = 'male') AS sumad,
(SELECT  count(ccc_no) FROM mike WHERE tb_screening = 't' AND age < 15 AND sex = 'female') AS sumae,
(SELECT  count(ccc_no) FROM mike WHERE tb_screening = 't' AND age > 14 AND sex = 'male') AS sumaf,
(SELECT  count(ccc_no) FROM mike WHERE tb_screening = 't' AND age > 14 AND sex = 'female') AS sumag,

----cervical cancer
(SELECT count(ccc_no) FROM mike WHERE cancer_screening = 't' AND age > 17 AND sex = 'female') AS sumah,

---PWP
(SELECT count(ccc_no) FROM mike WHERE pwp_contraceptive = 't') AS sumai,
(SELECT count(ccc_no) FROM mike WHERE pwp_condoms = 't') AS sumaj,

(SELECT count(ccc_no) FROM mike WHERE  age > 17 AND sex = 'female' AND visit_type='n') AS sumak,


---------------------------------------------------------aggreagate two's-----------------------------------------------------

(SELECT count(ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND sex = 'male' AND visit_type='n' AND age < 15 AND dur_mnths = 2) AS db,
(SELECT count(ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND sex = 'male' AND visit_type='n' AND age > 14 AND dur_mnths = 2) AS dc,
(SELECT count(ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND sex = 'female' AND visit_type='n' AND age < 15 AND dur_mnths = 2) AS dd,
(SELECT count(ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND sex = 'female' AND visit_type='n' AND age > 14 AND dur_mnths = 2) AS de,

(SELECT count(ccc_no) FROM mike WHERE enrolled_in_care = 't' AND age < 1 AND dur_mnths = 2) AS df,
(SELECT count(ccc_no) FROM mike WHERE enrolled_in_care = 't' AND age < 15 AND sex = 'male' AND dur_mnths = 2) AS dg,
(SELECT count(ccc_no) FROM mike WHERE enrolled_in_care = 't' AND age < 15 AND sex = 'female' AND dur_mnths = 2) AS dh,
(SELECT count(ccc_no) FROM mike WHERE enrolled_in_care = 't' AND age > 14 AND sex = 'male' AND dur_mnths = 2) AS di,
(SELECT count(ccc_no) FROM mike WHERE enrolled_in_care = 't' AND age > 14 AND sex = 'female' AND dur_mnths = 2) AS dj,

(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND age < 1 AND prophylaxis IS NOT NULL AND enrolled_in_care='f' AND dur_mnths = 2) AS dk,
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND age < 15 AND sex = 'male' AND prophylaxis IS NOT NULL AND enrolled_in_care='f' AND dur_mnths = 2) AS dl,
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND age < 15 AND sex = 'female' AND prophylaxis IS NOT NULL AND enrolled_in_care='f' AND dur_mnths = 2) AS dm, 
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND age > 14 AND sex = 'male' AND prophylaxis IS NOT NULL AND enrolled_in_care='f' AND dur_mnths = 2) AS dn,
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND age > 14 AND sex = 'female' AND prophylaxis IS NOT NULL AND enrolled_in_care='f' AND dur_mnths = 2) AS do,

----start art
(SELECT count(ccc_no) FROM mike WHERE started_on_art = 't' AND age < 1  AND dur_mnths = 2) AS dq,
(SELECT count(ccc_no) FROM mike WHERE started_on_art = 't' AND age < 15 AND sex = 'male' AND prophylaxis IS NOT NULL AND dur_mnths = 2) AS dr, 
(SELECT count(ccc_no) FROM mike WHERE started_on_art = 't' AND age < 15 AND sex = 'female' AND prophylaxis IS NOT NULL AND dur_mnths = 2) AS ds,
(SELECT count(ccc_no) FROM mike WHERE started_on_art = 't' AND age > 14 AND sex = 'male' AND prophylaxis IS NOT NULL AND dur_mnths = 2) AS dt, 
(SELECT count(ccc_no) FROM mike WHERE started_on_art = 't' AND age > 14 AND sex = 'female' AND prophylaxis IS NOT NULL AND dur_mnths = 2) AS du,
(SELECT count(ccc_no) FROM mike WHERE started_on_art = 't' AND pregnant='t' AND dur_mnths = 2) AS dv,
(SELECT count(ccc_no) FROM mike WHERE started_on_art = 't' AND tb_tx = 't' AND dur_mnths = 2) AS dw,

----continuation on theraphy
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND age < 1  AND started_on_art != 't' AND enrolled_in_care != 't' AND pre_art != 't' AND dur_mnths = 2) AS dx,
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND age < 15 AND sex = 'male'  AND started_on_art != 't' AND enrolled_in_care != 't' AND pre_art != 't' AND dur_mnths = 2) AS dy, 
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND age > 14 AND sex = 'male'  AND started_on_art != 't' AND enrolled_in_care != 't' AND pre_art != 't' AND dur_mnths = 2) AS dz, 
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND age < 15 AND sex = 'female'  AND started_on_art != 't' AND enrolled_in_care != 't' AND pre_art != 't' AND dur_mnths = 2) AS daa,
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND age > 14 AND sex = 'female'  AND started_on_art != 't' AND enrolled_in_care != 't' AND pre_art != 't' AND dur_mnths = 2) AS dab,

----tb screening
(SELECT  count(ccc_no) FROM mike WHERE tb_screening = 't' AND age < 15 AND sex = 'male' AND dur_mnths = 2) AS dad,
(SELECT  count(ccc_no) FROM mike WHERE tb_screening = 't' AND age < 15 AND sex = 'female' AND dur_mnths = 2) AS dae,
(SELECT  count(ccc_no) FROM mike WHERE tb_screening = 't' AND age > 14 AND sex = 'male' AND dur_mnths = 2) AS daf,
(SELECT  count(ccc_no) FROM mike WHERE tb_screening = 't' AND age > 14 AND sex = 'female' AND dur_mnths = 2) AS dag,

----cervical cancer
(SELECT count(ccc_no) FROM mike WHERE cancer_screening = 't' AND age > 17 AND sex = 'female' AND dur_mnths = 2) AS dah,

---PWP
(SELECT count(ccc_no) FROM mike WHERE pwp_contraceptive = 't' AND dur_mnths = 2) AS dai,
(SELECT count(ccc_no) FROM mike WHERE pwp_condoms = 't' AND dur_mnths = 2) AS daj,

(SELECT count(ccc_no) FROM mike WHERE  age > 17 AND sex = 'female' AND visit_type='n' AND dur_mnths = 2) AS dak,

---------------------------------------------------------aggreagate three's-----------------------------------------------------

(SELECT count(ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND sex = 'male' AND visit_type='n' AND age < 15 AND dur_mnths = 3) AS tb,
(SELECT count(ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND sex = 'male' AND visit_type='n' AND age > 14 AND dur_mnths = 3) AS tc,
(SELECT count(ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND sex = 'female' AND visit_type='n' AND age < 15 AND dur_mnths = 3) AS td,
(SELECT count(ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND sex = 'female' AND visit_type='n' AND age > 14 AND dur_mnths = 3) AS te,

(SELECT count(ccc_no) FROM mike WHERE enrolled_in_care = 't' AND age < 1 AND dur_mnths = 3) AS tf,
(SELECT count(ccc_no) FROM mike WHERE enrolled_in_care = 't' AND age < 15 AND sex = 'male' AND dur_mnths = 3) AS tg,
(SELECT count(ccc_no) FROM mike WHERE enrolled_in_care = 't' AND age < 15 AND sex = 'female' AND dur_mnths = 3) AS th,
(SELECT count(ccc_no) FROM mike WHERE enrolled_in_care = 't' AND age > 14 AND sex = 'male' AND dur_mnths = 3) AS ti,
(SELECT count(ccc_no) FROM mike WHERE enrolled_in_care = 't' AND age > 14 AND sex = 'female' AND dur_mnths = 3) AS tj,

(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND age < 1 AND prophylaxis IS NOT NULL AND enrolled_in_care='f' AND dur_mnths = 3) AS tk,
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND age < 15 AND sex = 'male' AND prophylaxis IS NOT NULL AND enrolled_in_care='f' AND dur_mnths = 3) AS tl,
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND age < 15 AND sex = 'female' AND prophylaxis IS NOT NULL AND enrolled_in_care='f' AND dur_mnths = 3) AS tm, 
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND age > 14 AND sex = 'male' AND prophylaxis IS NOT NULL AND enrolled_in_care='f' AND dur_mnths = 3) AS tn,
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND age > 14 AND sex = 'female' AND prophylaxis IS NOT NULL AND enrolled_in_care='f' AND dur_mnths = 3) AS to,

----start art
(SELECT count(ccc_no) FROM mike WHERE started_on_art = 't' AND age < 1  AND dur_mnths = 3) AS tq,
(SELECT count(ccc_no) FROM mike WHERE started_on_art = 't' AND age < 15 AND sex = 'male' AND prophylaxis IS NOT NULL AND dur_mnths = 3) AS tr, 
(SELECT count(ccc_no) FROM mike WHERE started_on_art = 't' AND age < 15 AND sex = 'female' AND prophylaxis IS NOT NULL AND dur_mnths = 3) AS ts,
(SELECT count(ccc_no) FROM mike WHERE started_on_art = 't' AND age > 14 AND sex = 'male' AND prophylaxis IS NOT NULL AND dur_mnths = 3) AS tt, 
(SELECT count(ccc_no) FROM mike WHERE started_on_art = 't' AND age > 14 AND sex = 'female' AND prophylaxis IS NOT NULL AND dur_mnths = 3) AS tu,
(SELECT count(ccc_no) FROM mike WHERE started_on_art = 't' AND pregnant='t' AND dur_mnths = 3) AS tv,
(SELECT count(ccc_no) FROM mike WHERE started_on_art = 't' AND tb_tx = 't' AND dur_mnths = 3) AS tw,

----continuation on theraphy
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND age < 1  AND started_on_art != 't' AND enrolled_in_care != 't' AND pre_art != 't' AND dur_mnths = 3) AS tx,
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND age < 15 AND sex = 'male'  AND started_on_art != 't' AND enrolled_in_care != 't' AND pre_art != 't' AND dur_mnths = 3) AS ty, 
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND age > 14 AND sex = 'male'  AND started_on_art != 't' AND enrolled_in_care != 't' AND pre_art != 't' AND dur_mnths = 3) AS tz, 
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND age < 15 AND sex = 'female'  AND started_on_art != 't' AND enrolled_in_care != 't' AND pre_art != 't' AND dur_mnths = 3) AS taa,
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND age > 14 AND sex = 'female'  AND started_on_art != 't' AND enrolled_in_care != 't' AND pre_art != 't' AND dur_mnths = 3) AS tab,

----tb screening
(SELECT  count(ccc_no) FROM mike WHERE tb_screening = 't' AND age < 15 AND sex = 'male' AND dur_mnths = 3) AS tad,
(SELECT  count(ccc_no) FROM mike WHERE tb_screening = 't' AND age < 15 AND sex = 'female' AND dur_mnths = 3) AS tae,
(SELECT  count(ccc_no) FROM mike WHERE tb_screening = 't' AND age > 14 AND sex = 'male' AND dur_mnths = 3) AS taf,
(SELECT  count(ccc_no) FROM mike WHERE tb_screening = 't' AND age > 14 AND sex = 'female' AND dur_mnths = 3) AS tag,

----cervical cancer
(SELECT count(ccc_no) FROM mike WHERE cancer_screening = 't' AND age > 17 AND sex = 'female' AND dur_mnths = 3) AS tah,

---PWP
(SELECT count(ccc_no) FROM mike WHERE pwp_contraceptive = 't' AND dur_mnths = 3) AS tai,
(SELECT count(ccc_no) FROM mike WHERE pwp_condoms = 't' AND dur_mnths = 3) AS taj,

(SELECT count(ccc_no) FROM mike WHERE  age > 17 AND sex = 'female' AND visit_type='n' AND dur_mnths = 3) AS tak

 from mike

";
			$mydata = $this->db->query($sql);
	
			if($mydata->num_rows() < 1){
				$this->session->set_flashdata('flash', 'true');
				$this->session->set_flashdata('flashtype', 'warning');
				$this->session->set_flashdata('flashmessage', 'we do not have data for the given period');
				redirect('reports/moh366');
			}
			$data['mydata'] = $mydata;	
			$data['title'] = "MOH Daily Activity Register (366)";
			$mycon = $this->load->view('mohdar',$data,TRUE);
			$data['zz'] = base64_encode(gzdeflate(serialize($mycon)));
			$this->load->view('mohdar',$data);
			$this->load->view('export',$data);
			
				
			
		}
						
		
	}
	
	
	

	
}
