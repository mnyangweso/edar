<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Act extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->pbscms->_check_logged();
		$this->form_validation->set_error_delimiters('<div class="ferror">', '</div>');
		//session_start();
	}
	
	public function index()
	{
		

		$this->form_validation->set_rules('mntyr','Month Year','trim|required');
		
		if ($this->form_validation->run() == FALSE){ 
			
			$data['title'] = "MOH ACT Report";
			$data['content'] = $this->load->view('mnt_yr',$data,TRUE);
			$this->load->view('template',$data);

			
		}else{
			if($this->input->post('ftype') ){				
				$this->pbscms->_export($this->input->post('zz'),$this->input->post('ftype'),$this->input->post('title')." ".$this->input->post('full_date'),"portrait","a4");
			}

			
			$var = explode(" ",$this->input->post('mntyr'));
			$mnt = $var[0];
			$yr = $var[1];
			$siku = "'01-$mnt-$yr'::DATE";

			$data['full_date'] = date('F', mktime(0, 0, 0, $mnt, 10)). " ".$yr;

			$sqlf =	"
				SELECT 
				EXTRACT (MONTH FROM('01-12-2015'::DATE - INTERVAL '1 MONTH')) AS twos, 
				EXTRACT (MONTH FROM('01-12-2015'::DATE - INTERVAL '2 MONTH')) AS threes
				";		
				$dd = $this->db->query($sqlf)->row();
				$twos = $dd->twos;
				$threes = $dd->threes;
			//print_r($data['full_date']); die();
			//SELECT (date_trunc('MONTH', $1) + INTERVAL '1 MONTH - 1 day')::DATE;
		
			$sql = "
				WITH mike AS (
				SELECT
				m.*, EXTRACT (MONTH FROM AGE(tca,visit_date)) AS dur_mnths,
				EXTRACT (MONTH FROM visit_date) AS mnths,
				--to_char(visit_date,'mon') AS mnths

(SELECT l.tb_screening FROM dar l WHERE l.ccc_no = m.ccc_no ORDER BY l.visit_date DESC LIMIT 1) AS last_tb_screen,
				DATE_PART('MONTH',visit_date) AS mntpart				
				FROM dar m
				WHERE m.visit_date
				BETWEEN ($siku - INTERVAL '2 MONTH') AND (DATE_TRUNC('MONTH', $siku) + INTERVAL '1 MONTH - 1 day')::DATE
				AND m.tca >= $siku
				
				)SELECT 
				(SELECT COUNT(ccc_no) FROM mike WHERE enrolled_in_care = 't' AND mntpart = $mnt AND age BETWEEN 0 AND 9 ) AS a1,
				(SELECT COUNT(ccc_no) FROM mike WHERE enrolled_in_care = 't' AND mntpart = $mnt AND age BETWEEN 10 AND 14 ) AS b1,
				(SELECT COUNT(ccc_no) FROM mike WHERE enrolled_in_care = 't' AND mntpart = $mnt AND age BETWEEN 15 AND 19 ) AS c1,
				(SELECT COUNT(ccc_no) FROM mike WHERE enrolled_in_care = 't' AND mntpart = $mnt AND age > 19 ) AS d1,
				(SELECT COUNT(ccc_no) FROM mike WHERE enrolled_in_care = 't' AND mntpart = $mnt) AS e1,

				(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) ) AND age BETWEEN 0 AND 9  ) AS a2,
				(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) ) AND age BETWEEN 10 AND 14  ) AS b2,
				(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) ) AND age BETWEEN 15 AND 19  ) AS c2,
				(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) ) AND age > 19  ) AS d2,
				(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) ) ) AS e2,
		
				(SELECT COUNT(ccc_no) FROM mike WHERE started_on_art='t' AND visit_type='n' AND age BETWEEN 0 AND 9) AS a3,
				(SELECT COUNT(ccc_no) FROM mike WHERE started_on_art='t' AND visit_type='n' AND age BETWEEN 10 AND 14) AS b3,
				(SELECT COUNT(ccc_no) FROM mike WHERE started_on_art='t' AND visit_type='n' AND age BETWEEN 15 AND 19) AS c3,
				(SELECT COUNT(ccc_no) FROM mike WHERE started_on_art='t' AND visit_type='n' AND age > 19) AS d3,
				(SELECT COUNT(ccc_no) FROM mike WHERE started_on_art='t' AND visit_type='n') AS e3,

				(SELECT COUNT(distinct ccc_no)  FROM mike WHERE pre_art = 'f' AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  AND age BETWEEN 0 AND 9) AS a4,
				(SELECT COUNT(distinct ccc_no)  FROM mike WHERE pre_art = 'f' AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  AND age BETWEEN 10 AND 14) AS b4,
				(SELECT COUNT(distinct ccc_no)  FROM mike WHERE pre_art = 'f' AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  AND age BETWEEN 15 AND 19) AS c4,
				(SELECT COUNT(distinct ccc_no)  FROM mike WHERE pre_art = 'f' AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  AND age > 19 ) AS d4,
				(SELECT COUNT(distinct ccc_no)  FROM mike WHERE pre_art = 'f' AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) ) ) AS e4,

				(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) ) AND age BETWEEN 0 AND 9 AND last_tb_screen = 't'  ) AS a5,
				(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) ) AND age BETWEEN 10 AND 14 AND last_tb_screen = 't'  ) AS b5,
				(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) ) AND age BETWEEN 15 AND 19 AND last_tb_screen = 't'  ) AS c5,
				(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) ) AND age > 19 AND last_tb_screen = 't'  ) AS d5,
				(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) ) AND last_tb_screen = 't' ) AS e5

				";
			
				$act = $this->db->query($sql)->row_array();


			$data['a1'] = $act['a1'];
			$data['b1'] = $act['b1'];
			$data['c1'] = $act['c1'];
			$data['d1'] = $act['d1'];
			$data['e1'] = $act['e1'];

			$data['a2'] = $act['a2'];
			$data['b2'] = $act['b2'];
			$data['c2'] = $act['c2'];
			$data['d2'] = $act['d2'];
			$data['e2'] = $act['e2'];

			$data['a3'] = $act['a3'];
			$data['b3'] = $act['b3'];
			$data['c3'] = $act['c3'];
			$data['d3'] = $act['d3'];
			$data['e3'] = $act['e3'];

			$data['a4'] = $act['a4'];
			$data['b4'] = $act['b4'];
			$data['c4'] = $act['c4'];
			$data['d4'] = $act['d4'];
			$data['e4'] = $act['e4'];

			$data['a5'] = $act['a5'];
			$data['b5'] = $act['b5'];
			$data['c5'] = $act['c5'];
			$data['d5'] = $act['d5'];
			$data['e5'] = $act['e5'];
			
			//print_r($data); die();
			

			$data['title'] = "ACT Report";
			$mycon = $this->load->view('act',$data,TRUE);
			$data['zz'] = base64_encode(gzdeflate(serialize($mycon)));
			$data['content'] = $this->load->view('act',$data,TRUE);
			$data['content'] .= $this->load->view('export',$data,TRUE);
			$mycon = $this->load->view('act',$data,TRUE);
			
			$this->load->view('template',$data);
			
		}

	 }
	

	
}
