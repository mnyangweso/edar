<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Linelist extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->pbscms->_check_logged();
		$this->form_validation->set_error_delimiters('<div class="ferror">', '</div>');
		//session_start();
	}
	
	public function index()
	{
		$this->form_validation->set_rules('start_date','Start Date','trim|required');
				
		if ($this->form_validation->run() == FALSE){ 
			
			$data['title'] = "Appointment Report";
			$data['content'] = $this->load->view('date_range2',$data,TRUE);
			$this->load->view('template',$data);

			
		}else{
			$sd = $this->input->post('start_date');
			$ed = $this->input->post('end_date');
	
			//$sql = "SELECT ROW_NUMBER() OVER (ORDER BY visit_date) AS rn,d.*,(select 'Yes' FROM dar c WHERE c.ccc_no = d.ccc_no and d.tca=c.visit_date ) as attended FROM dar d WHERE visit_date BETWEEN '$sd' AND '$ed'";
			$sql = " SELECT ROW_NUMBER() OVER (ORDER BY visit_date) AS rn, id, ccc_no, UPPER(sex) AS sex, dob, visit_date,UPPER(visit_type) AS visit_type, 
	CASE WHEN scheduled_visit = 't' THEN 'SCHEDULED' WHEN scheduled_visit = 'f' THEN 'UNSCHEDULED'  END AS scheduled_visit, 
       tca, UPPER(prophylaxis) AS prophylaxis,yn_bool( enrolled_in_care) AS enrolled_in_care, yn_bool(started_on_art) AS started_on_art,
       yn_bool( tb_screening) AS tb_screening, yn_bool(started_ipt) AS started_ipt,yn_bool( pwp_contraceptive) AS pwp_contraceptive, 
       yn_bool(pwp_condoms) AS pwp_condoms, yn_bool(cancer_screening) AS cancer_screening,yn_bool(nutrition_accessed) AS nutrition_accessed,
	yn_bool(nutrition_malnorished) AS nutrition_malnorished,yn_bool(nutrition_food_prescribed) AS nutrition_food_prescribed, 
      CASE WHEN pre_art = 't' THEN 'PRE ART' WHEN pre_art = 'f' THEN 'ART'  END AS art_status,
	yn_bool(pregnant) AS pregnant, age,yn_bool(tb_tx) AS tb_tx
  FROM dar
		WHERE visit_date BETWEEN '$sd' AND '$ed'
			";
			$query = $this->db->query($sql);
			$data['sd'] = $sd;
			$data['ed'] = $ed;
			$tt = now();
			//print_r($sql); die();
			//$data['title'] = "Patients Details Report";
			//$data['content'] = $this->load->view('appointment',$data,TRUE);
			//$this->load->view('template',$data);
			$this->load->dbutil();
			$delimiter = ",";
			$newline = "\r\n";
			$enclosure = '"';

			$data = $this->dbutil->csv_from_result($query, $delimiter, $newline, $enclosure);
			force_download("CSV Linelist Report Between $sd AND $ed $tt.csv", $data);
			
			$this->session->set_flashdata('flash','true');
			$this->session->set_flashdata('flashtype','success');
			$this->session->set_flashdata('flashmessage','The download is complete');
			redirect('reports/linelist');
		}
	}
	
	
	

	
}
