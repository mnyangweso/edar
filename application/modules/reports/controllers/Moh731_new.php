<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Moh731_new extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->pbscms->_check_logged();
		$this->form_validation->set_error_delimiters('<div class="ferror">', '</div>');
		//session_start();
	}
	
	

	

	public function index()
	{
		

		$this->form_validation->set_rules('mntyr','Month Year','trim|required');
				
		if ($this->form_validation->run() == FALSE){ 
			
			$data['title'] = "MOH 731 (2015) Report";
			$data['content'] = $this->load->view('mnt_yr',$data,TRUE);
			$this->load->view('template',$data);

			
		}else{

			if($this->input->post('ftype') ){				
				$this->pbscms->_export($this->input->post('zz'),$this->input->post('ftype'),$this->input->post('title')." ".$this->input->post('full_date'),"portrait","a4");
			}

			$var = explode(" ",$this->input->post('mntyr'));
			$mnt = $var[0];
			$yr = $var[1];
			$siku = "'01-$mnt-$yr'";

			$data['full_date'] = date('F', mktime(0, 0, 0, $mnt, 10)). " ".$yr;

			$sqlf =	"
				SELECT 
				EXTRACT (MONTH FROM('01-12-2015'::DATE - INTERVAL '1 MONTH')) AS twos, 
				EXTRACT (MONTH FROM('01-12-2015'::DATE - INTERVAL '2 MONTH')) AS threes
				";		
				$dd = $this->db->query($sqlf)->row();
				$twos = $dd->twos;
				$threes = $dd->threes;			

			//print_r($data['full_date']); die();
			//SELECT (date_trunc('MONTH', $1) + INTERVAL '1 MONTH - 1 day')::DATE;
			$sql = "
				WITH mike AS (
				SELECT
				m.*, EXTRACT (MONTH FROM AGE(tca,visit_date)) AS dur_mnths,
				EXTRACT (MONTH FROM visit_date) AS mnths,
				--to_char(visit_date,'mon') AS mnths
--(SELECT l.visit_date FROM dar l WHERE l.ccc_no = m.ccc_no ORDER BY l.visit_date DESC LIMIT 1) AS last_visit_date
(SELECT l.tb_screening FROM dar l WHERE l.ccc_no = m.ccc_no ORDER BY l.visit_date DESC LIMIT 1) AS last_tb_screen,
				DATE_PART('MONTH',visit_date) AS mntpart				
				FROM dar m
				WHERE m.visit_date
				BETWEEN ($siku::DATE - INTERVAL '2 MONTH') AND (DATE_TRUNC('MONTH', $siku::DATE) + INTERVAL '1 MONTH - 1 day')::DATE
				AND m.tca >= $siku::DATE
				
				)
				SELECT 
				-- (DATE_TRUNC('MONTH', $siku::DATE) + INTERVAL '1 MONTH - 1 day')::DATE  
				--(SELECT COUNT(DISTINCT ccc_no) FROM mike ) AS ca				
				--(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE DATE_PART('MONTH',visit_date)= $mnt ) AS ca
--select distinct on(ccc_no)ccc_no,visit_date,tca from dar where prophylaxis is not null AND visit_date BETWEEN '01-10-2015' AND '31-12-2015' AND tca >= '01-12-2015'


--------------------------------------------------------3.1 Enrolled in care
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE enrolled_in_care = 't') AS hv03_011, 
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE enrolled_in_care = 't' AND age < 2) AS hv03_001,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE enrolled_in_care = 't' AND age BETWEEN 2 AND 9) AS hv03_002,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE enrolled_in_care = 't' AND age BETWEEN 10 AND 14 AND sex='male' ) AS hv03_003,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE enrolled_in_care = 't' AND age BETWEEN 10 AND 14 AND sex='female' ) AS hv03_004,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE enrolled_in_care = 't' AND age BETWEEN 15 AND 19 AND sex='male' ) AS hv03_005,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE enrolled_in_care = 't' AND age BETWEEN 15 AND 19 AND sex='female' ) AS hv03_006,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE enrolled_in_care = 't' AND age BETWEEN 20 AND 24 AND sex='male' ) AS hv03_007,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE enrolled_in_care = 't' AND age BETWEEN 20 AND 24 AND sex='female' ) AS hv03_008,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE enrolled_in_care = 't' AND age > 24 AND sex='male' ) AS hv03_009,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE enrolled_in_care = 't' AND age > 24 AND sex='female' ) AS hv03_010,


--------------------------------------------------------3.3 Current in Care
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) ) ) AS hv03_035, 
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  AND age < 2) AS hv03_025,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  AND age BETWEEN 2 AND 9) AS hv03_026,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  AND age BETWEEN 10 AND 14 AND sex='male' ) AS hv03_027,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  AND age BETWEEN 10 AND 14 AND sex='female' ) AS hv03_028,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  AND age BETWEEN 15 AND 19 AND sex='male' ) AS hv03_029,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  AND age BETWEEN 15 AND 19 AND sex='female' ) AS hv03_030,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  AND age BETWEEN 20 AND 24 AND sex='male' ) AS hv03_031,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  AND age BETWEEN 20 AND 24 AND sex='female' ) AS hv03_032,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  AND age > 24 AND sex='male' ) AS hv03_033,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  AND age > 24 AND sex='female' ) AS hv03_034,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  AND key_pop = 't' ) AS hv03_036,

--------------------------------------------------------3.4 Starting ART
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE started_on_art='t') AS hv03_047, 
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE started_on_art='t' AND age < 2) AS hv03_037,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE started_on_art='t' AND age BETWEEN 2 AND 9) AS hv03_038,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE started_on_art='t' AND age BETWEEN 10 AND 14 AND sex='male' ) AS hv03_039,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE started_on_art='t' AND age BETWEEN 10 AND 14 AND sex='female' ) AS hv03_040,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE started_on_art='t' AND age BETWEEN 15 AND 19 AND sex='male' ) AS hv03_041,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE started_on_art='t' AND age BETWEEN 15 AND 19 AND sex='female' ) AS hv03_042,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE started_on_art='t' AND age BETWEEN 20 AND 24 AND sex='male' ) AS hv03_043,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE started_on_art='t' AND age BETWEEN 20 AND 24 AND sex='female' ) AS hv03_044,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE started_on_art='t' AND age > 24 AND sex='male' ) AS hv03_045,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE started_on_art='t' AND age > 24 AND sex='female' ) AS hv03_046,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE started_on_art='t' AND tb_tx ='t' ) AS hv03_048,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE started_on_art='t' AND dis_cou ='t' ) AS hv03_049,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE started_on_art='t' AND key_pop ='t' ) AS hv03_050,


--------------------------------------------------------3.7 On Prophylaxis
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  AND age < 2) AS hv03_075, 
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  AND age BETWEEN 2 AND 9) AS hv03_076,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  AND age BETWEEN 10 AND 14) AS hv03_077,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  AND age BETWEEN 15 AND 19) AS hv03_078,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  AND age BETWEEN 20 AND 24) AS hv03_079,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  AND age > 24) AS hv03_080,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) ) ) AS hv03_081,

--------------------------------------------------------3.8 On TB Screening
(SELECT COUNT(ccc_no) FROM mike WHERE mnths = $mnt AND tb_screening = 't' AND age < 2) AS hv03_082, 
(SELECT COUNT(ccc_no) FROM mike WHERE mnths = $mnt AND tb_screening = 't' AND age BETWEEN 2 AND 9) AS hv03_083,
(SELECT COUNT(ccc_no) FROM mike WHERE mnths = $mnt AND tb_screening = 't' AND age BETWEEN 10 AND 14) AS hv03_084,
(SELECT COUNT(ccc_no) FROM mike WHERE mnths = $mnt AND tb_screening = 't' AND age BETWEEN 15 AND 19) AS hv03_085,
(SELECT COUNT(ccc_no) FROM mike WHERE mnths = $mnt AND tb_screening = 't' AND age BETWEEN 20 AND 24) AS hv03_086,
(SELECT COUNT(ccc_no) FROM mike WHERE mnths = $mnt AND tb_screening = 't' AND age > 24) AS hv03_087,
(SELECT COUNT(ccc_no) FROM mike WHERE mnths = $mnt AND tb_screening = 't') AS hv03_088,

--------------------------------------------------------3.9 On started ipt
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE mnths = $mnt AND started_ipt = 't' AND age < 2) AS hv03_089, 
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE mnths = $mnt AND started_ipt = 't' AND age BETWEEN 2 AND 9) AS hv03_090,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE mnths = $mnt AND started_ipt = 't' AND age BETWEEN 10 AND 14) AS hv03_091,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE mnths = $mnt AND started_ipt = 't' AND age BETWEEN 15 AND 19) AS hv03_092,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE mnths = $mnt AND started_ipt = 't' AND age BETWEEN 20 AND 24) AS hv03_093,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE mnths = $mnt AND started_ipt = 't' AND age > 24) AS hv03_094,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE mnths = $mnt AND started_ipt = 't') AS hv03_095,


(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE mnths = $mnt AND nutrition_accessed = 't' AND age < 15) AS hv03_100,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE mnths = $mnt AND nutrition_accessed = 't' AND age > 14) AS hv03_101,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE mnths = $mnt AND nutrition_accessed = 't') AS hv03_102,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE mnths = $mnt AND nutrition_malnorished = 't' AND age < 15) AS hv03_103,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE mnths = $mnt AND nutrition_malnorished = 't' AND age > 14) AS hv03_104,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE mnths = $mnt AND nutrition_malnorished = 't') AS hv03_105,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE mnths = $mnt AND nutrition_food_prescribed = 't' AND age < 15) AS hv03_106,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE mnths = $mnt AND nutrition_food_prescribed = 't' AND age > 14) AS hv03_107,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE mnths = $mnt AND nutrition_food_prescribed = 't') AS hv03_108,

(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE mnths = $mnt AND sex = 'female' AND  cancer_screening='t' AND age > 17 ) AS hv03_111,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE mnths = $mnt AND sex = 'female' AND age > 17 ) AS hv03_112

		
FROM mike
";

			

			$care = $this->db->query($sql)->row_array();

			//print_r($care); die();
			
			$data['hv03_001'] = $care['hv03_001'];
			$data['hv03_002'] = $care['hv03_002'];
			$data['hv03_003'] = $care['hv03_003'];
			$data['hv03_004'] = $care['hv03_004'];
			$data['hv03_005'] = $care['hv03_005'];
			$data['hv03_006'] = $care['hv03_006'];
			$data['hv03_007'] = $care['hv03_007'];
			$data['hv03_008'] = $care['hv03_008'];
			$data['hv03_009'] = $care['hv03_009'];
			$data['hv03_010'] = $care['hv03_010'];
			$data['hv03_011'] = $care['hv03_011'];
//current in care
			$data['hv03_025'] = $care['hv03_025'];
			$data['hv03_026'] = $care['hv03_026'];
			$data['hv03_027'] = $care['hv03_027'];
			$data['hv03_028'] = $care['hv03_028'];
			$data['hv03_029'] = $care['hv03_029'];
			$data['hv03_030'] = $care['hv03_030'];
			$data['hv03_031'] = $care['hv03_031'];
			$data['hv03_032'] = $care['hv03_032'];
			$data['hv03_033'] = $care['hv03_033'];
			$data['hv03_034'] = $care['hv03_034'];
			$data['hv03_035'] = $care['hv03_035'];
			$data['hv03_036'] = $care['hv03_036'];

			$data['hv03_037'] = $care['hv03_037'];
			$data['hv03_038'] = $care['hv03_038'];
			$data['hv03_039'] = $care['hv03_039'];
			$data['hv03_040'] = $care['hv03_040'];
			$data['hv03_041'] = $care['hv03_041'];
			$data['hv03_042'] = $care['hv03_042'];
			$data['hv03_043'] = $care['hv03_043'];
			$data['hv03_044'] = $care['hv03_044'];
			$data['hv03_045'] = $care['hv03_045'];
			$data['hv03_046'] = $care['hv03_046'];
			$data['hv03_047'] = $care['hv03_047'];
			$data['hv03_048'] = $care['hv03_048'];
			$data['hv03_049'] = $care['hv03_049'];
			$data['hv03_050'] = $care['hv03_050'];

			$data['hv03_075'] = $care['hv03_075'];
			$data['hv03_076'] = $care['hv03_076'];
			$data['hv03_077'] = $care['hv03_077'];
			$data['hv03_078'] = $care['hv03_078'];
			$data['hv03_079'] = $care['hv03_079'];
			$data['hv03_080'] = $care['hv03_080'];
			$data['hv03_081'] = $care['hv03_081'];

		
			$data['hv03_082'] = $care['hv03_082'];
			$data['hv03_083'] = $care['hv03_083'];
			$data['hv03_084'] = $care['hv03_084'];
			$data['hv03_085'] = $care['hv03_085'];
			$data['hv03_086'] = $care['hv03_086'];
			$data['hv03_087'] = $care['hv03_087'];
			$data['hv03_088'] = $care['hv03_088'];

			$data['hv03_089'] = $care['hv03_089'];
			$data['hv03_090'] = $care['hv03_090'];
			$data['hv03_091'] = $care['hv03_091'];
			$data['hv03_092'] = $care['hv03_092'];
			$data['hv03_093'] = $care['hv03_093'];
			$data['hv03_094'] = $care['hv03_094'];
			$data['hv03_095'] = $care['hv03_095'];

			$data['hv03_100'] = $care['hv03_100'];
			$data['hv03_101'] = $care['hv03_101'];
			$data['hv03_102'] = $care['hv03_102'];
			$data['hv03_103'] = $care['hv03_103'];
			$data['hv03_104'] = $care['hv03_104'];
			$data['hv03_105'] = $care['hv03_105'];
			$data['hv03_106'] = $care['hv03_106'];
			$data['hv03_107'] = $care['hv03_107'];
			$data['hv03_108'] = $care['hv03_108'];

			$data['hv03_111'] = $care['hv03_111'];
			$data['hv03_112'] = $care['hv03_112'];




		
			$data['title'] = "MOH 731 (2015) Report";
			$mycon = $this->load->view('moh731_new',$data,TRUE);
			$data['zz'] = base64_encode(gzdeflate(serialize($mycon)));
			$data['content'] = $this->load->view('moh731_new',$data,TRUE);
			$data['content'] .= $this->load->view('export',$data,TRUE);
			$mycon = $this->load->view('moh731_new',$data,TRUE);

			$this->load->view('template',$data);
			
		}
						
		
	}
	

	
}
