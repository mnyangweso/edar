<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Moh366_new extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->pbscms->_check_logged();
		$this->form_validation->set_error_delimiters('<div class="ferror">', '</div>');
		//session_start();
	}
	
	

	public function index()
	{
		

		$this->form_validation->set_rules('mntyr','Month Year','trim|required');
		
		if ($this->form_validation->run() == FALSE){ 
			
			$data['title'] = "MOH Daily Activity Register (366) 2015";
			$data['content'] = $this->load->view('mnt_yr',$data,TRUE);
			$this->load->view('template',$data);

			
		}else{
			
			if($this->input->post('ftype') ){				
				$this->pbscms->_export($this->input->post('zz'),$this->input->post('ftype'),$this->input->post('title')." ".$this->input->post('full_date'),'landscape','a1');
			}

			$var = explode(" ",$this->input->post('mntyr'));
			$mnt = $var[0];
			$yr = $var[1];
			$siku = "01-$mnt-$yr";

			$data['full_date'] = date('F', mktime(0, 0, 0, $mnt, 10)). " ".$yr;


			$sql = "WITH mike AS (
		SELECT *,extract(YEAR FROM AGE(visit_date,dob)) AS yrs,extract (MONTH FROM AGE(tca,visit_date)) AS dur_mnths FROM dar WHERE DATE_PART('MONTH',visit_date) = $mnt  and DATE_PART('YEAR',visit_date) = $yr ORDER BY visit_date ASC 

		)SELECT ROW_NUMBER() OVER (ORDER BY ccc_no) AS a,visit_date as b,visit_type,ccc_no,ccc_no AS c,
---- on ctx 
(SELECT 'Y' WHERE enrolled_in_care = 't' AND yrs < 2 ) AS d, 
(SELECT 'Y' WHERE enrolled_in_care = 't' AND yrs BETWEEN 2 AND 9) AS e, 
(SELECT 'Y' WHERE enrolled_in_care = 't' AND sex = 'male' AND yrs BETWEEN 10 AND 14 ) AS f,
(SELECT 'Y' WHERE enrolled_in_care = 't' AND sex = 'female' AND yrs BETWEEN 10 AND 14 ) AS g,
(SELECT 'Y' WHERE enrolled_in_care = 't' AND sex = 'male' AND yrs BETWEEN 15 AND 19 ) AS h,
(SELECT 'Y' WHERE enrolled_in_care = 't' AND sex = 'female' AND yrs BETWEEN 15 AND 19 ) AS i,
(SELECT 'Y' WHERE enrolled_in_care = 't' AND sex = 'male' AND yrs BETWEEN 20 AND 24 ) AS j,
(SELECT 'Y' WHERE enrolled_in_care = 't' AND sex = 'female' AND yrs BETWEEN  20 AND 24  ) AS k,
(SELECT 'Y' WHERE enrolled_in_care = 't' AND sex = 'female' AND yrs > 24 ) AS l,
(SELECT 'Y' WHERE enrolled_in_care = 't' AND sex = 'female' AND yrs > 14) AS m,
(SELECT 'Y' WHERE enrolled_in_care = 't' AND key_pop = 't' ) AS n,

---- in care revist
(SELECT CASE WHEN visit_type = 'n' THEN dur_mnths::TEXT ELSE 'R' END  WHERE enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND yrs < 2) AS o,
(SELECT CASE WHEN visit_type = 'n' THEN dur_mnths::TEXT ELSE 'R' END WHERE enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND yrs BETWEEN 2 AND 9) AS p, 
(SELECT CASE WHEN visit_type = 'n' THEN dur_mnths::TEXT ELSE 'R' END WHERE enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'male' AND yrs BETWEEN 10 AND 14) AS q, 
(SELECT CASE WHEN visit_type = 'n' THEN dur_mnths::TEXT ELSE 'R' END WHERE enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'female' AND yrs BETWEEN 10 AND 14) AS r, 
(SELECT CASE WHEN visit_type = 'n' THEN dur_mnths::TEXT ELSE 'R' END WHERE enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'male' AND yrs BETWEEN 15 AND 19) AS s, 
(SELECT CASE WHEN visit_type = 'n' THEN dur_mnths::TEXT ELSE 'R' END WHERE enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'female' AND yrs BETWEEN 15 AND 19) AS t,
(SELECT CASE WHEN visit_type = 'n' THEN dur_mnths::TEXT ELSE 'R' END WHERE enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'male' AND yrs BETWEEN 20 AND 24) AS u, 
(SELECT CASE WHEN visit_type = 'n' THEN dur_mnths::TEXT ELSE 'R' END WHERE enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'female' AND yrs BETWEEN 20 AND 24) AS v,
(SELECT CASE WHEN visit_type = 'n' THEN dur_mnths::TEXT ELSE 'R' END WHERE enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'male' AND yrs > 24) AS w, 
(SELECT CASE WHEN visit_type = 'n' THEN dur_mnths::TEXT ELSE 'R' END WHERE enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'female' AND yrs > 24) AS x,
(SELECT CASE WHEN visit_type = 'n' THEN dur_mnths::TEXT ELSE 'R' END WHERE enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND key_pop = 't') AS y,


----start art
(SELECT 'Y' WHERE started_on_art = 't' AND yrs < 2 ) AS z,
(SELECT 'Y' WHERE started_on_art = 't' AND yrs BETWEEN 2 AND 9) AS aa, 
(SELECT 'Y' WHERE started_on_art = 't' AND sex = 'male' AND yrs BETWEEN 10 AND 14) AS ab,
(SELECT 'Y' WHERE started_on_art = 't' AND sex = 'female' AND yrs BETWEEN 10 AND 14) AS ac,
(SELECT 'Y' WHERE started_on_art = 't' AND sex = 'male' AND yrs BETWEEN 15 AND 19) AS ad,
(SELECT 'Y' WHERE started_on_art = 't' AND sex = 'female' AND yrs BETWEEN 15 AND 19) AS ae,
(SELECT 'Y' WHERE started_on_art = 't' AND sex = 'male' AND yrs BETWEEN 20 AND 24) AS af,
(SELECT 'Y' WHERE started_on_art = 't' AND sex = 'female' AND yrs BETWEEN 20 AND 24) AS ag,
(SELECT 'Y' WHERE started_on_art = 't' AND sex = 'male' AND yrs > 24) AS ah,
(SELECT 'Y' WHERE started_on_art = 't' AND sex = 'female' AND yrs > 24) AS ai,
(SELECT 'Y' WHERE started_on_art = 't' AND tb_tx = 't') AS aj,
(SELECT 'Y' WHERE started_on_art = 't' AND dis_cou = 't') AS ak,
(SELECT 'Y' WHERE started_on_art = 't' AND key_pop = 't') AS al,

---- in art revist
(SELECT CASE WHEN visit_type = 'n' THEN dur_mnths::TEXT ELSE 'R' END  WHERE enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND yrs < 2) AS am,
(SELECT CASE WHEN visit_type = 'n' THEN dur_mnths::TEXT ELSE 'R' END WHERE enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND yrs BETWEEN 2 AND 9) AS an, 
(SELECT CASE WHEN visit_type = 'n' THEN dur_mnths::TEXT ELSE 'R' END WHERE enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'male' AND yrs BETWEEN 10 AND 14) AS ao, 
(SELECT CASE WHEN visit_type = 'n' THEN dur_mnths::TEXT ELSE 'R' END WHERE enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'female' AND yrs BETWEEN 10 AND 14) AS ap, 
(SELECT CASE WHEN visit_type = 'n' THEN dur_mnths::TEXT ELSE 'R' END WHERE enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'male' AND yrs BETWEEN 15 AND 19) AS aq, 
(SELECT CASE WHEN visit_type = 'n' THEN dur_mnths::TEXT ELSE 'R' END WHERE enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'female' AND yrs BETWEEN 15 AND 19) AS ar,
(SELECT CASE WHEN visit_type = 'n' THEN dur_mnths::TEXT ELSE 'R' END WHERE enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'male' AND yrs BETWEEN 20 AND 24) AS ass, 
(SELECT CASE WHEN visit_type = 'n' THEN dur_mnths::TEXT ELSE 'R' END WHERE enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'female' AND yrs BETWEEN 20 AND 24) AS at,
(SELECT CASE WHEN visit_type = 'n' THEN dur_mnths::TEXT ELSE 'R' END WHERE enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'male' AND yrs > 24) AS au, 
(SELECT CASE WHEN visit_type = 'n' THEN dur_mnths::TEXT ELSE 'R' END WHERE enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'female' AND yrs > 24) AS av,
(SELECT CASE WHEN visit_type = 'n' THEN dur_mnths::TEXT ELSE 'R' END WHERE enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND key_pop = 't') AS aw,

---- on ctx and dapson
(SELECT 'Y' WHERE prophylaxis IS NOT NULL AND yrs < 2 ) AS ax, 
(SELECT 'Y' WHERE prophylaxis IS NOT NULL AND yrs BETWEEN 2 AND 9) AS ay, 
(SELECT 'Y' WHERE prophylaxis IS NOT NULL AND yrs BETWEEN 10 AND 14 ) AS az,
(SELECT 'Y' WHERE prophylaxis IS NOT NULL AND yrs BETWEEN 15 AND 19 ) AS ba,
(SELECT 'Y' WHERE prophylaxis IS NOT NULL AND yrs BETWEEN 20 AND 24 ) AS bb,
(SELECT 'Y' WHERE prophylaxis IS NOT NULL AND yrs > 24 ) AS bc,

---- screened for tb
(SELECT 'Y' WHERE tb_screening = 't' AND yrs < 2 ) AS bd, 
(SELECT 'Y' WHERE tb_screening = 't' AND yrs BETWEEN 2 AND 9) AS be, 
(SELECT 'Y' WHERE tb_screening = 't' AND yrs BETWEEN 10 AND 14 ) AS bf,
(SELECT 'Y' WHERE tb_screening = 't' AND yrs BETWEEN 15 AND 19 ) AS bg,
(SELECT 'Y' WHERE tb_screening = 't' AND yrs BETWEEN 20 AND 24 ) AS bh,
(SELECT 'Y' WHERE tb_screening = 't' AND yrs > 24 ) AS bi,


---- started on ipt
(SELECT 'Y' WHERE started_ipt = 't' AND yrs < 2 ) AS bj, 
(SELECT 'Y' WHERE started_ipt = 't' AND yrs BETWEEN 2 AND 9) AS bk, 
(SELECT 'Y' WHERE started_ipt = 't' AND yrs BETWEEN 10 AND 14 ) AS bl,
(SELECT 'Y' WHERE started_ipt = 't' AND yrs BETWEEN 15 AND 19 ) AS bm,
(SELECT 'Y' WHERE started_ipt = 't' AND yrs BETWEEN 20 AND 24 ) AS bn,
(SELECT 'Y' WHERE started_ipt = 't' AND yrs > 24 ) AS bo,

--enrolled in care
(SELECT 'Y' WHERE nutrition_accessed = 't' AND yrs < 15) AS bp,
(SELECT 'Y' WHERE nutrition_accessed = 't' AND yrs > 14) AS bq,
(SELECT 'Y' WHERE nutrition_malnorished = 't' AND yrs < 15) AS br,
(SELECT 'Y' WHERE nutrition_malnorished = 't' AND yrs > 14) AS bs,
(SELECT 'Y' WHERE nutrition_food_prescribed = 't' AND yrs < 15) AS bt,
(SELECT 'Y' WHERE nutrition_food_prescribed = 't' AND yrs > 14) AS bu, 

--cancer
(SELECT 'Y' WHERE cancer_screening = 't' AND yrs > 14) AS bv,
(SELECT 'Y' WHERE yrs > 17 AND sex = 'female') AS bw,

---------------------------------------------------------aggreagate-----------------------------------------------------

---- enrolled in care
(SELECT count(ccc_no) FROM mike WHERE enrolled_in_care = 't' AND yrs < 2 ) AS sumd, 
(SELECT count(ccc_no) FROM mike WHERE enrolled_in_care = 't' AND yrs BETWEEN 2 AND 9) AS sume, 
(SELECT count(ccc_no) FROM mike WHERE enrolled_in_care = 't' AND sex = 'male' AND yrs BETWEEN 10 AND 14 ) AS sumf,
(SELECT count(ccc_no) FROM mike WHERE enrolled_in_care = 't' AND sex = 'female' AND yrs BETWEEN 10 AND 14 ) AS sumg,
(SELECT count(ccc_no) FROM mike WHERE enrolled_in_care = 't' AND sex = 'male' AND yrs BETWEEN 15 AND 19 ) AS sumh,
(SELECT count(ccc_no) FROM mike WHERE enrolled_in_care = 't' AND sex = 'female' AND yrs BETWEEN 15 AND 19 ) AS sumi,
(SELECT count(ccc_no) FROM mike WHERE enrolled_in_care = 't' AND sex = 'male' AND yrs BETWEEN 20 AND 24 ) AS sumj,
(SELECT count(ccc_no) FROM mike WHERE enrolled_in_care = 't' AND sex = 'female' AND yrs BETWEEN  20 AND 24  ) AS sumk,
(SELECT count(ccc_no) FROM mike WHERE enrolled_in_care = 't' AND sex = 'female' AND yrs > 24 ) AS suml,
(SELECT count(ccc_no) FROM mike WHERE enrolled_in_care = 't' AND sex = 'female' AND yrs > 14) AS summ,
(SELECT count(ccc_no) FROM mike WHERE key_pop = 't' AND  enrolled_in_care = 't') AS sumn,

---- in care revist
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND yrs < 2) AS sumo,
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND yrs BETWEEN 2 AND 9) AS sump, 
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'male' AND yrs BETWEEN 10 AND 14) AS sumq, 
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'female' AND yrs BETWEEN 10 AND 14) AS sumr, 
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'male' AND yrs BETWEEN 15 AND 19) AS sums, 
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'female' AND yrs BETWEEN 15 AND 19) AS sumt,
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'male' AND yrs BETWEEN 20 AND 24) AS sumu, 
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'female' AND yrs BETWEEN 20 AND 24) AS sumv,
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'male' AND yrs > 24) AS sumw, 
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'female' AND yrs > 24) AS sumx,
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND  enrolled_in_care = 't' AND key_pop = 't') AS sumy,

----start art
(SELECT count(ccc_no) FROM mike WHERE started_on_art = 't' AND yrs < 2 ) AS sumz,
(SELECT count(ccc_no) FROM mike WHERE started_on_art = 't' AND yrs BETWEEN 2 AND 9) AS sumaa, 
(SELECT count(ccc_no) FROM mike WHERE started_on_art = 't' AND sex = 'male' AND yrs BETWEEN 10 AND 14) AS sumab,
(SELECT count(ccc_no) FROM mike WHERE started_on_art = 't' AND sex = 'female' AND yrs BETWEEN 10 AND 14) AS sumac,
(SELECT count(ccc_no) FROM mike WHERE started_on_art = 't' AND sex = 'male' AND yrs BETWEEN 15 AND 19) AS sumad,
(SELECT count(ccc_no) FROM mike WHERE started_on_art = 't' AND sex = 'female' AND yrs BETWEEN 15 AND 19) AS sumae,
(SELECT count(ccc_no) FROM mike WHERE started_on_art = 't' AND sex = 'male' AND yrs BETWEEN 20 AND 24) AS sumaf,
(SELECT count(ccc_no) FROM mike WHERE started_on_art = 't' AND sex = 'female' AND yrs BETWEEN 20 AND 24) AS sumag,
(SELECT count(ccc_no) FROM mike WHERE started_on_art = 't' AND sex = 'male' AND yrs > 24) AS sumah,
(SELECT count(ccc_no) FROM mike WHERE started_on_art = 't' AND sex = 'female' AND yrs > 24) AS sumai,
(SELECT count(ccc_no) FROM mike WHERE started_on_art = 't' AND tb_tx = 't') AS sumaj,
(SELECT count(ccc_no) FROM mike WHERE started_on_art = 't' AND dis_cou = 't') AS sumak,
(SELECT count(ccc_no) FROM mike WHERE started_on_art = 't' AND key_pop = 't') AS sumal,

---- in art revist
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND yrs < 2) AS sumam,
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND yrs BETWEEN 2 AND 9) AS suman, 
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'male' AND yrs BETWEEN 10 AND 14) AS sumao, 
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'female' AND yrs BETWEEN 10 AND 14) AS sumap, 
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'male' AND yrs BETWEEN 15 AND 19) AS sumaq, 
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'female' AND yrs BETWEEN 15 AND 19) AS sumar,
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'male' AND yrs BETWEEN 20 AND 24) AS sumass, 
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'female' AND yrs BETWEEN 20 AND 24) AS sumat,
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'male' AND yrs > 24) AS sumau, 
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'female' AND yrs > 24) AS sumav,
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND  enrolled_in_care = 't' AND key_pop = 't') AS sumaw,

---- on ctx and dapson
(SELECT count(ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND yrs < 2 ) AS sumax, 
(SELECT count(ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND yrs BETWEEN 2 AND 9) AS sumay, 
(SELECT count(ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND yrs BETWEEN 10 AND 14 ) AS sumaz,
(SELECT count(ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND yrs BETWEEN 15 AND 19 ) AS sumba,
(SELECT count(ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND yrs BETWEEN 20 AND 24 ) AS sumbb,
(SELECT count(ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND yrs > 24 ) AS sumbc,

---- screened for tb
(SELECT count(ccc_no) FROM mike WHERE tb_screening = 't' AND yrs < 2 ) AS sumbd, 
(SELECT count(ccc_no) FROM mike WHERE tb_screening = 't' AND yrs BETWEEN 2 AND 9) AS sumbe, 
(SELECT count(ccc_no) FROM mike WHERE tb_screening = 't' AND yrs BETWEEN 10 AND 14 ) AS sumbf,
(SELECT count(ccc_no) FROM mike WHERE tb_screening = 't' AND yrs BETWEEN 15 AND 19 ) AS sumbg,
(SELECT count(ccc_no) FROM mike WHERE tb_screening = 't' AND yrs BETWEEN 20 AND 24 ) AS sumbh,
(SELECT count(ccc_no) FROM mike WHERE tb_screening = 't' AND yrs > 24 ) AS sumbi,


---- started on ipt
(SELECT count(ccc_no) FROM mike WHERE started_ipt = 't' AND yrs < 2 ) AS sumbj, 
(SELECT count(ccc_no) FROM mike WHERE started_ipt = 't' AND yrs BETWEEN 2 AND 9) AS sumbk, 
(SELECT count(ccc_no) FROM mike WHERE started_ipt = 't' AND yrs BETWEEN 10 AND 14 ) AS sumbl,
(SELECT count(ccc_no) FROM mike WHERE started_ipt = 't' AND yrs BETWEEN 15 AND 19 ) AS sumbm,
(SELECT count(ccc_no) FROM mike WHERE started_ipt = 't' AND yrs BETWEEN 20 AND 24 ) AS sumbn,
(SELECT count(ccc_no) FROM mike WHERE started_ipt = 't' AND yrs > 24 ) AS sumbo,

--enrolled in care
(SELECT count(ccc_no) FROM mike WHERE nutrition_accessed = 't' AND yrs < 15) AS sumbp,
(SELECT count(ccc_no) FROM mike WHERE nutrition_accessed = 't' AND yrs > 14) AS sumbq,
(SELECT count(ccc_no) FROM mike WHERE nutrition_malnorished = 't' AND yrs < 15) AS sumbr,
(SELECT count(ccc_no) FROM mike WHERE nutrition_malnorished = 't' AND yrs > 14) AS sumbs,
(SELECT count(ccc_no) FROM mike WHERE nutrition_food_prescribed = 't' AND yrs < 15) AS sumbt,
(SELECT count(ccc_no) FROM mike WHERE nutrition_food_prescribed = 't' AND yrs > 14) AS sumbu, 

--cancer
(SELECT count(ccc_no) FROM mike WHERE cancer_screening = 't' AND yrs > 14) AS sumbv,
(SELECT count(ccc_no) FROM mike WHERE yrs > 17 AND sex = 'female') AS sumbw,

-- SELECT ccc_no,visit_date,tca,age,extract(MONTH FROM AGE(tca,visit_date)) AS dmnth FROM dar WHERE DATE_PART('MONTH',tca) = 12  and DATE_PART('YEAR',tca) = 2015



---------------------------------------------------------aggreagate two's-----------------------------------------------------

---- in art revist
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND yrs < 2 AND dur_mnths = 2) AS dam,
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND yrs BETWEEN 2 AND 9 AND dur_mnths = 2) AS dan, 
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'male' AND yrs BETWEEN 10 AND 14 AND dur_mnths = 2) AS dao, 
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'female' AND yrs BETWEEN 10 AND 14 AND dur_mnths = 2) AS dap, 
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'male' AND yrs BETWEEN 15 AND 19 AND dur_mnths = 2) AS daq, 
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'female' AND yrs BETWEEN 15 AND 19 AND dur_mnths = 2) AS dar,
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'male' AND yrs BETWEEN 20 AND 24 AND dur_mnths = 2) AS dass, 
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'female' AND yrs BETWEEN 20 AND 24 AND dur_mnths = 2) AS dat,
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'male' AND yrs > 24 AND dur_mnths = 2) AS dau, 
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'female' AND yrs > 24 AND dur_mnths = 3) AS dav, 
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND  enrolled_in_care = 't' AND key_pop = 't' AND dur_mnths = 2) AS daw,


---------------------------------------------------------aggreagate three's-----------------------------------------------------

(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND yrs < 2 AND dur_mnths = 3) AS tam,
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND yrs BETWEEN 2 AND 9 AND dur_mnths = 3) AS tan, 
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'male' AND yrs BETWEEN 10 AND 14 AND dur_mnths = 3) AS tao, 
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'female' AND yrs BETWEEN 10 AND 14 AND dur_mnths = 3) AS tap, 
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'male' AND yrs BETWEEN 15 AND 19 AND dur_mnths = 3) AS taq, 
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'female' AND yrs BETWEEN 15 AND 19 AND dur_mnths = 3) AS tar,
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'male' AND yrs BETWEEN 20 AND 24 AND dur_mnths = 3) AS tass, 
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'female' AND yrs BETWEEN 20 AND 24 AND dur_mnths = 3) AS tat,
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'male' AND yrs > 24 AND dur_mnths = 3) AS tau, 
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND sex = 'female' AND yrs > 24 AND dur_mnths = 3) AS tav, 
(SELECT count(ccc_no) FROM mike WHERE visit_type = 'n' AND enrolled_in_care = 'f' AND prophylaxis IS NOT NULL AND  enrolled_in_care = 't' AND key_pop = 't' AND dur_mnths = 3) AS taw



 from mike
";
			//$data['mydata'] = $this->db->query($sql);
			//$this->load->view('mohdar_new',$data);

			$data['mydata'] = $this->db->query($sql);	
			$data['title'] = "MOH Daily Activity Register (366) 2015";
			$mycon = $this->load->view('mohdar_new',$data,TRUE);
			$data['zz'] = base64_encode(gzdeflate(serialize($mycon)));
			$this->load->view('mohdar_new',$data);
			$this->load->view('export',$data);
			
			
	
			
		}
						
		
	}

	
}
