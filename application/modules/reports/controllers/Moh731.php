<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Moh731 extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->pbscms->_check_logged();
		$this->form_validation->set_error_delimiters('<div class="ferror">', '</div>');
		//session_start();
	}
	
	

	

	public function index()
	{
		

		$this->form_validation->set_rules('mntyr','Month Year','trim|required');
				
		if ($this->form_validation->run() == FALSE){ 
			
			$data['title'] = "MOH 366 Register";
			$data['content'] = $this->load->view('mnt_yr',$data,TRUE);
			$this->load->view('template',$data);

			
		}else{

			if($this->input->post('ftype') ){				
				$this->pbscms->_export($this->input->post('zz'),$this->input->post('ftype'),$this->input->post('title')." ".$this->input->post('full_date'),"portrait","a4");
			}

			$var = explode(" ",$this->input->post('mntyr'));
			$mnt = $var[0];
			$yr = $var[1];
			$siku = "'01-$mnt-$yr'::DATE";

			$data['full_date'] = date('F', mktime(0, 0, 0, $mnt, 10)). " ".$yr;
			
			$sqlf =	"
				SELECT 
				EXTRACT (MONTH FROM('01-12-2015'::DATE - INTERVAL '1 MONTH')) AS twos, 
				EXTRACT (MONTH FROM('01-12-2015'::DATE - INTERVAL '2 MONTH')) AS threes
				";		
				$dd = $this->db->query($sqlf)->row();
				$twos = $dd->twos;
				$threes = $dd->threes;
			//print_r($data['full_date']); die();
			//SELECT (date_trunc('MONTH', $1) + INTERVAL '1 MONTH - 1 day')::DATE;
			$sql = "
				WITH mike AS (
				SELECT
				m.*, EXTRACT (MONTH FROM AGE(tca,visit_date)) AS dur_mnths,
				EXTRACT (MONTH FROM visit_date) AS mnths,
				--to_char(visit_date,'mon') AS mnths

(SELECT l.tb_screening FROM dar l WHERE l.ccc_no = m.ccc_no ORDER BY l.visit_date DESC LIMIT 1) AS last_tb_screen,
				DATE_PART('MONTH',visit_date) AS mntpart				
				FROM dar m
				WHERE m.visit_date
				BETWEEN ($siku - INTERVAL '2 MONTH') AND (DATE_TRUNC('MONTH', $siku) + INTERVAL '1 MONTH - 1 day')::DATE
				AND m.tca >= $siku
				
				)
				SELECT 
				-- (DATE_TRUNC('MONTH', $siku) + INTERVAL '1 MONTH - 1 day')::DATE  
				--(SELECT COUNT(DISTINCT ccc_no) FROM mike ) AS ca				
				--(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE DATE_PART('MONTH',visit_date)= $mnt ) AS ca
--select distinct on(ccc_no)ccc_no,visit_date,tca from dar where prophylaxis is not null and visit_date between '01-10-2015' and '31-12-2015' and tca >= '01-12-2015'

--------------------------------------------------------3.1 On Cotrimoxazole Prophylaxis
--prophylaxis IS NOT NULL AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  ) AS ctx_ca, 
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  and sex='male' and age > 14 ) AS ctx_am, 
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  and sex='male' and age < 15 ) AS ctx_cm,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  and sex='female' and age > 14 ) AS ctx_af, 
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  AND sex='female' and age < 15 ) AS ctx_cf, 

---------------------------------------------------------------- 3.3 Currently in Care
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  ) AS curr_care_ca,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  AND sex='male' AND age > 14 ) AS curr_care_am,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  AND sex='male' and age < 15  ) AS curr_care_cm,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  AND sex='female' and age > 14 ) AS curr_care_af,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  AND sex='female' and age < 15 ) AS curr_care_cf,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  AND age < 1 ) AS curr_care_ci,


-------------------------------------------------------------------3.4
(SELECT COUNT(distinct ccc_no) FROM mike WHERE started_on_art='t')AS start_art_ca, 
(SELECT COUNT(distinct ccc_no)  FROM mike WHERE sex='male' and started_on_art='t' AND age > 14  ) AS start_art_am,
(SELECT COUNT(distinct ccc_no)  FROM mike WHERE sex='male' and started_on_art='t' AND age < 15  ) AS start_art_cm,
(SELECT COUNT(distinct ccc_no)  FROM mike WHERE sex='female' and started_on_art='t' AND age > 14  ) AS start_art_af,
(SELECT COUNT(distinct ccc_no)  FROM mike WHERE sex='female' and started_on_art='t' AND age < 15  ) AS start_art_cf,
(SELECT COUNT(distinct ccc_no)  FROM mike WHERE started_on_art='t'AND age < 1  ) AS start_art_ci,
(SELECT COUNT(distinct ccc_no)  FROM mike WHERE sex='female' and pregnant = 't' AND visit_type='n') AS start_art_pg,
(SELECT COUNT(distinct ccc_no)  FROM mike WHERE tb_tx = 't') AS start_art_tb,

-------------------------------------------------------------------3.6
(SELECT COUNT(distinct ccc_no)  FROM mike WHERE pre_art = 'f' AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) ) ) AS curr_art_ca,
(SELECT COUNT(distinct ccc_no)  FROM mike WHERE pre_art = 'f' and sex='male' and age > 14  AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  ) AS curr_art_am,
(SELECT COUNT(distinct ccc_no)  FROM mike WHERE pre_art = 'f' and sex='male' and age < 15  AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  ) AS curr_art_cm,
(SELECT COUNT(distinct ccc_no)  FROM mike WHERE pre_art = 'f' and sex='female' and age > 14  AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  ) AS curr_art_af,
(SELECT COUNT(distinct ccc_no)  FROM mike WHERE pre_art = 'f' and sex='female' and age < 15  AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  ) AS curr_art_cf,
(SELECT COUNT(distinct ccc_no)  FROM mike WHERE pre_art = 'f' and  age < 1  AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  ) AS curr_art_ci,

-------------------------------------------------------------------3.2

(SELECT COUNT(ccc_no) FROM mike WHERE enrolled_in_care = 't' ) AS in_care_ca, 
(SELECT COUNT(ccc_no)  FROM mike WHERE sex='male' AND enrolled_in_care = 't' AND age > 14  ) AS in_care_am,
(SELECT COUNT(ccc_no)  FROM mike WHERE sex='male' AND enrolled_in_care = 't' AND age < 15  ) AS in_care_cm,
(SELECT COUNT(ccc_no)  FROM mike WHERE sex='female' AND enrolled_in_care = 't' AND age > 14  ) AS in_care_af,
(SELECT COUNT(ccc_no)  FROM mike WHERE sex='female' AND enrolled_in_care = 't' AND age < 15  ) AS in_care_cf,
(SELECT COUNT(ccc_no)  FROM mike WHERE enrolled_in_care = 't'  AND age < 1  ) AS in_care_ci,

------------------------------------------------------------------3.5

(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND started_on_art != 't' AND pre_art != 't'  AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  ) AS art_rev_ca,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND started_on_art != 't' AND pre_art != 't' AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  AND sex='male' AND age > 14 ) AS art_rev_am,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND started_on_art != 't' AND pre_art != 't' AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  AND sex='male' and age < 15  ) AS art_rev_cm,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND started_on_art != 't' AND pre_art != 't' AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  AND sex='female' and age > 14 ) AS art_rev_af,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND started_on_art != 't' AND pre_art != 't' AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  AND sex='female' and age < 15 ) AS art_rev_cf,
(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE prophylaxis IS NOT NULL AND started_on_art != 't' AND pre_art != 't' AND ( (mntpart = $mnt) OR (mntpart = $twos AND dur_mnths > 1) OR (mntpart = $threes AND dur_mnths > 2) )  AND age < 1 ) AS art_rev_ci,

-------------------------------------------------------------------3.9

(SELECT COUNT( ccc_no)  FROM mike WHERE  mnths = $mnt AND tb_screening = 't')AS screen_tb_ca, 
(SELECT COUNT( ccc_no)  FROM mike WHERE  mnths = $mnt AND  tb_screening = 't' AND sex='male' and age > 14 ) AS screen_tb_am,
(SELECT COUNT( ccc_no)  FROM mike WHERE  mnths = $mnt AND  tb_screening = 't' AND sex='male' and age < 15 ) AS screen_tb_cm,
(SELECT COUNT( ccc_no)  FROM mike WHERE  mnths = $mnt AND  tb_screening = 't' AND sex='female' and age > 14 ) AS screen_tb_af,
(SELECT COUNT( ccc_no)  FROM mike WHERE  mnths = $mnt AND  tb_screening = 't' AND sex='female' and age < 15 ) AS screen_tb_cf,

-------------------------------------------------------------------3.10

(SELECT COUNT(distinct ccc_no) FROM mike WHERE pwp_contraceptive = 't') AS modern_fp, 
(SELECT COUNT(distinct ccc_no)  FROM mike WHERE pwp_condoms = 't') AS condom_fp,


-------------------------------------------------------------------3.11
(SELECT COUNT(ccc_no) FROM mike WHERE mnths = $mnt )AS ttl_visit, 
(SELECT COUNT(ccc_no)  FROM mike WHERE mnths = $mnt   AND sex='female' and age > 17  ) AS af_visits,
(SELECT COUNT(ccc_no)  FROM mike WHERE mnths = $mnt   AND   scheduled_visit = 'f'  ) AS us_visit,
(SELECT COUNT(ccc_no)  FROM mike WHERE mnths = $mnt   AND  scheduled_visit = 't'  ) AS s_visit


		
FROM mike
";

			//////////////////////////////3.1 On Cotrimoxazole Prophylaxis

			$care = $this->db->query($sql)->row_array();
			  
			$data['ctx_ca'] = $care['ctx_ca'];
			$data['ctx_cf'] = $care['ctx_cf'];
			$data['ctx_cm'] = $care['ctx_cm'];
			$data['ctx_af'] = $care['ctx_af'];
			$data['ctx_am'] = $care['ctx_am'];
			
			
			////////////////////////////////////////// 3.3
    			
			
			$data['curr_care_ca'] = $care['curr_care_ca'];
			$data['curr_care_cf'] = $care['curr_care_cf'];
			$data['curr_care_cm'] = $care['curr_care_cm'];
			$data['curr_care_af'] = $care['curr_care_af'];
			$data['curr_care_am'] = $care['curr_care_am'];
			$data['curr_care_ci'] = $care['curr_care_ci'];

			////////////////////////////////////////// 3.4
			$data['start_art_ca'] = $care['start_art_ca'];
			$data['start_art_am'] = $care['start_art_am'];
			$data['start_art_af'] = $care['start_art_af'];
			$data['start_art_cm'] = $care['start_art_cm'];
			$data['start_art_cf'] = $care['start_art_cf'];
			$data['start_art_ci'] = $care['start_art_ci'];
			$data['start_art_pg'] = $care['start_art_pg'];
			$data['start_art_tb'] = $care['start_art_tb'];

			////////////////////////////////////////// 3.6
			
			$data['curr_art_ca'] = $care['curr_art_ca'];
			$data['curr_art_cf'] = $care['curr_art_cf'];
			$data['curr_art_cm'] = $care['curr_art_cm'];
			$data['curr_art_af'] = $care['curr_art_af'];
			$data['curr_art_am'] = $care['curr_art_am'];
			$data['curr_art_ci'] = $care['curr_art_ci'];

			////////////////////////////////////////// 3.2

			$data['in_care_ca'] = $care['in_care_ca'];
			$data['in_care_cm'] = $care['in_care_cm'];
			$data['in_care_cf'] = $care['in_care_cf'];
			$data['in_care_af'] = $care['in_care_af'];
			$data['in_care_am'] = $care['in_care_am'];
			$data['in_care_ci'] = $care['in_care_ci'];


			/////////////////////////////////////////////////// 3.5 

			$data['art_rev_ca'] = $care['art_rev_ca'];
			$data['art_rev_am'] = $care['art_rev_am'];
			$data['art_rev_af'] = $care['art_rev_af'];
			$data['art_rev_cm'] = $care['art_rev_cm'];
			$data['art_rev_cf'] = $care['art_rev_cf'];
			$data['art_rev_ci'] = $care['art_rev_ci'];

			////////////////////////////////////////// 3.9
			$data['screen_tb_ca'] = $care['screen_tb_ca'];
			$data['screen_tb_am'] = $care['screen_tb_am'];
			$data['screen_tb_af'] = $care['screen_tb_af'];
			$data['screen_tb_cm'] = $care['screen_tb_cm'];
			$data['screen_tb_cf'] = $care['screen_tb_cf'];

			//////////////////////////////////////////////////// 3.10			

			$data['modern_fp'] = $care['modern_fp'];
			$data['condom_fp'] = $care['condom_fp'];

			///////////////////////////////////////////////// 3.11
		
			$data['ttl_visit'] = $care['ttl_visit'];
			$data['af_visits'] = $care['af_visits'];
			$data['us_visit'] = $care['us_visit'];
			$data['s_visit'] = $care['s_visit'];

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				

			$data['title'] = "MOH 731 Report";
			
			//print_r($data); die();
			//foreach (array(1, 2, 3, 4) as &$value) {
			//    $value = $value * 2;
			//}
					

			//$mycon = $this->load->view('moh731',$data, TRUE);
			//$data['zz'] = base64_encode(gzdeflate(serialize($mycon)));
			
			//$data['content'] = $this->load->view('moh731',$data, TRUE);
			//$this->load->view('template',$data);
		
			$data['title'] = "MOH 731 Report";
			$mycon = $this->load->view('moh731',$data,TRUE);
			$data['zz'] = base64_encode(gzdeflate(serialize($mycon)));
			$data['content'] = $this->load->view('moh731',$data,TRUE);
			$data['content'] .= $this->load->view('export',$data,TRUE);
			$mycon = $this->load->view('moh731',$data,TRUE);

			$this->load->view('template',$data);
			
		}
						
		
	}
	

	
}
