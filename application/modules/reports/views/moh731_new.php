<style type="text/css">
<!--
.b {font-weight:bold;}
.c{text-align:center}
-->
</style>

<table width="100%" border="1" cellpadding="0" cellspacing="0" style="font-size:12px; border-collapse:collapse; ">
  <tr>
    <td colspan="5" class="b c">National AIDS &amp; STI Control Programme</td>
  </tr>
  <tr>
    <td colspan="5" class="b c">MOH 731- Comprehensive HIV/AIDS Facility Reporting Form - NASCOP</td>
  </tr>
  <tr>
    <td colspan="5">&nbsp;</td>
  </tr>
  <tr>
    <td class="b">Sub County: </td>
    <td><?=$this->session->userdata('district')?></td>
    <td>&nbsp;</td>
    <td class="b">County:</td>
    <td><?=$this->session->userdata('county')?></td>
  </tr>
  <tr>
    <td class="b">Month-Year:</td>
    <td><span class="b">
      <?=$full_date?>
    </span></td>
    <td>&nbsp;</td>
    <td  class="b">MFL Code:</td>
    <td><?=$this->session->userdata('mfl_code')?></td>
  </tr>
  <tr>
    <td colspan="5"><span class="b">3 Care and Treatment</span></td>
  </tr>
  <tr>
    <td colspan="5"><span class="b">3.1 Enrollment in Care </span></td>
  </tr>
  <tr>
    <td> Enrolled &lt; 2 years </td>
    <td>HV03-01</td>
    <td><?=$hv03_001?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Enrolled 2 - 9 years </td>
    <td>HV03-02</td>
    <td><?=$hv03_002?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="22">Enrolled 10 - 14 years </td>
    <td>HV03-03 (M)</td>
    <td><?=$hv03_003?></td>
    <td>HV03-04 (F)</td>
    <td><?=$hv03_004?></td>
  </tr>
  <tr>
    <td>Enrolled 15 - 19 years </td>
    <td>HV03-05 (M)</td>
    <td><?=$hv03_005?></td>
    <td>HV03-06 (F)</td>
    <td><?=$hv03_006?></td>
  </tr>
  <tr>
    <td>Enrolled 20 - 24 years </td>
    <td>HV03-07 (M)</td>
    <td><?=$hv03_007?></td>
    <td>HV03-08 (F)</td>
    <td><?=$hv03_008?></td>
  </tr>
  <tr>
    <td>Enrolled 25+ years </td>
    <td>HV03-09 (M)</td>
    <td><?=$hv03_009?></td>
    <td>HV03-10 (F)</td>
    <td><?=$hv03_010?></td>
  </tr>
  <tr class="b">
    <td>Enrolled Total</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>HV03-11</td>
    <td><?=$hv03_011?></td>
  </tr>
  <tr class="">
    <td>Enrolled In Care_KeyPop </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>HV03-12</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="5"><span class="b">3.2 Revisits in Care </span></td>
  </tr>
  <tr>
    <td> In Care Revisits ART < 2 years </td>
    <td>HV03-013</td>
    <td bgcolor="#B5B5B5">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>In Care Revisits ART 2 - 9 years </td>
    <td>HV030-14</td>
    <td bgcolor="#B5B5B5">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="22">In Care  Revisits ART 10 - 14 years </td>
    <td>HV03-015 (M)</td>
    <td bgcolor="#B5B5B5">&nbsp;</td>
    <td>HV03-016 (F)</td>
    <td bgcolor="#B5B5B5">&nbsp;</td>
  </tr>
  <tr>
    <td>In Care Revisits  ART 15 - 19 years </td>
    <td>HV03-017 (M)</td>
    <td bgcolor="#B5B5B5">&nbsp;</td>
    <td>HV03-018 (F)</td>
    <td bgcolor="#B5B5B5">&nbsp;</td>
  </tr>
  <tr>
    <td>In Care  Revisits ART 20 - 24 years </td>
    <td>HV03-019 (M)</td>
    <td bgcolor="#B5B5B5">&nbsp;</td>
    <td>HV03-020 (F)</td>
    <td bgcolor="#B5B5B5">&nbsp;</td>
  </tr>
  <tr>
    <td>In Care  Revisits ART 25+ years </td>
    <td>HV03-021 (M)</td>
    <td bgcolor="#B5B5B5">&nbsp;</td>
    <td>HV03-022 (F)</td>
    <td bgcolor="#B5B5B5">&nbsp;</td>
  </tr>
  <tr class="b">
    <td>In Care_Revisits_TOTAL (Sum HV03-03 to HV03-06)</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>HV03-023</td>
    <td bgcolor="#B5B5B5">&nbsp;</td>
  </tr>
  <tr class="">
    <td>In Care Revisits  ART_KeyPop </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>HV03-024</td>
    <td bgcolor="#B5B5B5">&nbsp;</td>
  </tr><tr>
    <td colspan="5"><span class="b">3.3 Currently in Care </span></td>
  </tr>
  <tr>
    <td> In Care ART &lt; 2 years </td>
    <td>HV03-025</td>
    <td><?=$hv03_025?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>In Care ART 2 - 9 years </td>
    <td>HV03-026</td>
    <td><?=$hv03_026?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="22">In Care ART 10 - 14 years </td>
    <td>HV03-027 (M)</td>
    <td><?=$hv03_027?></td>
    <td>HV03-028 (F)</td>
    <td><?=$hv03_028?></td>
  </tr>
  <tr>
    <td>In Care ART 15 - 19 years </td>
    <td>HV03-029 (M)</td>
    <td><?=$hv03_029?></td>
    <td>HV03-030 (F)</td>
    <td><?=$hv03_030?></td>
  </tr>
  <tr>
    <td>In Care ART 20 - 24 years </td>
    <td>HV03-031 (M)</td>
    <td><?=$hv03_031?></td>
    <td>HV03-032 (F)</td>
    <td><?=$hv03_032?></td>
  </tr>
  <tr>
    <td>In Care ART 25+ years </td>
    <td>HV03-033 (M)</td>
    <td><?=$hv03_033?></td>
    <td>HV03-034 (F)</td>
    <td><?=$hv03_034?></td>
  </tr>
  <tr class="b">
    <td>In Care_TOTAL (Sum HV03-03 to HV03-06)</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>HV03-035</td>
    <td><?=$hv03_035?></td>
  </tr>
  <tr class="">
    <td>In Care ART_KeyPop </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>HV03-036</td>
    <td><?=$hv03_036?></td>
  </tr>
  <tr>
    <td colspan="5"><span class="b">3.4 Starting ART</span></td>
  </tr>
  <tr>
    <td> Start ART &lt; 2 years </td>
    <td>HV03-037</td>
    <td><?=$hv03_037?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Start ART 2 - 9 years </td>
    <td>HV03-038</td>
    <td><?=$hv03_038?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="22">Start ART 10 - 14 years </td>
    <td>HV03-039 (M)</td>
    <td><?=$hv03_039?></td>
    <td>HV03-040 (F)</td>
    <td><?=$hv03_040?></td>
  </tr>
  <tr>
    <td>Start  ART 15 - 19 years </td>
    <td>HV03-041 (M)</td>
    <td><?=$hv03_041?></td>
    <td>HV03-042 (F)</td>
    <td><?=$hv03_042?></td>
  </tr>
  <tr>
    <td>Start ART 20 - 24 years </td>
    <td>HV03-043 (M)</td>
    <td><?=$hv03_043?></td>
    <td>HV03-044 (F)</td>
    <td><?=$hv03_044?></td>
  </tr>
  <tr>
    <td>Start ART 25+ years </td>
    <td>HV03-045 (M)</td>
    <td><?=$hv03_045?></td>
    <td>HV03-046 (F)</td>
    <td><?=$hv03_046?></td>
  </tr>
  <tr class="b">
    <td height="18">Start ART_TOTAL (Sum HV03-03 to HV03-06)</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>HV03-047</td>
    <td><?=$hv03_047?></td>
  </tr>
  <tr class="">
    <td>Start  ART_TB Patient </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>HV03-048</td>
    <td><?=$hv03_048?></td>
  </tr>
  
  <tr>
    <td>Start  ART_Discordant </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>HV03-049</td>
    <td><?=$hv03_049?></td>
  </tr>
  <tr>
    <td>Start  ART_KeyPop </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>HV03-050</td>
    <td><?=$hv03_050?></td>
  </tr>
  <tr>
    <td colspan="5"><span class="b">3.5 Revisits on ART</span></td>
  </tr>
  <tr>
    <td>  Revisits On ART &lt; 2 years </td>
    <td>HV03-01</td>
    <td bgcolor="#B5B5B5">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td> Revisits On ART 2 - 9 years </td>
    <td>HV03-02</td>
    <td bgcolor="#B5B5B5">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="22">  Revisits On ART 10 - 14 years </td>
    <td>HV03-03 (M)</td>
    <td bgcolor="#B5B5B5">&nbsp;</td>
    <td>HV03-04 (F)</td>
    <td bgcolor="#B5B5B5">&nbsp;</td>
  </tr>
  <tr>
    <td> Revisits  On ART 15 - 19 years </td>
    <td>HV03-05 (M)</td>
    <td bgcolor="#B5B5B5">&nbsp;</td>
    <td>HV03-06 (F)</td>
    <td bgcolor="#B5B5B5">&nbsp;</td>
  </tr>
  <tr>
    <td>  Revisits On ART 20 - 24 years </td>
    <td>HV03-07 (M)</td>
    <td bgcolor="#B5B5B5">&nbsp;</td>
    <td>HV03-08 (F)</td>
    <td bgcolor="#B5B5B5">&nbsp;</td>
  </tr>
  <tr>
    <td>  Revisits On ART 25+ years </td>
    <td>HV03-09 (M)</td>
    <td bgcolor="#B5B5B5">&nbsp;</td>
    <td>HV03-10 (F)</td>
    <td bgcolor="#B5B5B5">&nbsp;</td>
  </tr>
  <tr class="b">
    <td>Revisits_TOTAL (Sum HV03-03 to HV03-06)</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>HV03-10 (F)</td>
    <td bgcolor="#B5B5B5">&nbsp;</td>
  </tr>
  <tr class="">
    <td> Revisits  On ART_KeyPop </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>HV03-12 (F)</td>
    <td bgcolor="#B5B5B5">&nbsp;</td>
  </tr>
  
  <tr>
    <td colspan="5"><span class="b">3.6 Currently on ART [All]</span></td>
  </tr>
  <tr>
    <td> Revisits On ART &lt; 2 years </td>
    <td>HV03-01</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td> Revisits On ART 2 - 9 years </td>
    <td>HV03-02</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="22"> Revisits On ART 10 - 14 years </td>
    <td>HV03-03 (M)</td>
    <td>&nbsp;</td>
    <td>HV03-04 (F)</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td> Revisits  On ART 15 - 19 years </td>
    <td>HV03-05 (M)</td>
    <td>&nbsp;</td>
    <td>HV03-06 (F)</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td> Revisits On ART 20 - 24 years </td>
    <td>HV03-07 (M)</td>
    <td>&nbsp;</td>
    <td>HV03-08 (F)</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td> Revisits On ART 25+ years </td>
    <td>HV03-09 (M)</td>
    <td>&nbsp;</td>
    <td>HV03-10 (F)</td>
    <td>&nbsp;</td>
  </tr>
  <tr class="b">
    <td>Revisits_TOTAL (Sum HV03-03 to HV03-06)</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>HV03-10 (F)</td>
    <td>&nbsp;</td>
  </tr>
  <tr class="">
    <td> Revisits  On ART_KeyPop </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>HV03-12 (F)</td>
    <td>&nbsp;</td>
  </tr>
  
   <tr>
     <td colspan="5"><span class="b">3.7 On CTX /Dapsone</span></td>
   </tr>
   <tr>
     <td> On CTX/DDS &lt; 2 years </td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>HV03-075</td>
     <td><?=$hv03_075?></td>
   </tr>
   <tr>
     <td> On CTX/DDS 2 - 9 years </td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>HV03-076</td>
     <td><?=$hv03_076?></td>
   </tr>
   <tr>
     <td height="22"> On CTX/DDS 10 - 14 years </td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>HV03-077</td>
     <td><?=$hv03_077?></td>
   </tr>
   <tr>
     <td> On  CTX/DDS 15 - 19 years </td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>HV03-078</td>
     <td><?=$hv03_078?></td>
   </tr>
   <tr>
     <td> On CTX/DDS 20 - 24 years </td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>HV03-079</td>
     <td><?=$hv03_079?></td>
   </tr>
   <tr>
     <td> On CTX/DDS 25+ years </td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>HV03-080</td>
     <td><?=$hv03_080?></td>
   </tr>
   <tr class="b">
     <td>CTX/DDS_TOTAL (Sum HV03-03 to HV03-06)</td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>HV03-081</td>
     <td><?=$hv03_081?></td>
   </tr>
   <tr>
     <td colspan="5"><span class="b">3.8 TB Screening </span></td>
   </tr>
   <tr>
     <td> Screen for TB &lt; 2 years </td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>HV03-082</td>
     <td><?=$hv03_082?></td>
   </tr>
   <tr>
     <td> Screen for TB 2 - 9 years </td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>HV03-083</td>
     <td><?=$hv03_083?></td>
   </tr>
   <tr>
     <td height="22"> Screen for TB 10 - 14 years </td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>HV03-084</td>
     <td><?=$hv03_084?></td>
   </tr>
   <tr>
     <td> Screen for TB 15 - 19 years </td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>HV03-085</td>
     <td><?=$hv03_085?></td>
   </tr>
   <tr>
     <td> Screen for TB 20 - 24 years </td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>HV03-086</td>
     <td><?=$hv03_086?></td>
   </tr>
   <tr>
     <td> Screen for TB 25+ years </td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>HV03-087</td>
     <td><?=$hv03_087?></td>
   </tr>
   <tr class="b">
     <td>Screen for TB_TOTAL (Sum HV03-03 to HV03-06)</td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>HV03-088</td>
     <td><?=$hv03_088?></td>
   </tr>
   
   <tr>
     <td colspan="5"><span class="b">3.9 Start IPT </span></td>
   </tr>
   <tr>
     <td> Start IPT &lt; 2 years </td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>HV03-089</td>
     <td><?=$hv03_089?></td>
   </tr>
   <tr>
     <td> Start IPT 2 - 9 years </td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>HV03-090</td>
     <td><?=$hv03_090?></td>
   </tr>
   <tr>
     <td height="22"> Start IPT 10 - 14 years </td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>HV03-091</td>
     <td><?=$hv03_091?></td>
   </tr>
   <tr>
     <td> Start IPT 15 - 19 years </td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>HV03-092</td>
     <td><?=$hv03_092?></td>
   </tr>
   <tr>
     <td> Start IPT 20 - 24 years </td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>HV03-093</td>
     <td><?=$hv03_093?></td>
   </tr>
   <tr>
     <td> Start IPT 25+ years </td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>HV03-094</td>
     <td><?=$hv03_094?></td>
   </tr>
   <tr class="b">
     <td>Start IPT_TOTAL (Sum HV03-03 to HV03-06)</td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>HV03-095</td>
     <td><?=$hv03_095?></td>
   </tr>
   
   
   <tr>
    <td colspan="5"><span class="b">3.10 Survival and Retention on ART at 12 months</span></td>
  </tr>
   <tr>
     <td>On ART  12 months</td>
     <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>HV03-096</td>
    <td bgcolor="#B5B5B5">&nbsp;</td>
  </tr>
   <tr>
     <td> Net Cohort at 12 months</td>
     <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>HV03-097</td>
    <td bgcolor="#B5B5B5">&nbsp;</td>
  </tr>
   <tr>
     <td>VL Suppressed 12 months </td>
     <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>HV03-098</td>
    <td bgcolor="#B5B5B5">&nbsp;</td>
  </tr>
   <tr>
     <td>VL Results  at 12 months</td>
     <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>HV03-099</td>
    <td bgcolor="#B5B5B5">&nbsp;</td>
  </tr>
   <tr>
    <td colspan="5"><span class="b">3.11 Nutrition and HIV</span></td>
  </tr>
   <tr>
     <td>Nutrition Assess &lt; 15 years </td>
     <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>HV03-100</td>
    <td><?=$hv03_100?></td>
  </tr>
   <tr>
     <td>Nutrition Assess - 15+ years</td>
     <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>HV03-101</td>
    <td><?=$hv03_101?></td>
  </tr>
   <tr>
     <td>Nutrition Assess_TOTAL</td>
     <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>HV03-102</td>
    <td><?=$hv03_102?></td>
  </tr>
   <tr>
     <td>Malnourished &lt; 15 years </td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>HV03-103</td>
     <td><?=$hv03_103?></td>
   </tr>
   <tr>
     <td>Malnourished - 15+ years</td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>HV03-104</td>
     <td><?=$hv03_104?></td>
   </tr>
   <tr>
     <td>Malnourished_TOTAL</td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>HV03-105</td>
     <td><?=$hv03_105?></td>
   </tr>
   
   <tr>
     <td>Food Provided &lt; 15 years </td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>HV03-106</td>
     <td><?=$hv03_106?></td>
   </tr>
   <tr>
     <td>Food Provided - 15+ years</td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>HV03-107</td>
     <td><?=$hv03_107?></td>
   </tr>
   <tr>
     <td>Food Provided_TOTAL</td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td>HV03-108</td>
     <td><?=$hv03_108?></td>
   </tr>
   
  <tr>
    <td colspan="5"><span class="b">3.11 HIV Care Visits</span></td>
  </tr>
  <tr>
    <td>Cervical Cancer Screening F_18+_New</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>HV03-111</td>
    <td><?=$hv03_111?></td>
  </tr>
  <tr>
    <td>Clinical Visits 18+ </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>HV03-112</td>
    <td><?=$hv03_112?></td>
  </tr>
</table>



