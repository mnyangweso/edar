<style>
.bb{
font-weight:bold;
font-family:  "Univers LT 57 Condensed",Garamond,GlasgowSerial-Heavy,Eras-Bold,"Chianti BT", "Chianti XBd BT"; font-size:11px;
}
.b{font-weight:bold}
.tr{color: #d9edf7;	background-color:#5bc0de;  }
.tda{background:#e7f5f9}
.tdb{background-color:#d0e6ef; }
.odd{background:#f0eff0}
.even{background:#e2e3ea}
.style1 {color: #666666;font-weight: bold;}
.style2 {color: #666666}
html{margin:5px;}
</style>
<link href="<?=base_url()?>assets/css/forms.css" rel="stylesheet" type="text/css" media="all">
<html style="margin:5px">
<form method="post" id="mike">
<table width="100%" border="1" cellpadding="2" cellspacing="2" frame="box" style="background-color:#FFFFFF; border-collapse:collapse;font-size:10px; ">
  <tr class="bb">
    <td colspan="42" align="center" class="bb" >ACTIVITY SHEET: HIV - Care and Treatment - MOH 366</td>
  </tr>
  <tr class="bb">
    <td colspan="7"><span class="b">County:</span>    <?=$this->session->userdata('county')?></td>
    <td colspan="7">Sub County: 
    <?=$this->session->userdata('district')?></td>
    <td colspan="7">Facility Name: <span class="b">
      <?=$this->session->userdata('mfl_name')?>
    </span></td>
    <td colspan="7">Period: <span class="b">
      <?=$full_date?>
    </span></td>
    <td colspan="7">MFL Code: 
    <?=$this->session->userdata('mfl_code')?></td>
    <td colspan="7">Site ID: 
    <?=$this->session->userdata('site_id')?></td>
  </tr>
  <tr class="bb">
    <td colspan="18" rowspan="2" class="bb"></td>
    <td colspan="2" rowspan="2" class="bb">      No. of Months</td>
    <td colspan="7" rowspan="2" class="bb"></td>
    <td colspan="5" class="bb">Continuation on Therapy (in months)</td>
    <td rowspan="2" class="bb"></td>
    <td colspan="9" class="bb"></td>
  </tr>
  <tr class="style1">
    <td colspan="5" class="bb">(On ART/no drugs="0"; 1 mth=1; 2 mths=2; etc)</td>
    <td colspan="5" class="bb">Screening</td>
    <td colspan="4" class="bb">PwP Services Offered</td>
  </tr>
  <tr class="bb">
    <td>No</td>
    <td>Visit Date </td>
    <td>Visit Type ( New / Repeat )</td>
    <td>CCC Number</td>
    <td>On CTX  Male (<15)</td>
    <td>On CTX  Male (15+)</td>
    <td>On CTX  Female (<15)</td>
    <td>On CTX  Female (15+)</td>
    <td>Enrolled in Care  (<1)</td>
    <td>Enrolled in Care  Male (<15)</td>
    <td>Enrolled in Care  Female (<15)</td>
    <td>Enrolled in Care  Male (15+)</td>
    <td>Enrolled in Care  Female (15+)</td>
    <td>Revisit in Care  (<1)</td>
    <td>Revisit in Care  Male (<15)</td>
    <td>Revisit in Care  Female (<15)</td>
    <td>Revisit in Care  Male (15+)</td>
    <td>Revisit in Care  Female (15+)</td>
    <td>CTX is dispensed</td>
    <td>Before Next HIV Care Appointment</td>
    <td>Start ART  (<1)</td>
    <td>Start ART  Male (<15)</td>
    <td>Start ART  Female (<15)</td>
    <td>Start ART  Male (15+)</td>
    <td>Start ART  Female (15+)</td>
    <td>Start ART  Pregnant</td>
    <td>Start ART - TB/HIV</td>
    <td>Revisit - (<1)</td>
    <td>Revisit - Male (<15)</td>
    <td>Revisit - Female (<15)</td>
    <td>Revisit - Male (15+)</td>
    <td>Revisit - Female (15+)</td>
    <td>Enter number of months HAART dispensed</td>
    <td>TB - Male (<15)</td>
    <td>TB - Female (<15)</td>
    <td>TB - Male (15+)</td>
    <td>TB- Female (15+) </td>
    <td>Cervical Cancer - Female (18+)</td>
    <td>Modern Contraceptive</td>
    <td>Provided with Condoms</td>
    <td>Females (18+) </td>
    <td>Appointment Management </td>
  </tr>
  <tr class="style1">
    <td class="bb"></td>
    <td class="bb">&nbsp;</td>
    <td class="bb"></td>
    <td class="bb">(a)</td>
    <td class="bb">(b)</td>
    <td class="bb">(c)</td>
    <td class="bb">(d)</td>
    <td class="bb">(e)</td>
    <td class="bb">(f)</td>
    <td class="bb">(g)</td>
    <td class="bb">(h)</td>
    <td class="bb">(i)</td>
    <td class="bb">(j)</td>
    <td class="bb">(k)</td>
    <td class="bb">(l)</td>
    <td class="bb">(m)</td>
    <td class="bb">(n)</td>
    <td class="bb">(o)</td>
    <td class="bb">(p1)</td>
    <td class="bb">(p2)</td>
    <td class="bb">(q)</td>
    <td class="bb">(r)</td>
    <td class="bb">(s)</td>
    <td class="bb">(t)</td>
    <td class="bb">(u)</td>
    <td class="bb">(v)</td>
    <td class="bb">(w)</td>
    <td class="bb">(x)</td>
    <td class="bb">(y)</td>
    <td class="bb">(z)</td>
    <td class="bb">(aa)</td>
    <td class="bb">(ab)</td>
    <td class="bb">(ac)</td>
    <td class="bb">(ad)</td>
    <td class="bb">(ae)</td>
    <td class="bb">(af)</td>
    <td class="bb">(ag)</td>
    <td class="bb">(ah)</td>
    <td class="bb">(ai)</td>
    <td class="bb">(aj)</td>
    <td class="bb">(ak)</td>
    <td class="bb">(al)</td>
  </tr>
  <?php foreach($mydata->result() as $my): ?>
  <tr class="<?= alternator('tda', 'tdb'); ?>" >
    <td><?=$my->rn?></td> 
    <td><?=$my->visit_date?></td>
    <!-- a -->
	<td><?=strtoupper($my->visit_type)?></td>
	<td><?=$my->a?></td>
    <td><?=$my->b?></td>     <!-- b -->
    <td><?=$my->c?></td>    <!-- c -->
    <td><?=$my->d?></td>    <!-- d -->
    <td><?=$my->e?></td>    <!-- e -->
    <td><?=$my->f?></td>   <!-- f-->
    <td><?=$my->g?></td>  <!-- g -->
    <td><?=$my->h?></td>  <!-- h -->
    <td><?=$my->i?></td> 
     <td><?=$my->j?></td>
    <td><?=$my->k?></td>
    <td><?=$my->l?></td>
    <td><?=$my->m?></td>
    <td><?=$my->n?></td>
    <td><?=$my->o?></td>
    <td><?=$my->p1?></td>
    <td><?=$my->p2?></td>
    <td><?=$my->q?></td>
    <td><?=$my->r?></td>
    <td><?=$my->s?></td>
    <td><?=$my->t?></td>
    <td><?=$my->u?></td>
    <td><?=$my->v?></td>
    <td><?=$my->w?></td>
    <td><?=$my->x?></td>
    <td><?=$my->y?></td>
    <td><?=$my->z?></td>
    <td><?=$my->aa?></td>
    <td><?=$my->ab?></td>
    <td><?=$my->ac?></td>
    <td><?=$my->ad?></td>
    <td><?=$my->ae?></td>
    <td><?=$my->af?></td>
    <td><?=$my->ag?></td>
    <td><?=$my->ah?></td>
    <td><?=$my->ai?></td>
    <td><?=$my->aj?></td>
    <td><?=$my->ak?></td>
    <td><?=strtoupper($my->al)?></td>
    <?php endforeach; ?>
  </tr>


   <tr class="bb" style="font-weight:bold;">
    <td colspan="4">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="2" bgcolor="#ACACAC">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td bgcolor="#ACACAC">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td bgcolor="#ACACAC">&nbsp;</td>
  </tr>
   <tr class="bb" style="font-weight:bold;">
     <td colspan="4">Two's</td>
     <td><?=$my->db?></td>
     <td><?=$my->dc?></td>
     <td><?=$my->dd?></td>
     <td><?=$my->de?></td>
     <td><?=$my->df?></td>
     <td><?=$my->dg?></td>
     <td><?=$my->dh?></td>
     <td><?=$my->di?></td>
     <td><?=$my->dj?></td>
     <td><?=$my->dk?></td>
     <td><?=$my->dl?></td>
     <td><?=$my->dm?></td>
     <td><?=$my->dn?></td>
     <td><?=$my->do?></td>
     <td colspan="2" bgcolor="#ACACAC">&nbsp;</td>
     <td><?=$my->dq?></td>
     <td><?=$my->dr?></td>
     <td><?=$my->ds?></td>
     <td><?=$my->dt?></td>
     <td><?=$my->du?></td>
     <td><?=$my->dv?></td>
     <td><?=$my->dw?></td>
     <td><?=$my->dx?></td>
     <td><?=$my->dy?></td>
     <td><?=$my->dz?></td>
     <td><?=$my->daa?></td>
     <td><?=$my->dab?></td>
     <td bgcolor="#ACACAC">&nbsp;</td>
     <td><?=$my->dad?></td>
     <td><?=$my->dae?></td>
     <td><?=$my->daf?></td>
     <td><?=$my->dag?></td>
     <td><?=$my->dah?></td>
     <td><?=$my->dai?></td>
     <td><?=$my->daj?></td>
     <td><?=$my->dak?></td>
     <td bgcolor="#ACACAC">&nbsp;</td>
   </tr>
   <tr class="bb" style="font-weight:bold;">
     <td colspan="4">Three's</td>
     <td><?=$my->tb?></td>
     <td><?=$my->tc?></td>
     <td><?=$my->td?></td>
     <td><?=$my->te?></td>
     <td><?=$my->tf?></td>
     <td><?=$my->tg?></td>
     <td><?=$my->th?></td>
     <td><?=$my->ti?></td>
     <td><?=$my->tj?></td>
     <td><?=$my->tk?></td>
     <td><?=$my->tl?></td>
     <td><?=$my->tm?></td>
     <td><?=$my->tn?></td>
     <td><?=$my->to?></td>
     <td colspan="2" bgcolor="#ACACAC">&nbsp;</td>
     <td><?=$my->tq?></td>
     <td><?=$my->tr?></td>
     <td><?=$my->ts?></td>
     <td><?=$my->tt?></td>
     <td><?=$my->tu?></td>
     <td><?=$my->tv?></td>
     <td><?=$my->tw?></td>
     <td><?=$my->tx?></td>
     <td><?=$my->ty?></td>
     <td><?=$my->tz?></td>
     <td><?=$my->taa?></td>
     <td><?=$my->tab?></td>
     <td bgcolor="#ACACAC">&nbsp;</td>
     <td><?=$my->tad?></td>
     <td><?=$my->tae?></td>
     <td><?=$my->taf?></td>
     <td><?=$my->tag?></td>
     <td><?=$my->tah?></td>
     <td><?=$my->tai?></td>
     <td><?=$my->taj?></td>
     <td><?=$my->tak?></td>
     <td bgcolor="#ACACAC">&nbsp;</td>
   </tr>
   <tr class="bb" style="font-weight:bold;">
     <td colspan="4">Total</td>
     <td><?=$my->sumb?></td>
     <td><?=$my->sumc?></td>
     <td><?=$my->sumd?></td>
     <td><?=$my->sume?></td>
     <td><?=$my->sumf?></td>
     <td><?=$my->sumg?></td>
     <td><?=$my->sumh?></td>
     <td><?=$my->sumi?></td>
     <td><?=$my->sumj?></td>
     <td><?=$my->sumk?></td>
     <td><?=$my->suml?></td>
     <td><?=$my->summ?></td>
     <td><?=$my->sumn?></td>
     <td><?=$my->sumo?></td>
     <td colspan="2" bgcolor="#ACACAC">&nbsp;</td>
     <td><?=$my->sumq?></td>
     <td><?=$my->sumr?></td>
     <td><?=$my->sums?></td>
     <td><?=$my->sumt?></td>
     <td><?=$my->sumu?></td>
     <td><?=$my->sumv?></td>
     <td><?=$my->sumw?></td>
     <td><?=$my->sumx?></td>
     <td><?=$my->sumy?></td>
     <td><?=$my->sumz?></td>
     <td><?=$my->sumaa?></td>
     <td><?=$my->sumab?></td>
     <td bgcolor="#ACACAC">&nbsp;</td>
     <td><?=$my->sumad?></td>
     <td><?=$my->sumae?></td>
     <td><?=$my->sumaf?></td>
     <td><?=$my->sumag?></td>
     <td><?=$my->sumah?></td>
     <td><?=$my->sumai?></td>
     <td><?=$my->sumaj?></td>
     <td><?=$my->sumak?></td>
     <td bgcolor="#ACACAC">&nbsp;</td>
   </tr>
</table>


