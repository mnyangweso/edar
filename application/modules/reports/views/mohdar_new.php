<link href="<?=base_url()?>assets/css/forms.css" rel="stylesheet" type="text/css" media="all">
<style>
.bb{
font-weight:bold;
font-family:  "Univers LT 57 Condensed",Garamond,GlasgowSerial-Heavy,Eras-Bold,"Chianti BT", "Chianti XBd BT"; font-size:12px;
background-color:#c4c6d4;
}
.nn{
font-weight:normal;
font-family:  "Univers LT 57 Condensed",Garamond,GlasgowSerial-Heavy,Eras-Bold,"Chianti BT", "Chianti XBd BT"; font-size:11px;
}
.tr{color: #d9edf7;	background-color:#5bc0de;  }
.tda{background:#e7f5f9}
.tdb{background-color:#d0e6ef; }
.odd{background:#f0eff0}
.even{background:#e2e3ea}
html{margin:5px;}
.b {font-weight:bold}
</style>

<html style="margin:5px">
<form method="post" id="mike">
<table cellspacing="0" cellpadding="0" border="1" style="font-size:10px; border-collapse:collapse; text-align:center;">
  <col/>
  <col/>
  <col/>
  <col span="35" />
  <col/>
  <col/>
  <col/>
  <col span="26" />
  <tr class="bb">
    <td colspan="14"><span class="b">County:</span>
      <?=$this->session->userdata('county')?></td>
    <td colspan="11"><span class="b">Sub County:</span>    <?=$this->session->userdata('district')?></td>
    <td colspan="13"><span class="b">Facility Name:</span> <span class="b">
    <?=$this->session->userdata('mfl_name')?>
    </span></td>
    <td colspan="11"><span class="b">Period:</span> <span class="b">
    <?=$full_date?>
    </span></td>
    <td colspan="18"><span class="b">MFL Code:</span>    <?=$this->session->userdata('mfl_code')?></td>
    <td colspan="8"><span class="b">Site ID:</span>    <?=$this->session->userdata('site_id')?></td>
  </tr>
  <tr>
    <td rowspan="4" class="bb">No</td>
    <td rowspan="4" class="bb">Date (dd)</td>
    <td rowspan="4" class="bb">CCC No.</td>
    <td colspan="11" rowspan="2" class="bb">Enrolled in Care</td>
    <td colspan="11" rowspan="2" class="bb">Revisits in Care</td>
    <td colspan="13" rowspan="2" class="bb">Starting&nbsp; ART</td>
    <td colspan="11" rowspan="2" class="bb">Revisits on ART</td>
    <td colspan="6" rowspan="2" class="bb">On CTX/Dapsone</td>
    <td colspan="12" class="bb">TB    in HIV Care</td>
    <td colspan="6" rowspan="2" class="bb">Nutrition and HIV</td>
    <td rowspan="3" class="bb">Screened for Cervical    Cancer</td>
    <td rowspan="3" class="bb">Female Aged 18+</td>
  </tr>
  <tr>
    <td colspan="6" class="bb">Screened</td>
    <td colspan="6" class="bb">&nbsp;Started on IPT</td>
  </tr>
  <tr>
    <td class="bb">&lt; 2 yrs</td>
    <td class="bb">2-9 yrs</td>
    <td class="bb">10-14 yrs (M)</td>
    <td class="bb">10-14 yrs (F)</td>
    <td class="bb">15-19&nbsp;    yrs (M)</td>
    <td class="bb">15-19 yrs (F)</td>
    <td class="bb">20-24 yrs(M)</td>
    <td class="bb">20-24 yrs (F)&nbsp;</td>
    <td class="bb">25+&nbsp;&nbsp;&nbsp;    yrs (M)&nbsp;&nbsp;</td>
    <td class="bb">25+ yrs&nbsp;&nbsp;&nbsp; (F)&nbsp;</td>
    <td class="bb">(KeyPop)</td>
    <td class="bb">&lt; 2 yrs</td>
    <td class="bb">2-9 yrs</td>
    <td class="bb">10-14 yrs (M)</td>
    <td class="bb">10-14 yrs (F)</td>
    <td class="bb">15-19&nbsp;    yrs (M)</td>
    <td class="bb">15-19 yrs (F)</td>
    <td class="bb">20-24 yrs(M)</td>
    <td class="bb">20-24 yrs (F)&nbsp;</td>
    <td class="bb">25+&nbsp;&nbsp;&nbsp;    yrs (M)&nbsp;&nbsp;</td>
    <td class="bb">25+&nbsp;&nbsp;&nbsp; yrs (F)&nbsp;&nbsp;</td>
    <td class="bb">(KeyPop)</td>
    <td class="bb">&lt; 2 yrs</td>
    <td class="bb">2-9 yrs</td>
    <td class="bb">10-14 yrs (M)</td>
    <td class="bb">10-14 yrs (F)</td>
    <td class="bb">15-19&nbsp;    yrs (M)</td>
    <td class="bb">15-19 yrs (F)</td>
    <td class="bb">20-24 yrs(M)</td>
    <td class="bb">20-24 yrs (F)&nbsp;</td>
    <td class="bb">25+&nbsp;&nbsp;&nbsp;    yrs (M)&nbsp;&nbsp;</td>
    <td class="bb">25+ yrs&nbsp;&nbsp;&nbsp; (F)&nbsp;</td>
    <td class="bb">TB patient</td>
    <td class="bb">Discordant</td>
    <td class="bb"> (KeyPop)</td>
    <td class="bb">&lt; 2 yrs</td>
    <td class="bb">2-9 yrs</td>
    <td class="bb">10-14 yrs (M)</td>
    <td class="bb">10-14 yrs (F)</td>
    <td class="bb">15-19&nbsp;    yrs (M)</td>
    <td class="bb">15-19 yrs (F)</td>
    <td class="bb">20-24 yrs(M)</td>
    <td class="bb">20-24 yrs (F)&nbsp;</td>
    <td class="bb">25+&nbsp;&nbsp;&nbsp;    yrs (M)&nbsp;&nbsp;</td>
    <td class="bb">25+&nbsp;&nbsp;&nbsp; yrs (F)&nbsp;&nbsp;</td>
    <td class="bb">(KeyPop)</td>
    <td class="bb">&lt; 2 yrs</td>
    <td class="bb">2-9 yrs</td>
    <td class="bb">10-14 yrs</td>
    <td class="bb">15-19&nbsp;    yrs</td>
    <td class="bb">20-24 yrs</td>
    <td class="bb">25+ yrs</td>
    <td class="bb">&lt; 2 yrs</td>
    <td class="bb">2-9 yrs</td>
    <td class="bb">10-14 yrs</td>
    <td class="bb">15-19&nbsp;    yrs</td>
    <td class="bb">20-24 yrs</td>
    <td class="bb">25+ yrs</td>
    <td class="bb">&lt; 2 yrs</td>
    <td class="bb">2-9 yrs</td>
    <td class="bb">10-14 yrs</td>
    <td class="bb">15-19&nbsp;    yrs</td>
    <td class="bb">20-24 yrs</td>
    <td class="bb">25+ yrs</td>
    <td class="bb">Assessed this visit &lt;15</td>
    <td class="bb">Assessed this visit 15+</td>
    <td class="bb">Malnourished &lt;15</td>
    <td class="bb">Malnourished 15+</td>
    <td class="bb">Provided food &lt;15</td>
    <td class="bb">Provided 15+</td>
  </tr>
  <tr>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
  </tr>
  <tr>
    <td class="nn">(a)</td>
    <td class="nn">(b)</td>
    <td class="nn">(c)</td>
    <td class="nn">(d)</td>
    <td class="nn">(e)</td>
    <td class="nn">(f)</td>
    <td class="nn">(g)</td>
    <td class="nn">(h)</td>
    <td class="nn">(i)</td>
    <td class="nn">(j)</td>
    <td class="nn">(k)</td>
    <td class="nn">(l)</td>
    <td class="nn">(m)</td>
    <td class="nn">(n)</td>
    <td class="nn">(o)</td>
    <td class="nn">(p)</td>
    <td class="nn">(q)</td>
    <td class="nn">(r)</td>
    <td class="nn">(s)</td>
    <td class="nn">(t)</td>
    <td class="nn">(u)</td>
    <td class="nn">(v)</td>
    <td class="nn">(w)</td>
    <td class="nn">(x)</td>
    <td class="nn">(y)</td>
    <td class="nn">(z)</td>
    <td class="nn">(aa)</td>
    <td class="nn">(ab)</td>
    <td class="nn">(ac)</td>
    <td class="nn">(ad)</td>
    <td class="nn">(ae)</td>
    <td class="nn">(af)</td>
    <td class="nn">(ag)</td>
    <td class="nn">(ah)</td>
    <td class="nn">(ai)</td>
    <td class="nn">(aj)</td>
    <td class="nn">(ak)</td>
    <td class="nn">(al)</td>
    <td class="nn">(am)</td>
    <td class="nn">(an)</td>
    <td class="nn">(ao)</td>
    <td class="nn">(ap)</td>
    <td class="nn">(aq)</td>
    <td class="nn">(ar)</td>
    <td class="nn">(as)</td>
    <td class="nn">(at)</td>
    <td class="nn">(au)</td>
    <td class="nn">(av)</td>
    <td class="nn">(aw)</td>
    <td class="nn">(ax)</td>
    <td class="nn">(ay)</td>
    <td class="nn">(az)</td>
    <td class="nn">(ba)</td>
    <td class="nn">(bb)</td>
    <td class="nn">(bc)</td>
    <td class="nn">(bd)</td>
    <td class="nn">(be)</td>
    <td class="nn">(bf)</td>
    <td class="nn">(bg)</td>
    <td class="nn">(bh)</td>
    <td class="nn">(bi)</td>
    <td class="nn">(bj)</td>
    <td class="nn">(bk)</td>
    <td class="nn">(bl)</td>
    <td class="nn">(bm)</td>
    <td class="nn">(bn)</td>
    <td class="nn">(bo)</td>
    <td class="nn">(bp)</td>
    <td class="nn">(bq)</td>
    <td class="nn">(br)</td>
    <td class="nn">(bs)</td>
    <td class="nn">(bt)</td>
    <td class="nn">(bu)</td>
    <td class="nn">(bv)</td>
    <td class="nn">(bw)</td>
  </tr>
<?php foreach($mydata->result() as $my): ?>
 <tr class="<?= alternator('tda', 'tdb'); ?>">
    <td><?=$my->a?></td>
    <td><?=$my->b?></td>
    <td><?=$my->c?></td>
    <td><?=$my->d?></td>
    <td><?=$my->e?></td>
    <td><?=$my->f?></td>
    <td><?=$my->g?></td>
    <td><?=$my->h?></td>
    <td><?=$my->i?></td>
    <td><?=$my->j?></td>
    <td><?=$my->k?></td>
    <td><?=$my->l?></td>
    <td><?=$my->m?></td>
    <td><?=$my->n?></td>
    <td><?=$my->o?></td>
    <td><?=$my->p?></td>
    <td><?=$my->q?></td>
    <td><?=$my->r?></td>
    <td><?=$my->s?></td>
    <td><?=$my->t?></td>
    <td><?=$my->u?></td>
    <td><?=$my->v?></td>
    <td><?=$my->w?></td>
    <td><?=$my->x?></td>
    <td><?=$my->y?></td>
    <td><?=$my->z?></td>
    <td><?=$my->aa?></td>
    <td><?=$my->ab?></td>
    <td><?=$my->ac?></td>
    <td><?=$my->ad?></td>
    <td><?=$my->ae?></td>
    <td><?=$my->af?></td>
    <td><?=$my->ag?></td>
    <td><?=$my->ah?></td>
    <td><?=$my->ai?></td>
    <td><?=$my->aj?></td>
    <td><?=$my->ak?></td>
 <td><?=$my->al?></td>
    <td><?=$my->am?></td>
    <td><?=$my->an?></td>
    <td><?=$my->ao?></td>
    <td><?=$my->ap?></td>
    <td><?=$my->aq?></td>
    <td><?=$my->ar?></td>
    <td><?=$my->ass?></td>
    <td><?=$my->at?></td>
    <td><?=$my->au?></td>
    <td><?=$my->av?></td>
    <td><?=$my->aw?></td>
    <td><?=$my->ax?></td>
    <td><?=$my->ay?></td>
    <td><?=$my->az?></td>
    <td><?=$my->ba?></td>
    <td><?=$my->bb?></td>
    <td><?=$my->bc?></td>
    <td><?=$my->bd?></td>
    <td><?=$my->be?></td>
    <td><?=$my->bf?></td>
    <td><?=$my->bg?></td>
    <td><?=$my->bh?></td>
    <td><?=$my->bi?></td>
    <td><?=$my->bj?></td>
    <td><?=$my->bk?></td>
    <td><?=$my->bl?></td>
    <td><?=$my->bm?></td>
    <td><?=$my->bn?></td>
    <td><?=$my->bo?></td>
    <td><?=$my->bp?></td>
    <td><?=$my->bq?></td>
    <td><?=$my->br?></td>
    <td><?=$my->bs?></td>
    <td><?=$my->bt?></td>
    <td><?=$my->bu?></td>
    <td><?=$my->bv?></td>
    <td><?=$my->bw?></td>
  </tr>
  <?php endforeach;  ?>
 <tr class="<?= alternator('tda', 'tdb'); ?>">
   <td colspan="3">Total This Month </td>
   <td><?=$my->sumd?></td>
   <td><?=$my->sume?></td>
   <td><?=$my->sumf?></td>
   <td><?=$my->sumg?></td>
   <td><?=$my->sumh?></td>
   <td><?=$my->sumi?></td>
   <td><?=$my->sumj?></td>
   <td><?=$my->sumk?></td>
   <td><?=$my->suml?></td>
   <td><?=$my->summ?></td>
   <td><?=$my->sumn?></td>
   <td><?=$my->sumo?></td>
   <td><?=$my->sump?></td>
   <td><?=$my->sumq?></td>
   <td><?=$my->sumr?></td>
   <td><?=$my->sums?></td>
   <td><?=$my->sumt?></td>
   <td><?=$my->sumu?></td>
   <td><?=$my->sumv?></td>
   <td><?=$my->sumw?></td>
   <td><?=$my->sumx?></td>
   <td><?=$my->sumy?></td>
   <td><?=$my->sumz?></td>
   <td><?=$my->sumaa?></td>
   <td><?=$my->sumab?></td>
   <td><?=$my->sumac?></td>
   <td><?=$my->sumad?></td>
   <td><?=$my->sumae?></td>
   <td><?=$my->sumaf?></td>
   <td><?=$my->sumag?></td>
   <td><?=$my->sumah?></td>
   <td><?=$my->sumai?></td>
   <td><?=$my->sumaj?></td>
   <td><?=$my->sumak?></td>
   <td><?=$my->sumal?></td>
   <td><?=$my->sumam?></td>
   <td><?=$my->suman?></td>
   <td><?=$my->sumao?></td>
   <td><?=$my->sumap?></td>
   <td><?=$my->sumaq?></td>
   <td><?=$my->sumar?></td>
   <td><?=$my->sumass?></td>
   <td><?=$my->sumat?></td>
   <td><?=$my->sumau?></td>
   <td><?=$my->sumav?></td>
   <td><?=$my->sumaw?></td>
   <td><?=$my->sumax?></td>
   <td><?=$my->sumay?></td>
   <td><?=$my->sumaz?></td>
   <td><?=$my->sumba?></td>
   <td><?=$my->sumbb?></td>
   <td><?=$my->sumbc?></td>
   <td><?=$my->sumbd?></td>
   <td><?=$my->sumbe?></td>
   <td><?=$my->sumbf?></td>
   <td><?=$my->sumbg?></td>
   <td><?=$my->sumbh?></td>
   <td><?=$my->sumbi?></td>
   <td><?=$my->sumbj?></td>
   <td><?=$my->sumbk?></td>
   <td><?=$my->sumbl?></td>
   <td><?=$my->sumbm?></td>
   <td><?=$my->sumbn?></td>
   <td><?=$my->sumbo?></td>
   <td><?=$my->sumbp?></td>
   <td><?=$my->sumbq?></td>
   <td><?=$my->sumbr?></td>
   <td><?=$my->sumbs?></td>
   <td><?=$my->sumbt?></td>
   <td><?=$my->sumbu?></td>
   <td><?=$my->sumbv?></td>
   <td><?=$my->sumbw?></td>
 </tr>
 <tr class="<?= alternator('tda', 'tdb'); ?>">
   <td colspan="3">count all 2 </td>
   <td colspan="11" rowspan="2" bgcolor="#B5B5B5">&nbsp;</td>
   <td><?=$my->dam?></td>
   <td><?=$my->dan?></td>
   <td><?=$my->dao?></td>
   <td><?=$my->dap?></td>
   <td><?=$my->daq?></td>
   <td><?=$my->dar?></td>
   <td><?=$my->dass?></td>
   <td><?=$my->dat?></td>
   <td><?=$my->dau?></td>
   <td><?=$my->dav?></td>
   <td><?=$my->daw?></td>
   <td colspan="13" rowspan="2" bgcolor="#B5B5B5">&nbsp;</td>
   <td><?=$my->dam?></td>
   <td><?=$my->dan?></td>
   <td><?=$my->dao?></td>
   <td><?=$my->dap?></td>
   <td><?=$my->daq?></td>
   <td><?=$my->dar?></td>
   <td><?=$my->dass?></td>
   <td><?=$my->dat?></td>
   <td><?=$my->dau?></td>
   <td><?=$my->dav?></td>
   <td><?=$my->daw?></td>
   <td colspan="26" rowspan="2" bgcolor="#B5B5B5">&nbsp;</td>
  </tr>
 <tr class="<?= alternator('tda', 'tdb'); ?>">
   <td colspan="3">count all 3 </td>
   <td><?=$my->tam?></td>
   <td><?=$my->tan?></td>
   <td><?=$my->tao?></td>
   <td><?=$my->tap?></td>
   <td><?=$my->taq?></td>
   <td><?=$my->tar?></td>
   <td><?=$my->tass?></td>
   <td><?=$my->tat?></td>
   <td><?=$my->tau?></td>
   <td><?=$my->tav?></td>
   <td><?=$my->taw?></td>
   <td><?=$my->tam?></td>
   <td><?=$my->tan?></td>
   <td><?=$my->tao?></td>
   <td><?=$my->tap?></td>
   <td><?=$my->taq?></td>
   <td><?=$my->tar?></td>
   <td><?=$my->tass?></td>
   <td><?=$my->tat?></td>
   <td><?=$my->tau?></td>
   <td><?=$my->tav?></td>
   <td><?=$my->taw?></td>
 </tr>

  <tr>
    <td colspan="3" class="bb">DE CODE</td>
    <td align="right" class="bb">HV3-001</td>
    <td align="right" class="bb">HV3-002</td>
    <td align="right" class="bb">HV3-003</td>
    <td align="right" class="bb">HV3-004</td>
    <td align="right" class="bb">HV3-005</td>
    <td align="right" class="bb">HV3-006</td>
    <td align="right" class="bb">HV3-007</td>
    <td align="right" class="bb">HV3-008</td>
    <td align="right" class="bb">HV3-009</td>
    <td align="right" class="bb">HV3-010</td>
    <td align="right" class="bb">HV3-012</td>
    <td align="right" class="bb">HV3-013</td>
    <td align="right" class="bb">HV3-014</td>
    <td align="right" class="bb">HV3-015</td>
    <td align="right" class="bb">HV3-016</td>
    <td align="right" class="bb">HV3-017</td>
    <td align="right" class="bb">HV3-018</td>
    <td align="right" class="bb">HV3-019</td>
    <td align="right" class="bb">HV3-020</td>
    <td align="right" class="bb">HV3-021</td>
    <td align="right" class="bb">HV3-022</td>
    <td align="right" class="bb">HV3-024</td>
    <td align="right" class="bb">HV3-037</td>
    <td align="right" class="bb">HV3-038</td>
    <td align="right" class="bb">HV3-039</td>
    <td align="right" class="bb">HV3-040</td>
    <td align="right" class="bb">HV3-041</td>
    <td align="right" class="bb">HV3-042</td>
    <td align="right" class="bb">HV3-043</td>
    <td align="right" class="bb">HV3-044</td>
    <td align="right" class="bb">HV3-045</td>
    <td align="right" class="bb">HV3-046</td>
    <td align="right" class="bb">HV3-048</td>
    <td align="right" class="bb">HV3-049</td>
    <td align="right" class="bb">HV3-050</td>
    <td align="right" class="bb">HV3-051</td>
    <td align="right" class="bb">HV3-052</td>
    <td align="right" class="bb">HV3-053</td>
    <td align="right" class="bb">HV3-054</td>
    <td align="right" class="bb">HV3-055</td>
    <td align="right" class="bb">HV3-056</td>
    <td align="right" class="bb">HV3-057</td>
    <td align="right" class="bb">HV3-058</td>
    <td align="right" class="bb">HV3-059</td>
    <td align="right" class="bb">HV3-060</td>
    <td align="right" class="bb">HV3-062</td>
    <td align="right" class="bb">HV3-075</td>
    <td align="right" class="bb">HV3-076</td>
    <td align="right" class="bb">HV3-077</td>
    <td align="right" class="bb">HV3-078</td>
    <td align="right" class="bb">HV3-079</td>
    <td align="right" class="bb">HV3-080</td>
    <td align="right" class="bb">HV3-082</td>
    <td align="right" class="bb">HV3-083</td>
    <td align="right" class="bb">HV3-084</td>
    <td align="right" class="bb">HV3-085</td>
    <td align="right" class="bb">HV3-086</td>
    <td align="right" class="bb">HV3-087</td>
    <td align="right" class="bb">HV3-089</td>
    <td align="right" class="bb">HV3-090</td>
    <td align="right" class="bb">HV3-091</td>
    <td align="right" class="bb">HV3-092</td>
    <td align="right" class="bb">HV3-093</td>
    <td align="right" class="bb">HV3-094</td>
    <td align="right" class="bb">HV3-100</td>
    <td align="right" class="bb">HV3-101</td>
    <td align="right" class="bb">HV3-103</td>
    <td align="right" class="bb">HV3-104</td>
    <td align="right" class="bb">HV3-106</td>
    <td align="right" class="bb">HV3-107</td>
    <td align="right" class="bb">HV3-111</td>
    <td align="right" class="bb">HV3-112</td>
  </tr>
  <tr>
    <td colspan="3" class="bb">TOTAL this Page [Count 1,2,3]</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" class="bb">TOTAL this Month</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
    <td class="bb">&nbsp;</td>
  </tr>
</table>

