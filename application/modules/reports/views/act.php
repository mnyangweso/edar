<link href="../../assets/css/forms.css" rel="stylesheet" type="text/css">
<style type="text/css">

<!--
.bb{
font-weight:bold;
font-family:  "Univers LT 57 Condensed",Garamond,GlasgowSerial-Heavy,Eras-Bold,"Chianti BT", "Chianti XBd BT"; font-size:18px;
}
.style1 {color: #666666;font-weight: bold; }
.style2 {color: #666666}
-->

</style>


<table width="100%" border="1" cellpadding="2" cellspacing="2" frame="box" class="bb" style="background-color:#FFFFFF; border-collapse:collapse">

  <tr class="bb">
    <td colspan="6"><div align="center" class="style1">ACT MONTHLY REPORTING TOOL  </div></td>
  </tr>
  <tr class="bb">
    <td colspan="6"><div align="center" class="style1">PERIOD <?=$full_date?></div></td>
  </tr>
  <tr>
    <td rowspan="2"><span class="style1">INDICATOR</span></td>
    <td colspan="4"><span class="style2">AGE</span></td>
    <td rowspan="2"><span class="style2"><strong>TOTAL</strong></span></td>
  </tr>
  <tr>
    <td><span class="style2"><strong>0 - 9 Years</strong></span></td>
    <td><span class="style2"><strong>10 - 14 Years </strong></span></td>
    <td><span class="style2"><strong>15 - 19 Years</strong></span></td>
    <td><span class="style2"><strong>20+ Years</strong></span></td>
  </tr>
  <tr  bgcolor="#c4c6d4">

    <td colspan="6">CARE &amp; TREATMENT</td>
  </tr>
  <tr>
    <td>Number enroled into HIV care</td>
    <td><?=$a1?></td>
    <td><?=$b1?></td>
    <td><?=$c1?></td>
    <td><?=$d1?></td>
    <td><?=$e1?></td>
  </tr>
  <tr>
    <td>Number currently in HIV care</td>
    <td><?=$a2?></td>
    <td><?=$b2?></td>
    <td><?=$c2?></td>
    <td><?=$d2?></td>
    <td><?=$e2?></td>
  </tr>
  <tr>
    <td>Number newly initiating ART</td>
    <td><?=$a3?></td>
    <td><?=$b3?></td>
    <td><?=$c3?></td>
    <td><?=$d3?></td>
    <td><?=$e3?></td>
  </tr>
  <tr>
    <td>Number currently on ART</td>
    <td><?=$a4?></td>
    <td><?=$b4?></td>
    <td><?=$c4?></td>
    <td><?=$d4?></td>
    <td><?=$e4?></td>
  </tr>
  <tr  bgcolor="#c4c6d4">

    <td colspan="6">TB</td>
  </tr>
  <tr>
    <td>Number of PLHIV clients screened for TB at last visit</td>
    <td><?=$a5?></td>
    <td><?=$b5?></td>
    <td><?=$c5?></td>
    <td><?=$d5?></td>
    <td><?=$e5?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>

