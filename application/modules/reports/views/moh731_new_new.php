<style type="text/css">
<!--
.b {font-weight:bold;}
.c{text-align:center}
-->
</style>

<table width="100%" border="1" cellpadding="0" cellspacing="0" style="font-size:12px; border-collapse:collapse; ">
  <tr>
    <td colspan="5" class="b c">National AIDS &amp; STI Control Programme</td>
  </tr>
  <tr>
    <td colspan="5" class="b c">MOH 731- Comprehensive HIV/AIDS Facility Reporting Form - NASCOP</td>
  </tr>
  <tr>
    <td colspan="5">&nbsp;</td>
  </tr>
  <tr>
    <td class="b">Sub County: </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td class="b">County:</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="b">Month-Year:</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td  class="b">MFL Code:</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="5"><span class="b">3 Care and Treatment</span></td>
  </tr>
  <tr>
    <td colspan="5"><span class="b">3.1 Enrollment in Care </span></td>
  </tr>
  <tr>
    <td> Enrolled &lt; 2 years </td>
    <td>HV03-01</td>
    <td><?=$ctx_cm?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Enrolled 2 - 9 years </td>
    <td>HV03-02</td>
    <td><?=$ctx_cm?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="22">Enrolled 10 - 14 years </td>
    <td>HV03-03 (M)</td>
    <td><?=$ctx_cm?></td>
    <td>HV03-04 (F)</td>
    <td><?=$ctx_cf?></td>
  </tr>
  <tr>
    <td>Enrolled 15 - 19 years </td>
    <td>HV03-05 (M)</td>
    <td><?=$ctx_am?></td>
    <td>HV03-06 (F)</td>
    <td><?=$ctx_af?></td>
  </tr>
  <tr>
    <td>Enrolled 20 - 24 years </td>
    <td>HV03-07 (M)</td>
    <td><?=$ctx_am?></td>
    <td>HV03-08 (F)</td>
    <td><?=$ctx_af?></td>
  </tr>
  <tr>
    <td>Enrolled 25+ years </td>
    <td>HV03-09 (M)</td>
    <td><?=$ctx_am?></td>
    <td>HV03-10 (F)</td>
    <td><?=$ctx_af?></td>
  </tr>
  <tr class="b">
    <td>Total on CTX (Sum HV03-03 to HV03-06)</td>
    <td>&nbsp;</td>
    <td><?=$ctx_ca?></td>
    <td>HV03-10 (F)</td>
    <td><?=$ctx_af?></td>
  </tr>
  <tr class="">
    <td>Enrolled In Care_KeyPop </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>HV03-12 (F)</td>
    <td><?=$ctx_af?></td>
  </tr>
  <tr>
    <td colspan="5"><span class="b">3.2 Revisits in Care </span></td>
  </tr>
  <tr>
    <td> In Care Revisits ART < 2 years </td>
    <td>HV03-01</td>
    <td><?=$ctx_cm?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>In Care Revisits ART 2 - 9 years </td>
    <td>HV03-02</td>
    <td><?=$ctx_cm?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="22">In Care  Revisits ART 10 - 14 years </td>
    <td>HV03-03 (M)</td>
    <td><?=$ctx_cm?></td>
    <td>HV03-04 (F)</td>
    <td><?=$ctx_cf?></td>
  </tr>
  <tr>
    <td>In Care Revisits  ART 15 - 19 years </td>
    <td>HV03-05 (M)</td>
    <td><?=$ctx_am?></td>
    <td>HV03-06 (F)</td>
    <td><?=$ctx_af?></td>
  </tr>
  <tr>
    <td>In Care  Revisits ART 20 - 24 years </td>
    <td>HV03-07 (M)</td>
    <td><?=$ctx_am?></td>
    <td>HV03-08 (F)</td>
    <td><?=$ctx_af?></td>
  </tr>
  <tr>
    <td>In Care  Revisits ART 25+ years </td>
    <td>HV03-09 (M)</td>
    <td><?=$ctx_am?></td>
    <td>HV03-10 (F)</td>
    <td><?=$ctx_af?></td>
  </tr>
  <tr class="b">
    <td>In Care_Revisits_TOTAL (Sum HV03-03 to HV03-06)</td>
    <td>&nbsp;</td>
    <td><?=$ctx_ca?></td>
    <td>HV03-10 (F)</td>
    <td><?=$ctx_af?></td>
  </tr>
  <tr class="">
    <td>In Care  Revisits ART 25+ years </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>HV03-11 (F)</td>
    <td><?=$ctx_af?></td>
  </tr>
  <tr class="">
    <td>In Care Revisits  ART_KeyPop </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>HV03-12 (F)</td>
    <td><?=$ctx_af?></td>
  </tr><tr>
    <td colspan="5"><span class="b">3.3 Currently in Care </span></td>
  </tr>
  <tr>
    <td> In Care ART &lt; 2 years </td>
    <td>HV03-01</td>
    <td><?=$ctx_cm?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>In Care ART 2 - 9 years </td>
    <td>HV03-02</td>
    <td><?=$ctx_cm?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="22">In Care ART 10 - 14 years </td>
    <td>HV03-03 (M)</td>
    <td><?=$ctx_cm?></td>
    <td>HV03-04 (F)</td>
    <td><?=$ctx_cf?></td>
  </tr>
  <tr>
    <td>In Care ART 15 - 19 years </td>
    <td>HV03-05 (M)</td>
    <td><?=$ctx_am?></td>
    <td>HV03-06 (F)</td>
    <td><?=$ctx_af?></td>
  </tr>
  <tr>
    <td>In Care ART 20 - 24 years </td>
    <td>HV03-07 (M)</td>
    <td><?=$ctx_am?></td>
    <td>HV03-08 (F)</td>
    <td><?=$ctx_af?></td>
  </tr>
  <tr>
    <td>In Care ART 25+ years </td>
    <td>HV03-09 (M)</td>
    <td><?=$ctx_am?></td>
    <td>HV03-10 (F)</td>
    <td><?=$ctx_af?></td>
  </tr>
  <tr class="b">
    <td>In Care_TOTAL (Sum HV03-03 to HV03-06)</td>
    <td>&nbsp;</td>
    <td><?=$ctx_ca?></td>
    <td>HV03-10 (F)</td>
    <td><?=$ctx_af?></td>
  </tr>
  <tr class="">
    <td>In Care ART_KeyPop </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>HV03-12 (F)</td>
    <td><?=$ctx_af?></td>
  </tr>
  <tr>
    <td colspan="5"><span class="b">3.4 Starting ART</span></td>
  </tr>
  <tr>
    <td> Start ART &lt; 2 years </td>
    <td>HV03-01</td>
    <td><?=$ctx_cm?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Start ART 2 - 9 years </td>
    <td>HV03-02</td>
    <td><?=$ctx_cm?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="22">Start ART 10 - 14 years </td>
    <td>HV03-03 (M)</td>
    <td><?=$ctx_cm?></td>
    <td>HV03-04 (F)</td>
    <td><?=$ctx_cf?></td>
  </tr>
  <tr>
    <td>Start  ART 15 - 19 years </td>
    <td>HV03-05 (M)</td>
    <td><?=$ctx_am?></td>
    <td>HV03-06 (F)</td>
    <td><?=$ctx_af?></td>
  </tr>
  <tr>
    <td>Start ART 20 - 24 years </td>
    <td>HV03-07 (M)</td>
    <td><?=$ctx_am?></td>
    <td>HV03-08 (F)</td>
    <td><?=$ctx_af?></td>
  </tr>
  <tr>
    <td>Start ART 25+ years </td>
    <td>HV03-09 (M)</td>
    <td><?=$ctx_am?></td>
    <td>HV03-10 (F)</td>
    <td><?=$ctx_af?></td>
  </tr>
  <tr class="b">
    <td>Start ART_TOTAL (Sum HV03-03 to HV03-06)</td>
    <td>&nbsp;</td>
    <td><?=$ctx_ca?></td>
    <td>HV03-10 (F)</td>
    <td><?=$ctx_af?></td>
  </tr>
  <tr class="">
    <td>Start  ART_TB Patient </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>HV03-12 (F)</td>
    <td><?=$ctx_af?></td>
  </tr>
  
  <tr>
    <td>Start  ART_Discordant </td>
    <td>HV03-20</td>
    <td><?=$start_art_ci?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Start  ART_KeyPop </td>
    <td>HV03-21 (M)</td>
    <td><?=$start_art_cm?></td>
    <td>HV03-22 (F)</td>
    <td><?=$start_art_cf?></td>
  </tr>
  <tr>
    <td colspan="5"><span class="b">3.5 Revisits on ART</span></td>
  </tr>
  <tr>
    <td>Revisit on ART - Below 1 year</td>
    <td>HV03-28</td>
    <td><?=$art_rev_ci?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Revisit on ART - Below 15 years</td>
    <td>HV03-29 (M)</td>
    <td><?=$art_rev_cm?></td>
    <td>HV03-30 (F)</td>
    <td>
        <?=$art_rev_cf?>    </td>
  </tr>
  <tr>
    <td>Revisit on ART - 15 years &amp; older</td>
    <td>HV03-31 (M)</td>
    <td><?=$art_rev_am?></td>
    <td>HV03-32 (F)</td>
    <td> <?=$art_rev_af?></td>
  </tr>
  <tr class="b">
    <td>Total Revisit on ART (Sum HV03-29 to HV03-32) </td>
    <td>HV03-33</td>
    <td><?=$art_rev_ca?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="5"><span class="b">3.6 Currently on ART [All]</span></td>
  </tr>
  <tr>
    <td>Currently on ART - Below 1 year</td>
    <td>HV03-34</td>
    <td><?=$curr_art_ci?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Currently on ART - Below 15 years</td>
    <td>HV03-35 (M)</td>
    <td><?=$curr_art_cm?></td>
    <td>HV03-36 (F)</td>
    <td><?=$curr_art_cf?></td>
  </tr>
  <tr>
    <td>Currently on ART - 15 years &amp; older </td>
    <td>HV03-37 (M)</td>
    <td><?=$curr_art_am?></td>
    <td>HV03-38 (F)</td>
    <td><?=$curr_art_af?></td>
  </tr>
  <tr class="b">
    <td>Total Current on ART (Sum HV03-35 to HV03-38)</td>
    <td>HV03-39</td>
    <td><?=$curr_art_ca?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
   <tr>
    <td colspan="5"><span class="b">3.7 Cumulative Ever on ART</span></td>
  </tr>
   <tr>
     <td>Ever on ART - Below 15 years</td>
     <td>HV03-40 (M)</td>
    <td><?=$ever_art_cm?></td>
    <td>HV03-41 (F)</td>
    <td><?=$ever_art_cf?></td>
  </tr>
   <tr>
     <td>Ever on ART - 15 years &amp; older</td>
     <td>HV03-42 (M)</td>
    <td><?=$ever_art_am?></td>
    <td>HV03-43 (F)</td>
    <td><?=$ever_art_af?></td>
  </tr>
   <tr class="b">
     <td>Total Ever on ART (Sum HV03-40 to HV03-43)</td>
     <td>HV03-44</td>
    <td><?=$ever_art_ca?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
   <tr>
    <td colspan="5"><span class="b">3.8 Survival and Retention on ART at 12 months</span></td>
  </tr>
   <tr>
     <td>ART Net Cohort at 12 months</td>
     <td>HV03-45</td>
    <td><?=$net_cohort?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
   <tr>
     <td>On Original 1st Line at 12 months</td>
     <td>HV03-46</td>
    <td><?=$orig1_cohort?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
   <tr>
     <td>On alternative 1st Line at 12 months</td>
     <td>HV03-47</td>
    <td><?=$alt1_cohort?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
   <tr>
     <td>On 2nd Line (or higher) at 12 months</td>
     <td>HV03-48</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
   <tr>
     <td>On therapy at 12 months (Sum HV03-46 to HV03-48)</td>
     <td>HV03-49</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
   <tr>
    <td colspan="5"><span class="b">3.9 Screening</span></td>
  </tr>
   <tr>
     <td>Screened for TB - Below 15 years</td>
     <td>HV03-50 (M)</td>
    <td><?=$screen_tb_cm?></td>
    <td>HV03-51 (F)</td>
    <td><?=$screen_tb_cf?></td>
  </tr>
   <tr>
     <td>Screened for TB - 15 years &amp; older</td>
     <td>HV03-52 (M)</td>
    <td><?=$screen_tb_am?></td>
    <td>HV03-53 (F)</td>
    <td><?=$screen_tb_af?></td>
  </tr>
   <tr class="b">
     <td>Total Screened for TB (Sum HV03-50 to -53)</td>
     <td>HV03-54</td>
    <td><?=$screen_tb_ca?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
   <tr>
     <td>Screened for cervical cancer (F 18+)</td>
     <td>HV03-55</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
   <tr>
    <td colspan="5"><span class="b">3.10 Prevention with Positives</span></td>
  </tr>
   <tr>
     <td>Modern contraceptive methods</td>
     <td>HV09-04</td>
    <td><?=$modern_fp?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
   <tr>
     <td>Provided with condoms</td>
     <td>HV09-05</td>
    <td><?=$condom_fp?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="5"><span class="b">3.11 HIV Care Visits</span></td>
  </tr>
  <tr>
    <td>Females (18+)</td>
    <td>HV03-70</td>
    <td><?=$af_visits?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Scheduled</td>
    <td>HV03-71</td>
    <td><?=$s_visit?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Unscheduled</td>
    <td>HV03-72</td>
    <td><?=$us_visit?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr class="b">
    <td>Total visits (HV03-71 &amp; -72)</td>
    <td>HV03-73</td>
    <td><?=$ttl_visit?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>



