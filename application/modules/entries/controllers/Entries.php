<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Entries extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->pbscms->_check_logged();
		$this->form_validation->set_error_delimiters('<div class="ferror">', '</div>');
	}
	
	public function index()
	{
		$this->form_validation->set_rules('ccc_no', 'ccc no', 'required|min_length[2]|max_length[30]');	
		$this->form_validation->set_rules('visit_date', 'visit date', 'required|min_length[2]|max_length[30]');	
		
		if ($this->form_validation->run() == FALSE){
			$this->db->distinct('ccc_no');
			$this->db->select('ccc_no');
			$data['query']= $this->db->get('dar');
						
			$data['title'] = "Daily Activity Register";
			$data['content'] = $this->load->view('dar',$data, TRUE);
			$this->load->view('template',$data);
		}else{
			
			$nn = $this->input->post();
			unset($nn['Submit']);
			$nn =  array_map('strtolower', $nn);
			$nn = array_diff($nn, array('') );
			

			///// check new
			$vd = $this->input->post('visit_date');
			$mid = $this->input->post('ccc_no');

			if(strpos($vd,'/'))
				$var = explode('/',$vd);
			else
				$var = explode('-',$vd);
			$mnt = $var[1];
			$yr = $var[2];


			$sqlbi = "UPDATE dar SET visit_type = 'r' WHERE ccc_no=$mid AND DATE_PART('MONTH',visit_date)=$mnt AND DATE_PART('YEAR',visit_date)= $yr";
			$sqlai = "UPDATE dar SET visit_type = 'n' WHERE id = (SELECT id FROM dar WHERE ccc_no= $mid AND DATE_PART('MONTH',visit_date)=$mnt AND DATE_PART('YEAR',visit_date)=$yr ORDER BY visit_date ASC LIMIT 1)";
			

			//// end

			if(count($nn) > 0){

				$this->db->trans_start();
					$this->db->insert('dar',$nn);
					$this->db->query($sqlbi);
					$this->db->query($sqlai);
				$this->db->trans_complete();

				if($this->db->trans_status()===FALSE){
					$this->session->set_flashdata('flash', 'true');
					$this->session->set_flashdata('flashtype', 'success');
					$this->session->set_flashdata('flashmessage', 'Record successfully updated');
				}
				
			}		
			
			redirect('');
		}
	}
	
	
	public function delete()
	{	
		$dl = $this->uri->segment(3);
		
		$sql = "SELECT id AS mid, date_part('MONTH', visit_date) AS mnt,date_part('YEAR', visit_date) AS yr  FROM dar WHERE id=$dl";
		$row = $this->db->query($sql)->row();
		
		$mid = $dl;
		$mnt = $row->mnt;
		$yr = $row->yr;

		
			$sqlbi = "UPDATE dar SET visit_type = 'r' WHERE ccc_no=$mid AND DATE_PART('MONTH',visit_date)=$mnt AND DATE_PART('YEAR',visit_date)= $yr";
			$sqlai = "UPDATE dar SET visit_type = 'n' WHERE id = (SELECT id FROM dar WHERE ccc_no= $mid AND DATE_PART('MONTH',visit_date)=$mnt AND DATE_PART('YEAR',visit_date)=$yr ORDER BY visit_date ASC LIMIT 1)";

			if($this->db->trans_status()===FALSE){
				$this->db->trans_start();
					$this->db->delete('dar', array('id' => $dl)); 
					$this->db->query($sqlbi);
					$this->db->query($sqlai);
				$this->db->trans_complete();
		
				$this->session->set_flashdata('flash', 'true');
				$this->session->set_flashdata('flashtype', 'success');
				$this->session->set_flashdata('flashmessage', 'Entry  has been succesfully deleted');
			}
			
		redirect('entries/show');	
	}
	

		
	public function myajax()
	{
			$ccc = $this->uri->segment(4);
			
			$this->db->where('ccc_no',$ccc);
			$call = $this->db->count_all_results('dar');
			if($call < 1) 
				break; 
			
			$this->db->limit(1);
			$this->db->where('ccc_no',$ccc);
			$this->db->order_by('visit_date','desc','pre_art');
			$this->db->select('sex, dob,pre_art');	
			$query = $this->db->get('dar')->row();

			$result[] = array("dob" => $query->dob,"sex" => $query->sex,"pre_art" => $query->pre_art);
			//print_r($result); die();
			
			echo json_encode($result);
						
			//$sql = "SELECT row_to_json(t) FROM ( SELECT sex,dob FROM dar WHERE ccc_no=$ccc ORDER BY visit_date DESC LIMIT 1  ) t";
			//$data['query'] = $this->db->query($sql)->row_array();
			//print_r($data['query']); die();
			//echo $data['query'];
	/*
			$keywords = array();
		
			foreach ($query->result() as $row){
				 array_push($keywords, $row->dob);
				 array_push($keywords, $row->sex);
				print_r($keywords);
			}*/
			//echo json_encode($keywords);

			
	}

	public function show()
	{
			$this->db->order_by('visit_date','desc');	
			$data['query'] =$this->db->get('dar');

			$data['title'] = "List Entries";
			$data['content'] = $this->load->view('list',$data,TRUE);
			
			$this->load->view('template',$data);

	}

	
	public function search()
	{	


	$sql = " SELECT ROW_NUMBER() OVER (ORDER BY visit_date) AS row_id,id,ccc_no,UPPER(sex)AS sex,dob,visit_date,UPPER(visit_type) AS visit_type, 
	CASE WHEN scheduled_visit = 't' THEN 'SCHEDULED' WHEN scheduled_visit = 'f' THEN 'UNSCHEDULED'  END AS scheduled_visit,tca, 
	UPPER(prophylaxis) AS prophylaxis,yn_bool( enrolled_in_care) AS enrolled_in_care, yn_bool(started_on_art) AS started_on_art,
       yn_bool( tb_screening) AS tb_screening, yn_bool(started_ipt) AS started_ipt,yn_bool( pwp_contraceptive) AS pwp_contraceptive, 
       yn_bool(pwp_condoms) AS pwp_condoms, yn_bool(cancer_screening) AS cancer_screening,yn_bool(nutrition_accessed) AS nutrition_accessed,
	yn_bool(nutrition_malnorished) AS nutrition_malnorished,yn_bool(nutrition_food_prescribed) AS nutrition_food_prescribed, 
      CASE WHEN pre_art = 't' THEN 'PRE ART' WHEN pre_art = 'f' THEN 'ART'  END AS art_status,
	yn_bool(pregnant) AS pregnant, age,yn_bool(tb_tx) AS tb_tx
  FROM dar
		ORDER BY visit_date 
			";
			

			$data['query'] = $this->db->query($sql);
			
			$data['title'] = "Search & Export Patient";
			$data['content'] = $this->load->view('search',$data,TRUE);
			$this->load->view('template',$data);
			//$this->load->view('search',$data);
		
	}


	public function edit()
	{
		$this->form_validation->set_rules('ccc_no', 'ccc no', 'required|min_length[2]|max_length[30]');	
		$this->form_validation->set_rules('visit_date', 'visit date', 'required|min_length[2]|max_length[30]');	
		
		if ($this->form_validation->run() == FALSE){

			$data['mydata'] = $this->db->get_where('dar',array('id'=> $this->uri->segment(3) ) )->row();
			//print_r($data['mydata']); die();
			$data['title'] = "Daily Activity Register";
			$data['content'] = $this->load->view('dar_edit',$data, TRUE);
			$this->load->view('template',$data);
		}else{

			///// check new
			$vd = $this->input->post('visit_date');
			$mid = $this->input->post('ccc_no');
			
			if(strpos($vd,'/'))
				$var = explode('/',$vd);				
			else
				$var = explode('-',$vd);
				
			//print_r($var); die();
			$mnt = $var[1];
			$yr = $var[2];

			$sqlbi = "UPDATE dar SET visit_type = 'r' WHERE ccc_no=$mid AND DATE_PART('MONTH',visit_date)=$mnt AND DATE_PART('YEAR',visit_date)= $yr";
			$sqlai = "UPDATE dar SET visit_type = 'n' WHERE id = (SELECT id FROM dar WHERE ccc_no= $mid AND DATE_PART('MONTH',visit_date)=$mnt AND DATE_PART('YEAR',visit_date)=$yr ORDER BY visit_date ASC LIMIT 1)";
			
			//// end

			$nn = $this->input->post();
			unset($nn['Submit']);
			$nn =  array_map('strtolower', $nn);
			$nn = array_diff($nn, array('') );
			
			if(count($nn) > 0){
				
				$this->db->trans_start();
					$this->db->update('dar',$nn, array('id'=> $this->uri->segment(3) ) ) ;
					$this->db->query($sqlbi);
					$this->db->query($sqlai);
				$this->db->trans_complete();

				if($this->db->trans_status()===FALSE){
					$this->session->set_flashdata('flash', 'true');
					$this->session->set_flashdata('flashtype', 'success');
					$this->session->set_flashdata('flashmessage', 'Record successfully updated');
				}
			}

			redirect('');
		}
	}

}
