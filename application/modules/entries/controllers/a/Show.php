<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Show extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->pbscms->_check_logged();
		$this->form_validation->set_error_delimiters('<div class="ferror">', '</div>');
		//session_start();
	}


	public function index()
	{
			$this->db->order_by('visit_date','desc');	
			$data['query'] =$this->db->get('dar');

			$data['title'] = "List Entries";
			$data['content'] = $this->load->view('list',$data,TRUE);
			
			$this->load->view('template',$data);

	}



}
