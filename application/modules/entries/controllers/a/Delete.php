<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Delete extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->pbscms->_check_logged();
		$this->form_validation->set_error_delimiters('<div class="ferror">', '</div>');
		//session_start();
	}
	
		
	
	public function index()
	{	
		//die('nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn');		
		$dl = $this->uri->segment(4);
		
		$sql = "SELECT id AS mid, date_part('MONTH', visit_date) AS mnt,date_part('YEAR', visit_date) AS yr  FROM dar WHERE id=$dl";
		$row = $this->db->query($sql)->row();
		
		$mid = $dl;
		$mnt = $row->mnt;
		$yr = $row->yr;

		
			$sqlbi = "UPDATE dar SET visit_type = 'r' WHERE ccc_no=$mid AND DATE_PART('MONTH',visit_date)=$mnt AND DATE_PART('YEAR',visit_date)= $yr";
			$sqlai = "UPDATE dar SET visit_type = 'n' WHERE id = (SELECT id FROM dar WHERE ccc_no= $mid AND DATE_PART('MONTH',visit_date)=$mnt AND DATE_PART('YEAR',visit_date)=$yr ORDER BY visit_date ASC LIMIT 1)";

			if($this->db->trans_status()===FALSE){
				$this->db->trans_start();
					$this->db->delete('dar', array('id' => $dl)); 
					$this->db->query($sqlbi);
					$this->db->query($sqlai);
				$this->db->trans_complete();
		
				$this->session->set_flashdata('flash', 'true');
				$this->session->set_flashdata('flashtype', 'success');
				$this->session->set_flashdata('flashmessage', 'Entry  has been succesfully deleted');
			}
			
		redirect('entries/show');	
	}
	

	
}
