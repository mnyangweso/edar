<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->pbscms->_check_logged();
		$this->form_validation->set_error_delimiters('<div class="ferror">', '</div>');
		//session_start();
	}
	
	
	
	public function index()
	{	


	$sql = " SELECT ROW_NUMBER() OVER (ORDER BY visit_date) AS row_id,id,ccc_no,UPPER(sex)AS sex,dob,visit_date,UPPER(visit_type) AS visit_type, 
	CASE WHEN scheduled_visit = 't' THEN 'SCHEDULED' WHEN scheduled_visit = 'f' THEN 'UNSCHEDULED'  END AS scheduled_visit,tca, 
	UPPER(prophylaxis) AS prophylaxis,yn_bool( enrolled_in_care) AS enrolled_in_care, yn_bool(started_on_art) AS started_on_art,
       yn_bool( tb_screening) AS tb_screening, yn_bool(started_ipt) AS started_ipt,yn_bool( pwp_contraceptive) AS pwp_contraceptive, 
       yn_bool(pwp_condoms) AS pwp_condoms, yn_bool(cancer_screening) AS cancer_screening,yn_bool(nutrition_accessed) AS nutrition_accessed,
	yn_bool(nutrition_malnorished) AS nutrition_malnorished,yn_bool(nutrition_food_prescribed) AS nutrition_food_prescribed, 
      CASE WHEN pre_art = 't' THEN 'PRE ART' WHEN pre_art = 'f' THEN 'ART'  END AS art_status,
	yn_bool(pregnant) AS pregnant, age,yn_bool(tb_tx) AS tb_tx
  FROM dar
		ORDER BY visit_date 
			";
			

			$data['query'] = $this->db->query($sql);
			
			$data['title'] = "Search & Export Patient";
			$data['content'] = $this->load->view('search',$data,TRUE);
			$this->load->view('template',$data);
			//$this->load->view('search',$data);
		
	}
	

}
