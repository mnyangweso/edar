<link href="forms.css" rel="stylesheet" type="text/css" media="all">

<style>

.bb{font-weight:bold;}
.cc{background-color:#c4c6d4; }

</style>

<script language="javascript">

</script>

<?php echo validation_errors(); ?>
<form method="post">

<table width="100%" frame="box" cellspacing="2" cellpadding="2" style="background-color:#FFFFFF">
  <tr class="cc">
    <td colspan="4" class="bb">Prevention With Positive</td>
  </tr>
  <tr class="odd">
    <td>CCC No</td>
    <td><input type="number" name="ccc_no" list="ccc" value="<?= set_value('ccc_no',$mydata->ccc_no)?>" required>
	<datalist id="ccc">    </datalist>	</td>
    <td>Sex</td>
    <td><label>
      <select name="sex" id="sex"  required="required"  onchange="femaleCheck(this.value)">
        <option value="">-- Select Sex --</option>
        <option value="male" <?=set_select('sex','male',( "male"==$mydata->sex) ) ?>>Male</option>
        <option value="female"  <?=set_select('sex','female',( "female"==$mydata->sex) ) ?> >Female</option>
      </select>
    </label></td>
  </tr>
  <tr class="even">
    <td>Date Of Birth</td>
    <td>
      <input type="text" name="dob" id="dob" required="required" value="<?= set_value('dob',$mydata->dob)?>" />    </td>
    <td>Age</td>
    <td><input type="number" name="age" maxlength="3" min="0" max="140" value="<?= set_value('age',$mydata->age)?>" onchange="myFunction(this.value)" /></td>
  </tr>
  <tr class="cc">
    <td colspan="4" class="bb">Appointment</td>
  </tr>
  <tr class="odd">
    <td>Visit Date</td>
    <td><label>
      <input type="text" name="visit_date" id="visit_date" value="<?= set_value('visit_date',$mydata->visit_date)?>"  required="required" />
    </label></td>
    <td>Scheduled Visit</td>
    <td><label>Yes
      <input name="scheduled_visit" type="radio" value="y" <?=set_radio('scheduled_visit','y',('t'==$mydata->scheduled_visit))?> />
      </label>
        <label>No
          <input name="scheduled_visit" type="radio" value="n" <?=set_radio('scheduled_visit','n',('f'==$mydata->scheduled_visit))?> />
      </label></td>
  </tr>
  <tr class="cc">
    <td colspan="4" class="bb">Medication and Enrollment </td>
  </tr>
  <tr class="odd">
    <td>Prophylaxis</td>
    <td><label>None
      <input name="prophylaxis" type="radio" value=" " <?=set_radio('prophylaxis', ' ',(' '==$mydata->prophylaxis))?> required="required" />
      </label>
        <label>CTX
          <input name="prophylaxis" type="radio" value="ctx" <?=set_radio('prophylaxis', 'ctx',('ctx'==$mydata->prophylaxis))?> required="required" />
        </label>
        <label>Dapson
          <input name="prophylaxis" type="radio" value="dapson" <?=set_radio('prophylaxis', 'dapson',('dapson'==$mydata->prophylaxis))?> required="required" />
        </label>    </td>
    <td>Client Type 
      <label></label><label></label></td>
    <td><label></label>
    <label>Pre-ART Client
        <input name="pre_art" type="radio" value="y" <?=set_radio('pre_art','y',('t'==$mydata->pre_art))?> required="required" />
</label>
      <label>ART Client
      <input name="pre_art" type="radio" value="n" <?=set_radio('pre_art','n',('f'==$mydata->pre_art))?> required="required" />
      </label></td>
  </tr>
  <tr class="even">
    <td>Enrolled In Care
      <label></label>
      <label>Today</label></td>
    <td><label></label><label>Yes
        <input name="enrolled_in_care" type="radio" value="y" <?=set_radio('enrolled_in_care','y',('t'==$mydata->enrolled_in_care))?> required="required" />
</label>
      <label>No
      <input name="enrolled_in_care" type="radio" value="n" <?=set_radio('enrolled_in_care','n',('f'==$mydata->enrolled_in_care))?> required="required" />
      </label></td>
    <td>Started On ART Today </td>
    <td><label></label><label></label><label>Yes
        <input name="started_on_art" type="radio" value="y"  <?=set_radio('started_on_art','y',('t'==$mydata->started_on_art))?>  required="required" onchange="preARTcheck()" />
</label>
      <label>No
      <input name="started_on_art" type="radio" value="n" <?=set_radio('started_on_art','n',('f'==$mydata->started_on_art))?>  required="required" />
      </label></td>
  </tr>
  <tr class="cc">
    <td colspan="4" class="bb">TB Screening and Treatment </td>
  </tr>
  <tr class="odd">
    <td>Screened TB </td>
    <td><label>Yes
        <input name="tb_screening" type="radio" value="y" <?=set_radio('tb_screening','y',('t'==$mydata->tb_screening))?>  required="required">
    </label>
      <label>No
      <input name="tb_screening" type="radio" value="n" <?=set_radio('tb_screening','n',('f'==$mydata->tb_screening))?> required="required">
      </label></td>
    <td>Started IPT Today</td>
    <td><label>Yes
        <input name="started_ipt" type="radio" value="y" <?=set_radio('started_ipt','y',('t'==$mydata->started_ipt))?> />
    </label>
      <label>No
      <input name="started_ipt" type="radio" value="n" <?=set_radio('started_ipt','n',('f'==$mydata->started_ipt))?>  >
      </label></td>
  </tr>
  <tr class="even">
    <td>On TB Treatment </td>
    <td><label>Yes
        <input name="tb_tx" type="radio" value="y" <?=set_radio('tb_tx','y',('t'==$mydata->tb_tx))?> required="required" />
    </label>
      <label>No
      <input name="tb_tx" type="radio" value="n" <?=set_radio('tb_tx','n',('f'==$mydata->tb_tx))?>  required="required" />
      </label></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr class="cc">
    <td colspan="4" class="bb">Prevention With Positive</td>
  </tr>
  <tr class="odd">
    <td>PWP Modern Contraceptive </td>
    <td><label>Yes
        <input name="pwp_contraceptive" type="radio" value="y" <?=set_radio('pwp_contraceptive','y',('t'==$mydata->pwp_contraceptive))?> />
    </label>
      <label>No
      <input name="pwp_contraceptive" type="radio" value="n" <?=set_radio('pwp_contraceptive','n',('f'==$mydata->pwp_contraceptive))?>  >
      </label></td>
    <td>PWP Provided With Condoms </td>
    <td><label>Yes
        <input name="pwp_condoms" type="radio" value="y" <?=set_radio('pwp_condoms','y',('t'==$mydata->pwp_condoms))?> />
    </label>
      <label>No
      <input name="pwp_condoms" type="radio" value="n" <?=set_radio('pwp_condoms','n',('f'==$mydata->pwp_condoms))?>  >
      </label></td>
  </tr>
  <tr bgcolor="#c4c6d4">
    <td colspan="4" class="bb">Nutrition</td>
  </tr>
  <tr class="odd">
    <td>Nutrition accessed </td>
    <td><label>Yes
        <input name="nutrition_accessed" type="radio" value="y" <?=set_radio('nutrition_accessed','y',('t'==$mydata->nutrition_accessed))?> />
    </label>
      <label>No
      <input name="nutrition_accessed" type="radio" value="n" <?=set_radio('nutrition_accessed','n',('f'==$mydata->nutrition_accessed))?> >
      </label></td>
    <td>Nutrition Malnorished </td>
    <td><label>Yes
        <input name="nutrition_malnorished" type="radio" value="y" <?=set_radio('nutrition_malnorished','y',('t'==$mydata->nutrition_malnorished))?> />
    </label>
      <label>No
      <input name="nutrition_malnorished" type="radio" value="n" <?=set_radio('nutrition_malnorished','n',('f'==$mydata->nutrition_malnorished))?> >
      </label></td>
  </tr>
  <tr class="even">
    <td>Nutrition food by prescription </td>
    <td><label>Yes
        <input name="nutrition_food_prescribed" type="radio" value="y" <?=set_radio('nutrition_food_prescribed','y',('t'==$mydata->nutrition_food_prescribed))?> />
    </label>
      <label>No
      <input name="nutrition_food_prescribed" type="radio" value="n" <?=set_radio('nutrition_food_prescribed','n',('f'==$mydata->nutrition_food_prescribed))?> >
      </label></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr class="cc">
    <td colspan="4" class="bb">Cancer</td>
  </tr>
  <tr class="odd">
    <td>Screened for Cancer </td>
    <td><label>Yes
        <input name="cancer_screening" type="radio" value="y" <?=set_radio('cancer_screening','y',('t'==$mydata->cancer_screening))?>  >
    </label>
      <label>No
      <input name="cancer_screening" type="radio" value="n" <?=set_radio('cancer_screening','n',('f'==$mydata->cancer_screening))?> >
      </label></td>
    <td>Pregnant</td>
    <td><label>Yes
        <input name="pregnant" type="radio" value="y" <?=set_radio('pregnant','y',('t'==$mydata->pregnant))?>  onchange="pgMale()" />
    </label>
      <label>No
      <input name="pregnant" type="radio" value="n" <?=set_radio('pregnant','n',('f'==$mydata->pregnant))?>  />
      </label></td>
  </tr>
  <tr class="cc">
    <td colspan="4" class="bb">Discordant & Key Population</td>
  </tr>
  <tr class="odd">
    <td>Discordant </td>
    <td><label>Yes
        <input name="dis_cou" id="dis_cou" type="radio" value="y" <?=set_radio('dis_cou','y',('t'==$mydata->dis_cou))?> >
    </label>
      <label>No
      <input name="dis_cou" id="dis_cou" type="radio" value="n" <?=set_radio('dis_cou','n',('f'==$mydata->dis_cou))?> >
      </label></td>
    <td>Key Population</td>
    <td><label>Yes
        <input name="key_pop" id="key_pop" type="radio" value="y"  <?=set_radio('dis_cou','y',('t'==$mydata->key_pop))?>  />
    </label>
      <label>No
      <input name="key_pop" id="key_pop" type="radio" value="n"  <?=set_radio('dis_cou','n',('f'==$mydata->key_pop))?> />
      </label></td>
  </tr>
  <tr class="cc">
    <td colspan="4" class="bb">Next Appointment Date  </td>
  </tr>
  <tr class="odd">
    <td>TCA</td>
    <td><input type="text" name="tca" id="tca" value="<?= set_value('tca',$mydata->tca)?>"  required="required" /></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr class="">
    <td colspan="4"><label>
      <div align="center">
        <input type="submit" name="Submit" value="Save" />
        </div>
    </label></td>
    </tr>
</table>

</form>
