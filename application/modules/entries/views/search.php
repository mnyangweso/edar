<script type="text/javascript" src="<?=base_url()?>assets/js/jquery-2.1.0.js"></script> 
<script type="text/javascript" src="<?=base_url()?>assets/js/datatables.js"></script>
<link href="<?=base_url()?>assets/css/datatables.css" rel="stylesheet" type="text/css" media="all"> 


<script language="javascript">
$(document).ready(function() {
    var table = $('#example').DataTable({
	//"responsive": true,        
	"paging":   true,
        "ordering": true,
        "info":     true,
	"scrollX": 300,
	"scrollY": 300,
	"select": true,	
	"autoWidth": true,
	
	 buttons: [
		  
		'print', 		
		{
                extend: 'copyHtml5',
                exportOptions: {columns: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23 ] }
            	},
		{
                extend: 'pdfHtml5',
                exportOptions: {columns: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23 ] },
		orientation: 'landscape',
                pageSize: 'A3'
		//,download: 'open'
            	},
		{
                extend: 'excelHtml5',
                exportOptions: {columns: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23] }
		//exportOptions: {columns: [ 0, ':visible' ]}
            	},
		{
                extend: 'csvHtml5',
                exportOptions: {columns: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23] }
            	},
		{
                    extend: 'colvis',
                    text: 'Hide / Show Column',
                    exclude: [1],
		    container : '#btncolvis',	
		    collectionLayout: 'four-column',
          	    postfixButtons: [ 'colvisRestore' ],
		   columns: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23 ],
		    exclude: [0,1,2,3],
				    
                }, 
		{
                extend: 'colvisGroup',
                text: 'Show all',
                show: ':hidden'
            	},
		{
                extend: 'colvisGroup',
                text: 'Hide all',
                hide: ':visible'
            	},
		{
        	extend: 'columnsToggle',
		text: 'Show xx',
        	columns: '.toggle'
    		},
		
		
		
		
		
		//,'columnsToggle'
		//,'mColumns': [ 0, 1, 4 ]
		//,exportOptions: { "columns": ':visible'}
			
		]
		
      
	
	,"columnDefs": [ {
            "targets": -1,
            "data": null,
           // "defaultContent": '<a href="<?=base_url()?>welcome/edit/4">Edit</a> | <a href="">Delete</a>',
	  //  "defaultContent": '<button> Edit</button>',
	    "orderable": false,
	    "searchable": false, "targets":-1,
		render: function(data, type, full, meta){
          		if(type === 'display'){
             		data = '<div class="links">' +
                 	'<a href="<?=base_url()?>entries/edit/'+data[1] +'"><img src="http://localhost/edar/assets/icons/action-edit.png"></a> ' +
                 	'&nbsp; &nbsp; <a href="<?=base_url()?>entries/delete/'+data[1] +
			'" onClick="return confirm(\'Are you sure you want to delete this entry\')"><img src="http://localhost/edar/assets/icons/action-delete.png"></a>' +
                 '</div>';                     
          	}

          return data;
       }
        } ]
	

	
	
	 			
    });
	

	table.buttons().containers().appendTo( '#btn' );


	
	/*

	new $.fn.dataTable.Buttons( table, {
    buttons: [
        {
            extend: 'collection',
            buttons: [ 'columnsToggle' ]
        }
    ]
} );	


	$('#example tbody').on( 'click', 'button', function () {
        var data = table.row( $(this).parents('tr') ).data();
        alert( data[0] +"'s salary is: "+ data[ 5 ] );
    	} );
	
	
	$(window).load( function () {
	$('#example').dataTable().fnAdjustColumnSizing( false );
	} );
	*/
	
} );
// select distinct on (a.ccc_no) a.ccc_no, a.sex, a.dob,array_to_json( ARRAY(select e.visit_date from dar e where e.ccc_no=a.ccc_no ) ) as visits from dar a order by a.ccc_no
</script>

<style>
.tr{color: #d9edf7;	background-color:#5bc0de;  }
.tda{background:#e7f5f9}
.tdb{background-color:#d0e6ef; }
div.container { width: 80%;  }

/* jQuery UI - v1.11.4 
.ui-helper-clearfix:before, .ui-helper-clearfix:after {content: ""; display: table; border-collapse: collapse;}
.ui-helper-clearfix:after {clear: both;}
.ui-helper-clearfix {position: relative;min-height: 0;}
.ui-state-disabled {cursor: default !important;}
.ui-button {display: inline-block;position: relative;overflow: visible;}
.ui-button .ui-button-text {display: block;line-height: normal;}
.ui-button-text-only .ui-button-text {padding: .4em 1em;}
.ui-buttonset {margin-right: 7px;}
.ui-buttonset .ui-button {margin-left: 0;   margin-right: -.1em;}
.ui-widget-header {border: 1px solid black;background: #5c9ccc;color: #ffffff;font-weight: bold;margin: -.1em 0;}
.ui-state-default, .ui-widget-header .ui-state-default, table.dataTable tfoot th:hover {border: 1px solid black;background: #d7ebf9;font-weight: bold;color: #2779aa;cursor: pointer;}
th.sorting.ui-state-default.sorting_asc, th.sorting.ui-state-default.sorting_desc {border: 1px solid black;background: #3baae3;font-weight: bold;color: #ffffff;}
.ui-state-default:hover, .ui-widget-header .ui-state-default:hover {border: 1px solid black;background: #79c9ec;font-weight: bold;color: #026890;}
th.sorting.ui-state-default.sorting_asc:hover, th.sorting.ui-state-default.sorting_desc:hover {border: 1px solid black;background: #e4f1fb;font-weight: bold;color: #0070a3;}
.ui-state-active,.ui-widget-header .ui-state-active {border: 1px solid black;background: #3baae3;font-weight: bold;color: #ffffff;}
.ui-widget-header .ui-state-disabled {background: #e4f1fb;color: #0070a3;opacity: .35;filter:Alpha(Opacity=35);}
.ui-widget-header:hover .ui-state-disabled {background: #e4f1fb;color: #0070a3;opacity: .35;filter:Alpha(Opacity=35);}
 */

/* Buttons for DataTables 1.1.0 */
div.dt-buttons {position: relative;float: left;font: 0.9em Arial; padding-bottom: 0.25em;}
div.dt-buttons .dt-button {margin: 0.25em 0.333em 0.25em 0;}
div.dt-button-collection {font: 0.9em Arial;position: absolute;margin-top: 3px; padding: 4px;border: 1px solid #ccc;background-color: #fff;overflow:hidden; z-index: 2002;}
div.dt-button-collection .dt-button {text-align: center;position: relative;display: block;margin-right: 0;width: 100px;}
div.dt-button-background {zoom: 1;position: fixed;top: 0; left: 0;width: 100%;height: 100%;background: #000;filter: alpha(opacity=35);opacity: .35;z-index: 2001;}
a.dt-button.buttons-select-cells, a.dt-button.buttons-select-rows, a.dt-button.buttons-select-columns {position: relative; width: 175px;text-align: left;}
a.dt-button.buttons-columnVisibility.ui-button-text-only {padding-left: 10px;}
a.dt-button.buttons-columnVisibility.ui-state-active, a.dt-button.buttons-columnVisibility {position: relative; width: auto;*width: 230px;text-align: left;
*zoom: expression(this.runtimeStyle.zoom="1",this.insertBefore(document.createElement("div"),this.childNodes[0]).className="before",this.appendChild(document.createElement("div")).className="after");}
a.dt-button.buttons-columnVisibility:before, a.dt-button.buttons-columnVisibility.ui-state-active:after {display: block;position: absolute;top: 1em;margin-top: -8px;margin-left: -6px;width: 16px;height: 16px;box-sizing: border-box;}
a.dt-button.buttons-columnVisibility:before {content: ' ';border: 2px solid black;border-radius:3px;}
a.dt-button.buttons-columnVisibility.ui-state-active:after {font: 0.9em Arial;content: '\2714';text-align: center;}
*a.dt-button.buttons-columnVisibility .before,*a.dt-button.buttons-columnVisibility.ui-state-active .after,*a.dt-button.buttons-columnVisibility.ui-state-active:hover .after {position: absolute;margin: 6px 0px 0px -6px;width: 16px;height: 16px;background-image: url("checkbox.png");background-position: 0px 0px;}
*a.dt-button.buttons-columnVisibility .before {background-position: 0px 0px;}
*a.dt-button.buttons-columnVisibility.ui-state-active .before {background-position: 0px 16px;}
*a.dt-button.buttons-columnVisibility.ui-state-active:hover .before {background-position: 16px 0px;}

</style>

<div style="width:100%;">
<div id="btn" class="btn" style="width:auto; float:left;"> </div> 
<div id="btncolvis" class="btncolvis" style="width:auto; float:left; background-color:#ccc"> </div> 
</div>


<table id="example" class="display" width="80" frame="box" cellpadding="2" cellspacing="2" style="width:80%; font-size:10px; width: 100%!important">
<thead>

<tr>
<td colspan="25" bgcolor="teal">  </td>
</tr>

<tr class="tr">
<th>row id</th>
<th>id</th>
<th>ccc no</th>
<th>sex</th>
<th>dob</th>
<th>visit date</th>
<th>visit type</th>
<th>scheduled visit</th>
<th>tca</th>
<th>prophylaxis</th>
<th>enrolled in care</th>
<th>started on art</th>
<th>tb screening</th>
<th>started ipt</th>
<th>pwp FP</th>
<th>pwp condoms</th>
<th>cancer screening</th>
<th>nutrition accessed</th>
<th>nutrition malnorished</th>
<th>nutrition food prescribed</th>
<th>art status</th>
<th>pregnant</th>
<th>age</th>
<th>tbtx</th>
<th>Action</th>
</tr>
</thead>
        
<tbody>

<?php foreach($query->result() as $row): ?>
<tr class="<?= alternator('tda', 'tdb') ?>">
<td> <?=$row->row_id?> </td>
<td> <?=$row->id?> </td>
<td> <?=$row->ccc_no?> </td>
<td> <?=$row->sex?> </td>
<td> <?=$row->dob?> </td>
<td> <?=$row->visit_date?> </td>
<td> <?=$row->visit_type?> </td>
<td> <?=$row->scheduled_visit?> </td>
<td> <?=$row->tca?> </td>
<td> <?=$row->prophylaxis?> </td>
<td> <?=$row->enrolled_in_care?> </td>
<td> <?=$row->started_on_art?> </td>
<td> <?=$row->tb_screening?> </td>
<td> <?=$row->started_ipt?> </td>
<td> <?=$row->pwp_contraceptive?> </td>
<td> <?=$row->pwp_condoms?> </td>
<td> <?=$row->cancer_screening?> </td>
<td> <?=$row->nutrition_accessed?> </td>
<td> <?=$row->nutrition_malnorished?> </td>
<td> <?=$row->nutrition_food_prescribed?> </td>
<td> <?=$row->art_status?> </td>
<td> <?=$row->pregnant?> </td>
<td> <?=$row->age?> </td>
<td> <?=$row->tb_tx?> </td>
<th> </th>
</tr>
  <?php endforeach; ?>

</tbody>
</table>
