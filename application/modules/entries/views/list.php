<table width="100%" border="0" cellspacing="3" cellpadding="3" frame="box">
  <tr bgcolor="#c4c6d4">
    <td><strong>#</strong></td>
    <td><strong>CCC NO </strong></td>
    <td><strong>Visit Date </strong></td>
    <td colspan="2"><strong>Action</strong></td>
  </tr>
  <?php $i = 0; ?>
  <?php foreach($query->result() as $row): ?>
 <?php	$i++;	?> 
  <tr class="<?= alternator('odd', 'even') ?>">
    <td><?=$i?></td>
    <td><?=$row->ccc_no?></td>
    <td><?=$row->visit_date?></td>
    <td><span class="edit_action">
      <?= anchor('entries/edit/index/'.$row->id,' Edit Entry',array('class' => '')) ?>
    </span></td>
    <td><span class="edit_action">
      <?= anchor('entries/delete/index/'.$row->id,' Delete Entry',array("class" => "","onClick"=>"return confirm('Are you sure you want to delete this entry')")) ?>
    </span></td>
  </tr>
  <?php endforeach; ?>
</table>


