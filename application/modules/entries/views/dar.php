
<style>
.bb{font-weight:bold;}
.cc{background-color:#c4c6d4; }
</style>

<script language="javascript">




</script>

<?php echo validation_errors(); ?>


<table width="100%" frame="box" cellspacing="2" cellpadding="2" style="background-color:#FFFFFF">
  <tr class="cc">
    <td colspan="4" class="bb">Bio Data</td>
  </tr>
  <tr class="odd">
    <td>CCC No</td>
    <td><input type="number" name="ccc_no" id="ccc_no" list="ccc" required >
		<datalist id="ccc">
			<?php foreach ($query->result() as $row): ?>   
			<option value="<?=$row->ccc_no;?>"> 
			<?php endforeach;?>
		</datalist>	</td>
    <td>Sex</td>
    <td><label>
      <select name="sex" id="sex"  required="required" onchange="femaleCheck(this.value)">
        <option value="">-- Select Sex --</option>
        <option value="male">Male</option>
        <option value="female">Female</option>
      </select>
    </label></td>
  </tr>
  <tr class="even">
    <td>Date Of Birth</td>
    <td>
      <input type="text" name="dob" id="dob" required class="myinvalid" />    </td>
    <td>Age</td>
    <td><input type="number" name="age" id="age" maxlength="3" min="0" max="140" onchange="myFunction(this.value)" /></td>
  </tr>
  <tr class="cc">
    <td colspan="4" class="bb">Appointment</td>
  </tr>
  <tr class="odd">
    <td>Visit Date</td>
    <td><label>
      <input type="text" id="visit_date" name="visit_date" required="required" />
    </label></td>
    <td>Scheduled Visit </td>
    <td><label>Yes
      <input name="scheduled_visit" type="radio" value="y"/>
      </label>
        <label>No
          <input name="scheduled_visit" type="radio" value="n"/>
      </label> </td>
  </tr>
  <tr class="cc">
    <td colspan="4" class="bb">Medication and Enrollment </td>
  </tr>
  <tr class="odd">
    <td>Prophylaxis</td>
    <td><label>None
      <input name="prophylaxis" type="radio" value=" " required="required" />
      </label>
        <label>CTX
          <input name="prophylaxis" type="radio" value="ctx" required="required" />
        </label>
        <label>Dapson
          <input name="prophylaxis" type="radio" value="dapson" required="required" />
        </label>    </td>
    <td>Client Type 
      <label></label><label></label></td>
    <td><label></label>
    <label>Pre-ART Client
        <input id="pre_art" name="pre_art" type="radio" value="y" required="required" />
</label>
      <label>ART Client
      <input id="pre_art" name="pre_art" type="radio" value="n" required="required" />
      </label></td>
  </tr>
  <tr class="even">
    <td>Enrolled In Care
      <label></label>
      <label>Today</label></td>
    <td><label></label><label>Yes
        <input name="enrolled_in_care" type="radio" value="y" required="required" />
</label>
      <label>No
      <input name="enrolled_in_care" type="radio" value="n" required="required" />
      </label></td>
    <td>Started On ART Today </td>
    <td><label></label><label></label><label>Yes
        <input name="started_on_art" type="radio" value="y" required="required" onchange="preARTcheck()" />
</label>
      <label>No
      <input name="started_on_art" type="radio" value="n" required="required" />
      </label></td>
  </tr>
  <tr class="cc">
    <td colspan="4" class="bb">TB Screening and Treatment </td>
  </tr>
  <tr class="odd">
    <td>Screened TB </td>
    <td><label>Yes
        <input name="tb_screening" type="radio" value="y" required>
    </label>
      <label>No
      <input name="tb_screening" type="radio" value="n" required>
      </label></td>
    <td>Started IPT Today</td>
    <td><label>Yes
        <input name="started_ipt" type="radio" value="y">
    </label>
      <label>No
      <input name="started_ipt" type="radio" value="n">
      </label></td>
  </tr>
  <tr class="even">
    <td>On TB Treatment </td>
    <td><label>Yes
        <input name="tb_tx" type="radio" value="y" required="required" />
    </label>
      <label>No
      <input name="tb_tx" type="radio" value="n" required="required" />
      </label></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr class="cc">
    <td colspan="4" class="bb">Prevention With Positive</td>
  </tr>
  <tr class="odd">
    <td>PWP Modern Contraceptive </td>
    <td><label>Yes
        <input name="pwp_contraceptive" type="radio" value="y">
    </label>
      <label>No
      <input name="pwp_contraceptive" type="radio" value="n">
      </label></td>
    <td>PWP Provided With Condoms </td>
    <td><label>Yes
        <input name="pwp_condoms" type="radio" value="y">
    </label>
      <label>No
      <input name="pwp_condoms" type="radio" value="n">
      </label></td>
  </tr>
  <tr class="cc">
    <td colspan="4" class="bb">Nutrition</td>
  </tr>
  <tr class="odd">
    <td>Nutrition accessed </td>
    <td><label>Yes
        <input name="nutrition_accessed" type="radio" value="y">
    </label>
      <label>No
      <input name="nutrition_accessed" type="radio" value="n">
      </label></td>
    <td>Nutrition Malnorished </td>
    <td><label>Yes
        <input name="nutrition_malnorished" type="radio" value="y">
    </label>
      <label>No
      <input name="nutrition_malnorished" type="radio" value="n">
      </label></td>
  </tr>
  <tr class="even">
    <td>Nutrition food by prescription </td>
    <td><label>Yes
        <input name="nutrition_food_prescribed" type="radio" value="y">
    </label>
      <label>No
      <input name="nutrition_food_prescribed" type="radio" value="n">
      </label></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr class="cc">
    <td colspan="4" class="bb">Cancer</td>
  </tr>
  <tr class="odd">
    <td>Screened for Cancer </td>
    <td><label>Yes
        <input name="cancer_screening" id="cancer_screening" type="radio" value="y">
    </label>
      <label>No
      <input name="cancer_screening" id="cancer_screening" type="radio" value="n">
      </label></td>
    <td>Pregnant</td>
    <td><label>Yes
        <input name="pregnant" id="pregnant" type="radio" value="y" onchange="pgMale()" />
    </label>
      <label>No
      <input name="pregnant" id="pregnant" type="radio" value="n" />
      </label></td>
  </tr>
   <tr class="cc">
    <td colspan="4" class="bb">Discordant & Key Population</td>
  </tr>
  <tr class="odd">
    <td>Discordant </td>
    <td><label>Yes
        <input name="dis_cou" id="dis_cou" type="radio" value="y">
    </label>
      <label>No
      <input name="dis_cou" id="dis_cou" type="radio" value="n">
      </label></td>
    <td>Key Population</td>
    <td><label>Yes
        <input name="key_pop" id="key_pop" type="radio" value="y" />
    </label>
      <label>No
      <input name="key_pop" id="key_pop" type="radio" value="n" />
      </label></td>
  </tr>
  <tr class="cc">
    <td colspan="4" class="bb">Next Appointment Date  </td>
  </tr>
  <tr class="odd">
    <td>TCA</td>
    <td><input type="text" id="tca" name="tca" required="required" /></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr class="">
    <td colspan="4"><label>
      <div align="center">
        <input type="submit" name="Submit" value="Save"; />
        </div>
    </label></td>
    </tr>
</table>

