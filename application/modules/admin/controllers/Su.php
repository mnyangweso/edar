<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Su extends MX_Controller {

	function __construct()
	{
		parent::__construct();
		$this->pbscms->_check_logged();
		$this->form_validation->set_error_delimiters('<div class="ferror">', '</div>');
		$this->config->load('pbs',TRUE);
	}

//sudo apt-get install php-apigen naturaldocs doxygen	
	function index()
	{

		$this->form_validation->set_rules('mfl','mfl','trim|required');		
								
		if ($this->form_validation->run() == FALSE){ 

			$data['vcc']= $this->session->userdata('mfl_code');
			$pv = $this->config->item('province', 'pbs');
	
			//$this->db->where('province', $pv);
			$this->db->where('site_id IS NOT NULL');
			
			$this->db->order_by('province ASC, county ASC, facility_name ASC, facility_code ASC');
			$data['con'] = $this->db->get('mfl');
		
						
			$data['title'] = "Site Configuration";			
			$data['content'] = $this->load->view('admin/su_new',$data,TRUE);
			$this->load->view('template',$data);
		}else{
		
						
			$nn = array('facility_code'=>$this->input->post('mfl') );
			//unset($nn['Submit']); 
			//$this->db->where('', 1);
			$this->db->update('ci_config', $nn);
			
			redirect('admin/su');
		}
	
	}

	
	function mfl()
	{

		$this->form_validation->set_rules('mfl_code','mfl_code','trim|required');		
								
		if ($this->form_validation->run() == FALSE){ 
		
			//$dbname = $this->config->item('hostname', 'database');
			
			//echo "____".$this->db->database."____"; die();
			$data['con'] = $this->db->get('ci_config');
						
			$data['title'] = "Site Configuration";			
			$data['content'] = $this->load->view('admin/su_new',$data,TRUE);
			$this->load->view('template',$data);
		}else{
		
						
			$nn = $this->input->post();
			unset($nn['Submit']); 
			//$this->db->update('ci_config', $nn);
			$res = NULL;
			$this->db->trans_start();
			foreach ($nn as $key=>$value)
			{
				$sql = "UPDATE ci_config SET description = '".ucwords(strtolower($value))."' WHERE name = '$key'";
				//echo "<br/>". $sql;
				$res = $this->db->query($sql);
			}
			 
			$this->db->trans_complete();
			//print_r( $nn ); die();
			redirect('admin/su/index/');
		}
	
	}
	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
