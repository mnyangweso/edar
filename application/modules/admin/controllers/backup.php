<?php

class Backup extends  CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->pbscms->_check_logged();
		session_start();
		//$this->pbscms->_check_rlogged();
		$this->load->library('zip');
		$this->form_validation->set_error_delimiters('<div class="ferror">', '</div><br/>');

	}
	
	
	function backups()
	{
		$xx = date('d-m-Y H_i_s');
		$dd = "/tmp/cpad_full_backup_$xx.sql";
		$ddzz = "/tmp/cpad_full_backup_$xx.zip";
		
		
		$cmd = "pg_dump -h localhost -U postgres -i -O -v online > '$dd'";
		//echo "\n";
		$output = shell_exec($cmd);
		echo "<pre>$output</pre>";
		$this->zip->read_file($dd);
		
		echo $dd."<br/>".$ddzz; die();
		$this->zip->download($ddzz,FALSE);
		
		$this->session->set_flashdata('flash', 'true');
		$this->session->set_flashdata('flashtype', 'success');
		$this->session->set_flashdata('flashmessage', 'full backup has been succesfully created');
		//redirect('admin/check_entries');
	}
	
	
	
	
	
	function index()
	{	
		
		
		//delete_files($pp,TRUE);		
		if ( function_exists('sys_get_temp_dir')) 
		$tmpdir = sys_get_temp_dir();
		else if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') 
		$tmpdir = "/temp";
		else
		$tmpdir = "/tmp";
		
		//delete_files($pp,TRUE);		
		if ( function_exists('sys_get_temp_dir')) 
		$tmpdir = sys_get_temp_dir();
		else if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') 
		$tmpdir = "/temp";
		else
		$tmpdir = "/tmp";
		
		$created = date('d-m-Y H_i_s');
		$sqlcsv = "
COPY (SELECT * FROM tblpatient_information) TO '$tmpdir/tblpatient_information_$created.csv' WITH DELIMITER ',' CSV QUOTE '\"' HEADER;
COPY (SELECT * FROM tblvisit_information) TO '$tmpdir/tblvisit_information_$created.csv' WITH DELIMITER ',' CSV QUOTE '\"' HEADER;  
COPY (SELECT * FROM \"tblARTInterruptions\") TO '$tmpdir/tblartinterruptions_$created.csv' WITH DELIMITER ',' CSV QUOTE '\"' HEADER;  
COPY (SELECT * FROM tbllost_to_followup) TO '$tmpdir/tbllost_to_followup_$created.csv' WITH DELIMITER ',' CSV QUOTE '\"' HEADER;  
COPY (SELECT * FROM tblsubstitutecodesreg) TO '$tmpdir/tblsubstitutecodesreg_$created.csv' WITH DELIMITER ',' CSV QUOTE '\"' HEADER;  
COPY (SELECT * FROM tblsubstituteregreason) TO '$tmpdir/tblsubstituteregreason_$created.csv' WITH DELIMITER ',' CSV QUOTE '\"' HEADER;
COPY (SELECT * FROM tblfpmethod) TO '$tmpdir/tblfpmethod_$created.csv' WITH DELIMITER ',' CSV QUOTE '\"' HEADER;
COPY (SELECT * FROM tbladdress) TO '$tmpdir/tbladdress_$created.csv' WITH DELIMITER ',' CSV QUOTE '\"' HEADER;

";		

			$zipfilename = "patient_info_$created.zip";

			$query = $this->db->query($sqlcsv);
			
			$this->zip->read_file("$tmpdir/tblpatient_information_$created.csv");
			$this->zip->read_file("$tmpdir/tblvisit_information_$created.csv"); 
			$this->zip->read_file("$tmpdir/tblartinterruptions_$created.csv");
			$this->zip->read_file("$tmpdir/tbllost_to_followup_$created.csv");
			$this->zip->read_file("$tmpdir/tblsubstitutecodesreg_$created.csv");
			$this->zip->read_file("$tmpdir/tblsubstituteregreason_$created.csv"); 
			$this->zip->read_file("$tmpdir/tblfpmethod_$created.csv");
			$this->zip->read_file("$tmpdir/tbladdress_$created.csv");
			
			
			//$this->zip->read_dir($pp); 
			$this->zip->download($zipfilename,FALSE);
				
			$this->session->set_flashdata('flash', 'true');
			$this->session->set_flashdata('flashtype', 'success');
			$this->session->set_flashdata('flashmessage', 'classified '.$this->input->post('name').' has been succesfully added');
			redirect('admin/check_entries');
	}
	
	
	function deactivate()
	{	
		$data_update = array('isactive' => 'false');
		$this->db->where('id', $this->uri->segment(4));
		$this->db->update('classified', $data_update); 
		
		$this->session->set_flashdata('flash', 'true');
		$this->session->set_flashdata('flashtype', 'success');
		$this->session->set_flashdata('flashmessage', 'Classified menu item  has been succesfully deactivated');
			
		redirect('admin/classified/');	
	}
	
	
	
	
	
}

/* End of file categories.php */
/* Location: ./system/application/controllers/admin/categories.php */
