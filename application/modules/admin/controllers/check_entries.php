<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Check_entries extends CI_Controller {




	function __construct()
	{
		parent::__construct();
		$this->pbscms->_check_logged();		
		$this->form_validation->set_error_delimiters('<div class="ferror">', '</div>');
		session_start();
	}


	public function index()
	{
		 
		
			$sql ="select created::date,min(updated) as first_entry, max(updated) as last_entry,max(updated) - min(updated) as tlapse from tblvisit_information group by created::date order by created::date desc";	
			
			$data['row_num'] = $this->db->query($sql)->num_rows();
			$config['base_url'] = base_url().'admin/check_entries/index';
			$config['total_rows'] = $data['row_num'];
			$config['per_page'] = '15'; 
			$config['uri_segment'] = 4;				
			
			$this->db->limit($config['per_page'],$this->uri->segment(4)); 		
			$data['entries'] = $this->db->query($sql); 
			
			//print_r($data['entries']); die();	
			
			
			$data['title'] = "Entry times by Day";
			$data['content'] = $this->load->view('check_entries',$data, TRUE);
			$this->load->view('template',$data);
		
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
