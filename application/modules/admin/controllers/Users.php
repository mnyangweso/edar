<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->pbscms->_check_logged();
		$this->form_validation->set_error_delimiters('<div class="ferror">', '</div><br/>');
		//session_start();	
	}

	//used to list users
	function index()
	{
		
		$data['query'] = $this->db->get_where('users',array('isactive' =>'true'));
		$data['row_num'] = $data['query']->num_rows();
		$config['base_url'] = base_url().'admin/users/index';

		$config['total_rows'] = $data['row_num'];
		$config['per_page'] = '10'; 
		$config['uri_segment'] = 3;	
		
		$this->pagination->initialize($config);
		
		$this->db->order_by('username', 'asc');
		$data['query'] = $this->db->get_where('users',array('isactive' =>'true'),$config['per_page'],$this->uri->segment(3));
		//print_r($data['query']);
		$data['title'] = "List Users";
		$data['content'] = $this->load->view('admin/users/list',$data, TRUE);
		$this->load->view('template',$data);
	}
	
	function inactive()
	{
		$data['query'] = $this->db->get_where('users',array('isactive' =>'false'));
		$data['row_num'] = $data['query']->num_rows();
		$config['base_url'] = 'users/index';

		$config['total_rows'] = $data['row_num'];
		$config['per_page'] = '10'; 
		$config['uri_segment'] = 4;	
		
		$this->pagination->initialize($config);
		
		$this->db->order_by('username', 'asc');
		$data['query'] = $this->db->get_where('users',array('isactive' =>'false'),$config['per_page'],$this->uri->segment(4) );
		$data['title'] = "List Users";
		$data['content'] = $this->load->view('admin/users/inactive',$data, TRUE);
		$this->load->view('template',$data);
	}

	function add()
	{
		$this->form_validation->set_rules('username', 'user name', 'trim|required|min_length[4]|max_length[20]|unique[users.username]');
		$this->form_validation->set_rules('first_name', 'first name', 'trim|required|min_length[4]|max_length[20]');
		$this->form_validation->set_rules('last_name', 'last name', 'trim|required|min_length[4]|max_length[20]');
		
		
		$this->form_validation->set_rules('pwd', 'Password', 'required|min_length[4]|max_length[20]|matches[pwd2]');	
		$this->form_validation->set_rules('pwd2', 'confirm password', 'required|min_length[4]|max_length[20]||matches[pwd]');
		$this->form_validation->set_rules('email', 'Email', 'required|min_length[4]|max_length[60]|valid_email');	
		
		$this->form_validation->set_rules('pwd', 'password', 'trim|required');
		$this->form_validation->set_rules('pwd2', 'confirm password', 'trim|required');
			

		
		if ($this->form_validation->run() == FALSE){
			
			
			$data['title'] = "Add Users";
			//$data['tiny_mce'] = $this->load->view('tiny_mce',$data, TRUE);
			$data['content'] = $this->load->view('users/add',$data, TRUE);
			$this->load->view('template',$data);

		}else{
			
			$salt = time();
			$pwdhashed = sha1($_POST['pwd'].$salt);
			
			$nn = $this->input->post();
			$nn['salt'] = $salt;
			$nn['pwd'] = $pwdhashed;
			
			unset($nn['Submit']);
			unset($nn['pwd2']); 
			//print_r( $nn );
			//die();
			$this->db->insert('users',$nn);
			$this->session->set_flashdata('flash', 'true');
			$this->session->set_flashdata('flashtype', 'success');
			$this->session->set_flashdata('flashmessage', 'user '.$this->input->post('username').' has been succesfully added');
			redirect('admin/users/add');
		}
		
	}

	function edit()
	{
		//$this->form_validation->set_rules('username', 'user name', 'trim|required|min_length[4]|max_length[20]|unique[users.username]');
		$this->form_validation->set_rules('first_name', 'first name', 'trim|required|min_length[4]|max_length[20]');
		$this->form_validation->set_rules('last_name', 'last name', 'trim|required|min_length[4]|max_length[20]');
		$this->form_validation->set_rules('middle_name', 'middle name', 'trim');
		$this->form_validation->set_rules('email', 'Email', 'required|min_length[4]|max_length[60]|valid_email');	
				

		
		if ($this->form_validation->run() == FALSE){
			$data['query'] = $this->db->get_where('users', array('id'=>$this->uri->segment(4) ) );		
			$data['title'] = "Edit Uses";
			$data['content'] = $this->load->view('users/edit',$data, TRUE);
			$this->load->view('template',$data);

		}else{
			
					
			$datauser = array(				
				'first_name' => $this->input->post('first_name'),
				'last_name' => $this->input->post('last_name'),
				'middle_name' => $this->input->post('middle_name'),
				'email'  => $this->input->post('email')
			);
			
			$this->db->where('id', $this->uri->segment(4));
			$this->db->update('users',$datauser);
			$this->session->set_flashdata('flash', 'true');
			$this->session->set_flashdata('flashtype', 'success');
			$this->session->set_flashdata('flashmessage', 'user '.$this->input->post("first_name").' '. $this->input->post("last_name").' has been succesfully edited');
			redirect('admin/users');
		}
		
	}

	
	function view()
	{	
		$data['query'] = $this->db->get_where('users', array('id'=>$this->uri->segment(4) ) );	
		
		$data['title'] = "View User";
		$data['content'] = $this->load->view('users/view',$data, TRUE);
		$this->load->view('template',$data);
	}

	function deactivate()
	{	
		$data_update = array('isactive' => 'false');
		$this->db->where('id', $this->uri->segment(4));
		$this->db->update('users', $data_update); 
		
		$this->session->set_flashdata('flash', 'true');
		$this->session->set_flashdata('flashtype', 'success');
		$this->session->set_flashdata('flashmessage', 'user  has been succesfully deactivated');
			
		redirect('admin/users');	
	}

	
	function activate()
	{	
		$data_update = array('isactive' => 'true');
		$this->db->where('id', $this->uri->segment(4));
		$this->db->update('users', $data_update); 
		
		$this->session->set_flashdata('flash', 'true');
		$this->session->set_flashdata('flashtype', 'success');
		$this->session->set_flashdata('flashmessage', 'users  has been succesfully activated');
			
		redirect('admin/users/inactive');	
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
