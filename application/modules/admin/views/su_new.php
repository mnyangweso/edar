<link href="../../../../../assets/css/main.css" rel="stylesheet" type="text/css" />
<style>
.tbr{color: #d9edf7;	background-color:#5bc0de; font-weight:bold;  }
.tda{background:#e7f5f9}
.tdb{background-color:#d0e6ef; }
</style>


<table width="100%" border="0" cellspacing="2" cellpadding="2" frame="box">
  <tr class="tbr">
    <td colspan="4">MFL INFORMATION </td>
  </tr>
  <tr class="odd">
    <td>MFL Information </td>
    <td colspan="2"><?= form_error('mfl') ?>	
	<select name="mfl" id="mfl" size="1" onchange='updateMFL(this.value)' style="width:65% !important">
		<option value="">-- Select MFL Code --</option>
		<?php foreach($con->result() as $con):?>
	   <option value="<?=$con->facility_code?>" <?=set_select('mfl',$con->facility_code,($con->facility_code==$vcc))?>><?=$con->province?>****<?=$con->county?>****<?=$con->district?>****<?=$con->facility_name?>****<?=$con->facility_code?>****<?=$con->site_id?></option>
	   <?php endforeach; ?>
	  </select></td>
    <td>&nbsp;</td>
  </tr>
  <tr class="even">
    <td>MFL Code </td>
	
    <td><input name="facility_code" type="text" id="facility_code"  value="" readonly=""/></td>
    <td>Facility Name </td>
    <td><input name="facility_name" type="text" id="facility_name"  value="" readonly=""/></td>
  </tr>
  <tr  class="odd">
    <td>Site ID </td>
    <td><input name="site_id" type="text" id="site_id"  value="" readonly=""/></td>
    <td>Province</td>
    <td><input name="province" type="text" id="province"  value="" readonly=""/></td>
  </tr>
  <tr  class="even">
    <td>County </td>
    <td><input name="county" type="text" id="county"  value="" readonly=""/></td>
    <td>District</td>
    <td><input name="district" type="text" id="district"  value="" readonly=""/></td>
  </tr>
   <tr class="tbr">
    <td colspan="4">SYSTEM INFORMATION </td>
  </tr>
   <tr class="odd">
     <td>E-Dar Version </td>
     <td><input name="edar_version" type="text" id="edar_version" value="<?=$this->config->item('version', 'pbs');?>.<?=$this->config->item('major_version', 'pbs');?>.<?=$this->config->item('minor_version', 'pbs');?>"  readonly="readonly"/></td>
     <td>Database Name </td>
     <td><input name="dbname" type="text" id="dbname" value="<?=$this->db->database?>"  readonly="readonly"/></td>
   </tr>
   <tr class="even">
     <td>Database Platform </td>
     <td><input name="db_plat" type="text" id="db_plat" value="<?=$this->db->platform()?>"  readonly="readonly"/></td>
     <td>Database Version </td>
     <td><input name="dbversion" type="text" id="dbversion" value="<?=$this->db->version()?>"  readonly="readonly"/></td>
   </tr>
   <tr class="odd">
     <td>PHP Version </td>
     <td><input name="php_ver" type="text" id="php_ver" value="<?=PHP_VERSION?>"  readonly="readonly"/></td>
     <td>Operating System </td>
     <td><input name="os_type" type="text" id="os_type" value="<?=PHP_OS?>"  readonly="readonly"/></td>
   </tr>
   <tr class="even">
     <td>Server Software </td>
     <td><input name="server_software" type="text" id="server_software" value="<?=$this->input->server('SERVER_SOFTWARE')?>"  readonly="readonly"/></td>
     <td>Server Port </td>
     <td><input name="server_port" type="text" id="server_port" value="<?=$this->input->server('SERVER_PORT')?>"  readonly="readonly"/></td>
   </tr>
   <tr class="odd">
     <td>Document Root </td>
     <td><input name="doc_root" type="text" id="doc_root" value="<?=$this->input->server('DOCUMENT_ROOT')?>"  readonly="readonly"/></td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
   </tr>
   <tr>
    <td colspan="4"><div align="center"><span style="text-align:center">
      <input type="submit" name="Submit" value="Save" />
    </span></div></td>
  </tr>
</table>
<p>&nbsp;</p>

<script language="javascript">


$(document).ready(function(){  
/*code here*/
updateMFL();
});



function updateMFL(varr){

if (varr !== undefined)
{
	var r = confirm("You will be changing the MFL Code\n And the rest of Vital Information \n Press OK to proceed or Press Cancel to Mantain the current config");
	if (r != true){ 
		$('#mfl').val(<?=$vcc?>);
		return false;
	}
}

var mm = $('#mfl option:selected') .text() ;
var ar = mm.split('****');

$("#province").val(ar[0]);
$("#county").val(ar[1]);
$("#district").val(ar[2]);
$("#facility_name").val(ar[3]);
$("#facility_code").val(ar[4]);
$("#site_id").val(ar[5]);

}

</script>