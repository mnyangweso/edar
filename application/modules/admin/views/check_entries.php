<script type="text/javascript">
$(document).ready(function() {
	$(function() {
		$( "#first_name" ).autocomplete({
			source: function(request, response) {
				$.ajax({ url: "<?php echo site_url('patient/search/suggestions'); ?>",
				data: { term: $("#first_name").val()},
				dataType: "json",
				type: "POST",
				success: function(data){
					response(data);
				}
			});
		},
		minLength: 1
		});
	});
});
</script>
<br/>


<table id="searchtable" width="100%">
<thead>
<tr style="font-weight:bold;  color:#003f77; font-size:14px;">
    <td>&nbsp;</td>
	<td>Date of entries </td>
    <td>First Entry  Time </td>
    <td>Last Entry Time </td>
    <td>Time lapse Between Entries </td>
    </tr>
  </thead>
  <?php $i = 0; ?>
 <?php foreach($entries->result() as $row): ?>
 <?php	$i++;	?> 
 
 <tbody>
  <tr class="<?= alternator('odd', 'even') ?>" >
    <td><?= $i +$this->uri->segment(4)?></td>
	 <td><?=$row->created ?></td>
    <td><?=$row->first_entry ?></td>
    <td><?=$row->last_entry ?></td>
    <td><?=$row->tlapse ?></td>
    </tr>
  </tbody>
  <?php endforeach; ?>
  
  <tfoot>
  <tr >
    <td colspan="5">&nbsp;</td>
    </tr>
  </tfoot>
</table>

<div class="pagination" style="text-align:center"><?php  echo $this->pagination->create_links();?></div>

<script type="text/javascript">
$('#patient_id').numeric();
$('#fileno').numeric();		
</script>
