
<form method="post">
 <?php foreach($query->result() as $row): ?>
  <?php endforeach; ?>

  
<fieldset> <legend> Biodata </legend>
<table width="100%" border="0" cellspacing="2" cellpadding="2" >
  <tr>
    <td class="right"><strong>First Name * </strong></td>
    <td ><?= form_error('first_name'); ?><input name="first_name" type="text" id="first_name" value="<?=$row->first_name ?><?= set_value('first_name'); ?>"  /></td>
    <td class="right"><strong>Last Name * </strong></td>
    <td ><?= form_error('last_name'); ?><input name="last_name" type="text" id="last_name" value="<?= set_value('last_name'); ?><?=$row->last_name ?>" /></td>
  </tr>
  <tr>
    <td class="right"><strong>Middle Name </strong></td>
    <td >
      <input name="middle_name" type="text" id="middle_name" value="<?= set_value('middle_name'); ?><?=$row->middle_name?>" /></td>
    <td class="right"><strong>Email *</strong></td>
    <td ><?= form_error('email'); ?>
      <input name="email" type="text"  id="email" value="<?=$row->email ?><?= set_value('email'); ?>" /></td>
  </tr>
</table>
</fieldset>
<fieldset> <legend> Login Information</legend>

<table width="100%" border="0" cellspacing="2" cellpadding="2">
  <tr>
    <td class="right"><div align="left"><strong>Username </strong></div></td>
    <td><?=$row->username ?></td>
    <td class="right"><div align="left"><strong>User Role </strong></div></td>
    <td><?= $this->pbscms->_retfield('roles','name',$row->role_id); ?></td>
  </tr>
  <tr>
    <td >&nbsp;</td>
    <td class="right"><input name="Reset" type="reset"  value="Reset" />    </td>
    <td><input type="submit" name="Submit" value="Submit"/></td>
    <td>&nbsp;</td>
  </tr>
</table>
</fieldset>
  
</form>
