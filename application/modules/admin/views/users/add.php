
<?php /*echo validation_errors(); */ ?>
<form method="post">
<fieldset> <legend> Biodata </legend>
<table width="100%" border="0" cellspacing="2" cellpadding="2" >
  <tr>
    <td class="right">First Name * </td>
    <td ><?= form_error('first_name'); ?><input name="first_name" type="text" id="first_name" value="<?= set_value('first_name'); ?>" /></td>
    <td class="right">Last Name * </td>
    <td ><?= form_error('last_name'); ?><input name="last_name" type="text" id="last_name" value="<?= set_value('last_name'); ?>" /></td>
  </tr>
  <tr>
    <td class="right">Middle Name </td>
    <td >
      <input name="middle_name" type="text" id="middle_name" value="<?= set_value('middle_name'); ?>" /></td>
    <td class="right">Email *</td>
    <td ><?= form_error('email'); ?>
      <input name="email" type="text"  id="email" value="<?= set_value('email'); ?>" /></td>
  </tr>
</table>
</fieldset>
<fieldset> <legend> Login Information</legend>

<table width="100%" border="0" cellspacing="2" cellpadding="2">
  <tr>
    <td class="right">Username * </td>
    <td><?= form_error('username') ?>
      <input name="username" type="text" id="username" value="<?= set_value('username'); ?>"  /></td>
    <td class="right">User Role * </td>
    <td><?= form_error('role_id') ?>
      <select name="role_id" id="role_id">
      <option value="1" <?=set_select('role', '1')?> >Administrator</option>
      <option value="2" <?=set_select('role', '2',TRUE)?> >Data Clerk</option>
        </select></td>
  </tr>
  <tr>
    <td class="right">Password * </td>
    <td><?= form_error('pwd') ?>
        <input name="pwd" type="password" id="pwd" value="<?= set_value('pwd'); ?>" >    </td>
    <td class="right">Confirm Password * </td>
    <td><?= form_error('pwd2') ?>
        <input name="pwd2" type="password" id="pwd2" value="<?= set_value('pwd2'); ?>" /></td>
  </tr>
  <tr>
    <td >&nbsp;</td>
    <td class="right"><input name="Reset" type="reset"  value="Reset" />    </td>
    <td><input type="submit" name="Submit" value="Submit"/></td>
    <td>&nbsp;</td>
  </tr>
</table>


</fieldset>

</form>