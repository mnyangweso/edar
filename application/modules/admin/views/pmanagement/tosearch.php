<script type="text/javascript">
$(document).ready(function() {
	$(function() {
		$( "#first_name" ).autocomplete({
			source: function(request, response) {
				$.ajax({ url: "<?php echo site_url('patient/search/suggestions'); ?>",
				data: { term: $("#first_name").val()},
				dataType: "json",
				type: "POST",
				success: function(data){
					response(data);
				}
			});
		},
		minLength: 1
		});
	});
});
</script>

<table width="100%" border="0">
  <tr>
    <td width="14%" style="font-weight:bold;  color:#003f77; font-size:14px;">Search By</td>
    <td width="31%">&nbsp;</td>
    <td width="4%">&nbsp;</td>
    <td width="18%">&nbsp;</td>
    <td width="33%">&nbsp;</td>
  </tr>
  <tr>
    <td>Unique Patient Number</td>
    <td><?= form_error('patient_id') ?><input name="patient_id" type="text" id="patient_id"   value="<?=$patient_id?>" /></td>
    <td>&nbsp;</td>
    <td>Patient Clinic Number</td>
    <td><?= form_error('fileno') ?><input name="fileno" type="text" id="fileno" value="<?=$fileno?>" /></td>
  </tr>
  <tr>
    <td> First Name</td>
    <td><?= form_error('first_name') ?><input name="first_name" type="text" id="first_name"  value="<?=$first_name?>" /></td>
    <td>&nbsp;</td>
    <td>Last Name(s)</td>
    <td><input name="last_name" type="text" id="last_name"  value="<?=$last_name?>" //></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><label>
      <input type="submit" name="Submit" value="Submit" />
    </label></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<br/>


<table id="searchtable" width="100%" >
<thead>
<tr style="font-weight:bold;  color:#003f77; font-size:14px;">
    <td>&nbsp;</td>
	<td>Unique Patient No</td>
    <td>Clinic Number</td>
    <td>First Name</td>
    <td>Last Name(s)</td>
    <td><div align="center">Re Transfer in the Patient </div></td>
    </tr>
  </thead>
  <?php $i = 0; ?>
 <?php foreach($query->result() as $row): ?>
 <?php	$i++;	?> 
 
 <tbody>
  <tr class="<?= alternator('odd', 'even') ?>" >
    <td><?= $i +$this->uri->segment(4)?></td>
	 <td><?=$row->patient_id ?></td>
    <td><?=$row->fileno ?></td>
    <td><?=$row->first_name ?></td>
    <td><?=$row->last_name ?></td>
    <td  class="edit_action"> <?= anchor('admin/pmanagement/change_to/'.$row->patient_id,' Change TO Status',array('class' => '')) ?></td>
  </tr>
  </tbody>
  <?php endforeach; ?>
  
  <tfoot>
  <tr >
    <td colspan="9">&nbsp;</td>
    </tr>
	</tfoot>
</table>

<div class="pagination" style="text-align:center"><?php  echo $this->pagination->create_links();?></div>

<script type="text/javascript">
$('#patient_id').numeric();
$('#fileno').numeric();		
</script>
