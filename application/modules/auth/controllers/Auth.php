<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->form_validation->set_error_delimiters('<div class="ferror">', '</div>');
		//session_start();
	}

	function index()
	{
	
		$this->form_validation->set_rules('uname', 'User Name', 'required|min_length[2]|max_length[30]');	
		$this->form_validation->set_rules('pwd', 'password', 'required|min_length[2]|max_length[30]');	
		
		if ($this->form_validation->run() == FALSE){	
		
		if (strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome') !== false
			 || strpos($_SERVER['HTTP_USER_AGENT'], 'CriOS') !== false) 
				 	$this->session->set_flashdata('is_chrome','true');
				else 
					$this->session->set_flashdata('is_chrome','false');
		
		$ses_user_data = array(
				'logged' => '',
				'username' => '',
				'first_name' => '',
				'last_name' => '',
				'id' =>'',
				'role_id' => ''
		);
		$this->session->set_userdata($ses_user_data);				
			$data['title'] = "User Login";
			$this->load->view('new_login',$data);	
			
		}else{
		
			$username =  $this->input->post('uname');
			$pwd = $this->input->post('pwd');
			$data = array(
			   'username' => $username,
			   'pwd' => $pwd 
			);
			$this->db->where(array('isactive' => 'true','username' => $username));
			$query = $this->db->get('users');
			
			$this->db->select('ci_config.facility_code,mfl.facility_name,mfl.site_id,mfl.district,mfl.county,mfl.province');
			$this->db->from('ci_config');
			$this->db->join('mfl', 'mfl.facility_code = ci_config.facility_code');
			$q2 = $this->db->get()->row();
			
			
				
				//check if the users exists
				if($query->num_rows()>0){
					$rowdata = $query->row_array();

					$salt =  $rowdata['salt'];
					$pwdhashed =  $rowdata['pwd'];			
					$pwdhashed2 = sha1($pwd.$salt);
					$role_id = $rowdata['role_id'];		
					
							//check if the passwords match
							//cause the user exists
							if($pwdhashed2 == $pwdhashed){
								$ses_user_data = array(
									'logged' => 'yes',
									'username' => $rowdata['username'],
									'first_name' => $rowdata['first_name'],
									'last_name' => $rowdata['last_name'],
									'id' => $rowdata['id'],
									'role_id' => $rowdata['role_id']
									); 
				//( [facility_code] => 13657 [facility_name] => Katito Health Centre [site_id] => 1868 [district] => Nyakach [county] => Kisumu [province] => Nyanza )		
							 	$ses_user_data['mfl_code'] = $q2->facility_code;
								$ses_user_data['mfl_name'] = $q2->facility_name;
								$ses_user_data['site_id'] = $q2->site_id;
								$ses_user_data['district'] = $q2->district;
								$ses_user_data['county'] = $q2->county;
								$ses_user_data['province'] = $q2->province;
								
								
																	
							 $this->session->set_userdata($ses_user_data);	
							 redirect(base_url());
							}else{
								$this->session->set_flashdata('flash', 'true');
								$this->session->set_flashdata('flashtype', 'warning');
								$this->session->set_flashdata('flashmessage', 'The password entered is wrong');
								redirect('auth');
							}
				}else{
										
					$this->session->set_flashdata('flash', 'true');
					$this->session->set_flashdata('flashtype', 'error');
					$this->session->set_flashdata('flashmessage', 'No such user exists');
					redirect('auth');
				}
		
		
		}
				
		
	}
	
	
	function logout()
	{
		//print_r($this->session->all_userdata()); die();
		//$this->session->sess_destroy();
		$this->session->set_flashdata('flash','true');
		$this->session->set_flashdata('flashtype','success');
		$this->session->set_flashdata('flashmessage', "You have succesfully logged out.");
		redirect('auth');
	}
	
	function change_password()
	{
			$this->pbscms->_check_logged();
			$this->form_validation->set_rules('old-pwd', 'password', 'required|min_length[4]|max_length[60]');
			$this->form_validation->set_rules('pwd', 'New password', 'required|min_length[4]|max_length[60]|matches[pwd2]');
			$this->form_validation->set_rules('pwd2', 'confirm  new password', 'required|min_length[4]|max_length[60]|matches[pwd]');
			
			if ($this->form_validation->run() == FALSE){					
				$data['title'] = "Change Password";
				$data['content'] = $this->load->view('pwdchange',$data, TRUE);
				$this->load->view('template',$data);
					
			}else{
				$query = $this->db->get_where('users', array('id' => $this->session->userdata('id'),'isactive' => '1' ) );
				$usersinfo = $query->row_array();

					if(!($usersinfo['pwd'] == sha1($_POST['old-pwd'].$usersinfo['salt']))){					
						$this->session->set_flashdata('flash', 'true');
						$this->session->set_flashdata('flashtype', 'error');
						$this->session->set_flashdata('flashmessage', 'The old passwords does not match');
						redirect($_SERVER['HTTP_REFERER']);
					}
					
					$time = time();
					$pwdhashed = sha1($_POST['pwd'].$time);
					$data_update = array(
					   'salt' => $time ,
					   'pwd' => $pwdhashed
					);
					$this->db->where('id',$this->session->userdata('id'));
					$this->db->update('users', $data_update);
					
					$this->session->set_flashdata('flash', 'true');
					$this->session->set_flashdata('flashtype', 'success');
					$this->session->set_flashdata('flashmessage', 'Password successfully changed');
					redirect('');
			}		
	}
	
	function forgot_password()
	{
	
				$this->pbscms->_check_logged();
				$this->form_validation->set_rules('uname', 'user name', 'required|min_length[2]|max_length[60]');
				$this->form_validation->set_rules('email', 'email', 'required|min_length[4]|max_length[60]|valid_email');
				
				if ($this->form_validation->run() == FALSE){
						$data['title'] = "Reset Forgotten Password";
						//$this->load->view('pwdrecover');
						$data['content'] = $this->load->view('pwdrecover',$data, TRUE);
						$this->load->view('pbscar',$data);
				}else{ 
				
						$query = $this->db->get_where( 'users',array('username' => $_POST['uname'],'email' =>$_POST['email'],'isactive' => '1' ) );
						if($query->num_rows()!=1){
							
							$this->session->set_flashdata('flash','true');
							$this->session->set_flashdata('flashtype','error');
							$this->session->set_flashdata('flashmessage','The email and username combination do no match');
							redirect('auth/forgot_password');
						}else{
							
							$pwd = random_string('alnum',8);
							$time = time();
							$pwdhashed = sha1($pwd.$time);
							$data_update = array(
							   'salt' => $time ,
							   'pwd' => $pwdhashed
							);
							$this->db->where('username', $_POST['uname']);
							$this->db->update('users', $data_update);
							
							$config['wordwrap'] = TRUE;
							$config['mailtype'] = 'html';
							$config['protocol'] = 'smtp';
							$config['smtp_host'] = 'penguinet.com';
							$config['email_address'] = 'info@ladivalink.com';
							$this->email->initialize($config);				
							$this->email->from('no-reply@wananchi.com', 'Palm Travel');
							$this->email->to($_POST['email']);
							$this->email->subject('Password reset');
							$this->email->message('Your password has been successfully reset.<br/> Your new password is '.$pwd.' Please reset it after logging in.');
							$this->email->send();
							
							$this->session->set_flashdata('flash','true');
							$this->session->set_flashdata('flashtype','success');
							$this->session->set_flashdata('flashmessage','Password successfully reset. Please check your email');
							
							$data['title'] = "";
							//$data['content'] = $this->load->view('login',$data, TRUE);
							redirect('auth');
						
						
						}
				
				
				
				}
	
	}
	

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
