<link href="../../../../assets/css/forms.css" rel="stylesheet" type="text/css" />
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr class="bgnone">
    <td colspan="2" id="tbl-legend">Login</td>
  </tr>
  <tr class="bgnone">
    <td colspan="2" ><hr size="1" noshade color="#999999"/></td>
  </tr>
  <tr class="bgnone">
    <td width="50%" class="tbl-label">User Name *</td>
    <td width="50%"><?= form_error('uname') ?>
      <input name="uname" type="text" id="uname" class="loginName" placeholder="Enter Username" /></td>
  </tr>
  <tr class="bgnone">
    <td width="50%" class="tbl-label">Password *</td>
    <td width="50%"><?= form_error('pwd') ?>
      <input name="pwd" type="password" class="loginPass" id="pwd"  placeholder="Enter Password" /></td>
  </tr>
  <tr class="bgnone">
  	<!-- ffd30d cb480c -->
    <td colspan="2"><hr noshade="noshade" color="#999999" size="1"/></td>
  </tr>
  <tr class="bgnone">
    <td>&nbsp;</td>
    <td width="50%"><div align="left">
  <input name="button" type="button" onclick="history.back()"class="button" value=" Back " />
  &nbsp;
  <input type="submit" name="Submit" value="Login" />
    </div></td>
  </tr>
  <tr class="bgnone">
    <td><div align="center"></div></td>
    <td><div align="left">
	<?= anchor('admin/employees/add','Please Register',array('title' => 'Please Register'));?>
&nbsp;&nbsp; &nbsp;

  <?= anchor('auth/forgot_password','Forgotten Password?');?>
</div></td>
  </tr>
</table>

