<link href="<?=base_url()?>css/template.css" rel="stylesheet" type="text/css" />


<link href="../../../../assets/css/forms.css" rel="stylesheet" type="text/css" />
<form method="post">

<table>
  <tr class="bgnone">
    <td colspan="2" id="tbl-legend">Login</td>
  </tr>
  <tr class="bgnone">
    <td colspan="2" ><hr size="1" noshade color="#999999"/></td>
  </tr>
  <tr class="bgnone">
    <td class="tbl-label">User Name *</td>
    <td ><div align="left">
      <?= form_error('uname') ?>
      <input name="uname" type="text" class="loginName" id="uname" placeholder="username" />
    </div></td>
  </tr>
  <tr class="bgnone">
    <td class="tbl-label" >Password *</td>
    <td><div align="left">
      <?= form_error('pwd') ?>
      <input name="pwd" type="password" class="loginPass"  id="pwd" placeholder="password" />
    </div></td>
  </tr>
  <tr class="bgnone">
    <td colspan="2" ><hr size="1" noshade="noshade" color="#999999"/></td>
  </tr>
  <tr class="bgnone">
    <td >&nbsp;</td>
    <td>
      
        <div align="left">
          <input name="button" type="button" onclick="history.back()"class="button" value=" Back " /> &nbsp;&nbsp;&nbsp;
           <input type="submit" name="Submit" value="Login">      
        </div></td>
  </tr>
  <tr>
    <td ></td>
    <td><div align="left">
      <?= anchor('admin/employees/add','Please Register',array('title' => 'Please Register'));?>
&nbsp;&nbsp; &nbsp;
<?= anchor('auth/forgot_password','Forgotten Password?');?>
</div></td>
  </tr>
</table>

</form>
