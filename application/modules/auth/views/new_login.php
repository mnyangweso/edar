
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
<title>E-DAR Login</title>
<link href="<?=base_url()?>assets/icons/favicon.ico" rel="shortcut icon" type="image/x-png">
<link href="<?=base_url()?>assets/css/admin.css" rel="stylesheet" type="text/css" media="all">
<link href="<?=base_url()?>assets/css/login.css" rel="stylesheet" type="text/css" media="all">
<link href="<?=base_url()?>assets/css/forms.css" rel="stylesheet" type="text/css" media="all">
<link href="<?=base_url()?>assets/css/flash.css" rel="stylesheet" type="text/css" media="all">
<link href="<?=base_url()?>assets/css/paginate.css" rel="stylesheet" type="text/css" media="all">

<script type="text/javascript" src="<?=base_url()?>assets/js/jquery-2.1.0.js"></script>
  
</head>

<body>
<!--
<div id="logo1"  style=" float:left; position:absolute; left:5px; top:5px;">
<img src="<?=base_url()?>assets/images/logos/ICAP logo-kenya.png">
</div>

<div id="logo2"  style=" float:left; position:absolute; right:5px; top:5px;">
<img src="<?=base_url()?>assets/images/logos/moh-logo-kenya.png">
</div>

<div id="logo3"  style=" float:left; position:absolute; left:5px; bottom:5px;">
<img src="<?=base_url()?>assets/images/logos/pepfar-kenya.png">
</div>

<div id="logo2"  style=" float:left; position:absolute; right:5px; bottom:5px;">
<img src="<?=base_url()?>assets/images/logos/cdc-kenya.png">
</div>
-->

<script language="javascript">
<?php if($this->session->flashdata('is_chrome')=='false'):?>
alert('Please Note that this application works best with Chrome');
<?php endif; ?>
</script>

<div class="login-box">
	<div class="login-title"> <img src="<?=base_url()?>assets/images/e-dar.png"> </div>	
		<div class="lform">
			<form method="post">				
				<div><label>User Name * </label></div>
				<div><input type="text" class="logintxt" name="uname" placeholder="Enter User Name" title="Enter User Name" value="" /></div>
				<div><label>Password *</label></div>
				<div><input name="pwd" type="password" class="loginpwd" id="pwd" placeholder="Enter your password." title="Enter your password." value="" /></div>
				<div> <input type="submit" name="Submit" value="Log In" style="width:90px !important" /></div>
			</form>
		</div>
	</div>


</body>
</html>

