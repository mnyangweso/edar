<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search extends CI_Controller {

/*
 * code by Abhishek R Kaushik
 * Downloaded from http://devzone.co.in
 */
 	function __construct()
	{
		parent::__construct();
		//$this->pbscms->_check_logged();
		//$this->form_validation->set_error_delimiters('<div class="ferror">', '</div>');
		//session_start();
	}
	
	
	public function index()
	{
		$data['title'] = "User Search";
		$data['content'] = $this->load->view('search',$data, TRUE);
		$this->load->view('template',$data);	
	}
        
        public function suggestions(){
		$search = strtolower($this->input->post('term'));
		//$search = 'ad';
		if (strlen($search) < 2) break;
		$query = $this->db->get_where('users',array('isactive'=>'true','first_name ~*'=> $search) );
		//print_r($query); die();
		$keywords = array();
		
			foreach ($query->result() as $row)
				 array_push($keywords, $row->first_name);
		//print_r($keywords);
		echo json_encode($keywords);
	}
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
