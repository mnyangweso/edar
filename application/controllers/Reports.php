<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->pbscms->_check_logged();
		$this->form_validation->set_error_delimiters('<div class="ferror">', '</div>');
		//session_start();
	}
	
	

	public function new_moh366()
	{
		

		$this->form_validation->set_rules('start_date','Start Date','trim|required');
		//$this->form_validation->set_rules('end_date','End Date','trim|required');
		$this->form_validation->set_rules('site_name','Facility Name','trim');
		
		
		if ($this->form_validation->run() == FALSE){ 
			
			$data['title'] = "MOH 366 Register";
			$data['content'] = $this->load->view('rep_dar',$data,TRUE);
			$this->load->view('template',$data);

			
		}else{
			$sql = "WITH mike AS (
		select *,extract(year from age(visit_date,dob)) as yrs,ROW_NUMBER() OVER (ORDER BY ccc_no) AS rn,extract (month from age(tca,visit_date)) as dur_mnths from dar order by visit_date asc 

		)SELECT rn as a,visit_date as b,visit_type,ccc_no,ccc_no AS c,
---- on ctx 
(SELECT 'Y' WHERE prophylaxis IS NOT NULL AND yrs < 2 ) AS d, 
(SELECT 'Y' WHERE prophylaxis IS NOT NULL AND yrs BETWEEN 2 AND 9) AS e, 
(SELECT 'Y' WHERE prophylaxis IS NOT NULL AND sex = 'male' AND yrs BETWEEN 10 AND 14 ) AS f,
(SELECT 'Y' WHERE prophylaxis IS NOT NULL AND sex = 'female' AND yrs BETWEEN 10 AND 14 ) AS g,
(SELECT 'Y' WHERE prophylaxis IS NOT NULL AND sex = 'male' AND yrs BETWEEN 15 AND 19 ) AS h,
(SELECT 'Y' WHERE prophylaxis IS NOT NULL AND sex = 'female' AND yrs BETWEEN 15 AND 19 ) AS i,
(SELECT 'Y' WHERE prophylaxis IS NOT NULL AND sex = 'male' AND yrs BETWEEN 20 AND 24 ) AS j,
(SELECT 'Y' WHERE prophylaxis IS NOT NULL AND sex = 'female' AND yrs BETWEEN  20 AND 24  ) AS k,
(SELECT 'Y' WHERE prophylaxis IS NOT NULL AND sex = 'female' AND yrs > 24 ) AS l,
(SELECT 'Y' WHERE prophylaxis IS NOT NULL AND sex = 'female' AND yrs > 14) AS m,
(SELECT NULL ) AS n,

---- in care revist
(SELECT 'Y' WHERE visit_type = 'r' AND prophylaxis IS NOT NULL AND yrs < 2) AS o,
(SELECT 'Y' WHERE visit_type = 'r' AND prophylaxis IS NOT NULL AND yrs BETWEEN 2 AND 9) AS p, 
(SELECT 'Y' WHERE visit_type = 'r' AND sex = 'male' AND prophylaxis IS NOT NULL AND yrs BETWEEN 10 AND 14) AS q, 
(SELECT 'Y' WHERE visit_type = 'r' AND sex = 'female' AND prophylaxis IS NOT NULL AND yrs BETWEEN 10 AND 14) AS r, 
(SELECT 'Y' WHERE visit_type = 'r' AND sex = 'male' AND prophylaxis IS NOT NULL AND yrs BETWEEN 15 AND 19) AS s, 
(SELECT 'Y' WHERE visit_type = 'r' AND sex = 'female' AND prophylaxis IS NOT NULL AND yrs BETWEEN 15 AND 19) AS t,
(SELECT 'Y' WHERE visit_type = 'r' AND sex = 'male' AND prophylaxis IS NOT NULL AND yrs BETWEEN 20 AND 24) AS u, 
(SELECT 'Y' WHERE visit_type = 'r' AND sex = 'female' AND prophylaxis IS NOT NULL AND yrs BETWEEN 20 AND 24) AS v,
(SELECT 'Y' WHERE visit_type = 'r' AND sex = 'male' AND prophylaxis IS NOT NULL AND yrs > 24) AS w, 
(SELECT 'Y' WHERE visit_type = 'r' AND sex = 'female' AND prophylaxis IS NOT NULL AND yrs > 24) AS x,
(SELECT NULL ) AS y,


----start art
(SELECT 'Y' WHERE started_on_art = 't' AND yrs < 2 ) AS z,
(SELECT 'Y' WHERE started_on_art = 't' AND yrs BETWEEN 2 AND 9) AS aa, 
(SELECT 'Y' WHERE started_on_art = 't' AND sex = 'male' AND yrs BETWEEN 10 AND 14) AS ab,
(SELECT 'Y' WHERE started_on_art = 't' AND sex = 'female' AND yrs BETWEEN 10 AND 14) AS ac,
(SELECT 'Y' WHERE started_on_art = 't' AND sex = 'male' AND yrs BETWEEN 15 AND 19) AS ad,
(SELECT 'Y' WHERE started_on_art = 't' AND sex = 'female' AND yrs BETWEEN 15 AND 19) AS ae,
(SELECT 'Y' WHERE started_on_art = 't' AND sex = 'male' AND yrs BETWEEN 20 AND 24) AS af,
(SELECT 'Y' WHERE started_on_art = 't' AND sex = 'female' AND yrs BETWEEN 20 AND 24) AS ag,
(SELECT 'Y' WHERE started_on_art = 't' AND sex = 'male' AND yrs > 24) AS ah,
(SELECT 'Y' WHERE started_on_art = 't' AND sex = 'female' AND yrs > 24) AS ai,
(SELECT NULL ) AS aj,
(SELECT NULL ) AS ak,
(SELECT NULL ) AS al,

---revisits  on art
(SELECT 'Y' WHERE started_on_art = 't' AND visit_type = 'r' AND yrs < 2 ) AS am,
(SELECT 'Y' WHERE started_on_art = 't' AND visit_type = 'r' AND yrs BETWEEN 2 AND 9 ) AS an,
(SELECT 'Y' WHERE started_on_art = 't' AND visit_type = 'r' AND sex = 'male' AND yrs BETWEEN 10 AND 14) AS ao,
(SELECT 'Y' WHERE started_on_art = 't' AND visit_type = 'r' AND sex = 'female' AND yrs BETWEEN 10 AND 14) AS ap,
(SELECT 'Y' WHERE started_on_art = 't' AND visit_type = 'r' AND sex = 'male' AND yrs BETWEEN 15 AND 19) AS aq,
(SELECT 'Y' WHERE started_on_art = 't' AND visit_type = 'r' AND sex = 'female' AND yrs BETWEEN 15 AND 19) AS ar,
(SELECT 'Y' WHERE started_on_art = 't' AND visit_type = 'r' AND sex = 'male' AND yrs BETWEEN 20 AND 24) AS ass,
(SELECT 'Y' WHERE started_on_art = 't' AND visit_type = 'r' AND sex = 'female' AND yrs BETWEEN 20 AND 24) AS at,
(SELECT 'Y' WHERE started_on_art = 't' AND visit_type = 'r' AND sex = 'male' AND yrs > 24) AS au,
(SELECT 'Y' WHERE started_on_art = 't' AND visit_type = 'r' AND sex = 'female' AND yrs > 24) AS av,
(SELECT NULL ) AS aw,

---- on ctx and dapson
(SELECT 'Y' WHERE prophylaxis IS NOT NULL AND yrs < 2 ) AS ax, 
(SELECT 'Y' WHERE prophylaxis IS NOT NULL AND yrs BETWEEN 2 AND 9) AS ay, 
(SELECT 'Y' WHERE prophylaxis IS NOT NULL AND yrs BETWEEN 10 AND 14 ) AS az,
(SELECT 'Y' WHERE prophylaxis IS NOT NULL AND yrs BETWEEN 15 AND 19 ) AS ba,
(SELECT 'Y' WHERE prophylaxis IS NOT NULL AND yrs BETWEEN 20 AND 24 ) AS bb,
(SELECT 'Y' WHERE prophylaxis IS NOT NULL AND yrs > 24 ) AS bc,

---- screened for tb
(SELECT 'Y' WHERE tb_screening = 't' AND yrs < 2 ) AS bd, 
(SELECT 'Y' WHERE tb_screening = 't' AND yrs BETWEEN 2 AND 9) AS be, 
(SELECT 'Y' WHERE tb_screening = 't' AND yrs BETWEEN 10 AND 14 ) AS bf,
(SELECT 'Y' WHERE tb_screening = 't' AND yrs BETWEEN 15 AND 19 ) AS bg,
(SELECT 'Y' WHERE tb_screening = 't' AND yrs BETWEEN 20 AND 24 ) AS bh,
(SELECT 'Y' WHERE tb_screening = 't' AND yrs > 24 ) AS bi,


---- started on ipt
(SELECT 'Y' WHERE started_ipt = 't' AND yrs < 2 ) AS bj, 
(SELECT 'Y' WHERE started_ipt = 't' AND yrs BETWEEN 2 AND 9) AS bk, 
(SELECT 'Y' WHERE started_ipt = 't' AND yrs BETWEEN 10 AND 14 ) AS bl,
(SELECT 'Y' WHERE started_ipt = 't' AND yrs BETWEEN 15 AND 19 ) AS bm,
(SELECT 'Y' WHERE started_ipt = 't' AND yrs BETWEEN 20 AND 24 ) AS bn,
(SELECT 'Y' WHERE started_ipt = 't' AND yrs > 24 ) AS bo,

--enrolled in care
(SELECT 'Y' WHERE nutrition_accessed = 't' AND yrs < 15) AS bp,
(SELECT 'Y' WHERE nutrition_accessed = 't' AND yrs > 14) AS bq,
(SELECT 'Y' WHERE nutrition_malnorished = 't' AND yrs < 15) AS br,
(SELECT 'Y' WHERE nutrition_malnorished = 't' AND yrs > 14) AS bs,
(SELECT 'Y' WHERE nutrition_food_prescribed = 't' AND yrs < 15) AS bt,
(SELECT 'Y' WHERE nutrition_food_prescribed = 't' AND yrs > 14) AS bu, 

--cancer
(SELECT 'Y' WHERE cancer_screening = 't' AND yrs > 14) AS bv,
(SELECT 'Y' WHERE yrs > 17 AND sex = 'female') AS bw

---------------------------------------------------------aggreagate-----------------------------------------------------

 from mike
";
			$data['mydata'] = $this->db->query($sql);
			//print_r($data['mydata']->row_array()); die();
			$data['content'] = $this->load->view('mohdar_new',$data,TRUE);
			//$this->load->view('mohdar_new',$data);
			//die();
			
	$excel_file_name = now().".xls";
			  
	header("Content-type: application/octet-stream");//A MIME attachment with the content type "application/octet-stream" is a binary file. 
	//Typically, it will be an application or a document that must be opened in an application, such as a spreadsheet or word processor.  
        header("Content-Disposition: attachment; filename=$excel_file_name");//with this extension of file name you tell what kind of file it is. 
	header("Pragma: no-cache");//Prevent Caching 
	header("Expires: 0");//Expires and 0 mean that the browser will not cache the page on your hard drive
	force_download($excel_file_name,$data['content']);
/**/
			
		}
						
		
	}



	public function moh731_new()
	{
		

		$this->form_validation->set_rules('mntyr','Month Year','trim|required');
				
		if ($this->form_validation->run() == FALSE){ 
			
			$data['title'] = "MOH 366 Register";
			//$data['content'] = $this->load->view('moh731_new',$data,TRUE);
			//$this->load->view('template',$data);

			$this->load->view('moh731_new',$data);
			
			
		}else{
			$var = explode(" ",$this->input->post('mntyr'));
			$mnt = $var[0];
			$yr = $var[1];
			$siku = "'01-$mnt-$yr'";
			//print_r($siku); die();
			//SELECT (date_trunc('MONTH', $1) + INTERVAL '1 MONTH - 1 day')::DATE;
			$sql = "
				WITH mike AS (
				SELECT d.*,EXTRACT(YEAR FROM AGE(visit_date,dob)) as last_visit_age,ROW_NUMBER() OVER (ORDER BY ccc_no) AS rn,
				EXTRACT (MONTH from AGE(tca,visit_date)) AS dur_mnths, 
				(
				SELECT v.visit_date AS last_visit_date FROM dar v WHERE 
				v.visit_date 
				BETWEEN ($siku::DATE - INTERVAL '3 MONTH') AND (date_trunc('MONTH', $siku::DATE) + INTERVAL '1 MONTH - 1 day')::DATE
				AND v.ccc_no = d.ccc_no ORDER BY v.visit_date DESC LIMIT 1
				) 
				FROM dar d 
				WHERE d.visit_date
				BETWEEN ($siku::DATE - INTERVAL '3 MONTH') AND (DATE_TRUNC('MONTH', $siku::DATE) + INTERVAL '1 MONTH - 1 day')::DATE 
				ORDER BY d.visit_date ASC 
				)
				SELECT 
				--(SELECT COUNT(DISTINCT ccc_no) FROM mike ) AS ca				
				--(SELECT COUNT(DISTINCT ccc_no) FROM mike WHERE DATE_PART('MONTH',visit_date)= $mnt ) AS ca

--------------------------------------------------------3.1 On Cotrimoxazole Prophylaxis
(SELECT COUNT(ccc_no) FROM mike WHERE prophylaxis IS NOT NULL ) AS ctx_ca, 
(SELECT COUNT(ccc_no) FROM mike WHERE prophylaxis IS NOT NULL and sex='male' and last_visit_age > 14 ) AS ctx_am, 
(SELECT COUNT(ccc_no) FROM mike WHERE prophylaxis IS NOT NULL and sex='male' and last_visit_age < 15 ) AS ctx_cm,
(SELECT COUNT(ccc_no) FROM mike WHERE prophylaxis IS NOT NULL and sex='female' and last_visit_age > 14 ) AS ctx_af, 
(SELECT COUNT(ccc_no) FROM mike WHERE prophylaxis IS NOT NULL and sex='female' and last_visit_age < 15 ) AS ctx_cf, 

---------------------------------------------------------------- 3.3 Currently in Care
(SELECT COUNT(ccc_no)  FROM mike ) AS curr_care_ca,
(SELECT COUNT(ccc_no)  FROM mike WHERE enrolled_in_care = 't' AND sex='male' and last_visit_age > 14 ) AS curr_care_am,
(SELECT COUNT(ccc_no)  FROM mike WHERE enrolled_in_care = 't' AND  sex='male' and last_visit_age < 15  ) AS curr_care_cm,
(SELECT COUNT(ccc_no)  FROM mike WHERE enrolled_in_care = 't' AND  sex='female' and last_visit_age > 14 ) AS curr_care_af,
(SELECT COUNT(ccc_no)  FROM mike WHERE enrolled_in_care = 't' AND  sex='female' and last_visit_age < 15 ) AS curr_care_cf,
(SELECT COUNT(ccc_no)  FROM mike WHERE enrolled_in_care = 't' AND  last_visit_age < 1 ) AS curr_care_ci,

-------------------------------------------------------------------3.6
(SELECT COUNT(ccc_no)  FROM mike WHERE started_on_art = 't' ) AS curr_art_ca,
(SELECT COUNT(ccc_no)  FROM mike WHERE started_on_art  = 't' and sex='male' and last_visit_age > 14  ) AS curr_art_am,
(SELECT COUNT(ccc_no)  FROM mike WHERE started_on_art  = 't' and sex='male' and last_visit_age < 15  ) AS curr_art_cm,
(SELECT COUNT(ccc_no)  FROM mike WHERE started_on_art  = 't' and sex='female' and last_visit_age > 14  ) AS curr_art_af,
(SELECT COUNT(ccc_no)  FROM mike WHERE started_on_art  = 't' and sex='female' and last_visit_age < 15  ) AS curr_art_cf,
(SELECT COUNT(ccc_no)  FROM mike WHERE started_on_art  = 't' and  last_visit_age < 1  ) AS curr_art_ci
FROM mike
				
		 		
			";

			//////////////////////////////3.1 On Cotrimoxazole Prophylaxis

			$care = $this->db->query($sql)->row_array();
			  
			$data['ctx_ca'] = $care['ctx_ca'];
			$data['ctx_cf'] = $care['ctx_cf'];
			$data['ctx_cm'] = $care['ctx_cm'];
			$data['ctx_af'] = $care['ctx_af'];
			$data['ctx_am'] = $care['ctx_am'];
			
			
			////////////////////////////////////////// 3.3
    			
			
			$data['curr_care_ca'] = $care['curr_care_ca'];
			$data['curr_care_cf'] = $care['curr_care_cf'];
			$data['curr_care_cm'] = $care['curr_care_cm'];
			$data['curr_care_af'] = $care['curr_care_af'];
			$data['curr_care_am'] = $care['curr_care_am'];
			$data['curr_care_ci'] = $care['curr_care_ci'];

////////////////////////////////////////// 3.6
			
			$data['curr_art_ca'] = $care['curr_art_ca'];
			$data['curr_art_cf'] = $care['curr_art_cf'];
			$data['curr_art_cm'] = $care['curr_art_cm'];
			$data['curr_art_af'] = $care['curr_art_af'];
			$data['curr_art_am'] = $care['curr_art_am'];
			$data['curr_art_ci'] = $care['curr_art_ci'];

			$data['title'] = "MOH 731 Report";
			
						
			$data['content'] = $this->load->view('moh731_rep',$data, TRUE);
			$this->load->view('template',$data);	
			
		}
						
		
	}
	

	
}
