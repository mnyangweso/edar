<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {


 	function __construct()
	{
		parent::__construct();
		$this->pbscms->_check_logged();
		$this->form_validation->set_error_delimiters('<div class="ferror">', '</div>');
		//session_start();
	}
	
	
	public function index()
	{
		$this->form_validation->set_rules('facility_code','Master Facility List Name','trim|required');
		//$this->form_validation->set_rules('site_name','Facility Name','trim');
		
		
		if ($this->form_validation->run() == FALSE){ 

			$this->db->order_by(array('province,mfl,facility_name' ),'asc');
			$this->db->or_where('province' ,'Eastern');
			$this->db->or_where('province' ,'Nyanza');
			$data['query'] = $this->db->get('mfl');
				
			$data['title'] = "Admin Settings";
			$data['content'] = $this->load->view('admin',$data,TRUE);
			$this->load->view('template',$data);			
		}else{
	
		}
	}
        
        
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
