<link href="forms.css" rel="stylesheet" type="text/css" media="all">

<style>
/*
input:invalid {
  box-shadow: 0 0 5px 1px red;}

input:focus:invalid {
  outline: none;
}
{}
*/

.bb{font-weight:bold;}
</style>

<script language="javascript">
function myFunction(val) {
   // var v = getFullYear();
	//var y = v-val;
    alert(val);
}
</script>

<?php echo validation_errors(); ?>
<form method="post">

<table width="100%" frame="box" cellspacing="2" cellpadding="2" style="background-color:#FFFFFF">
  <tr  bgcolor="#c4c6d4">
    <td colspan="4" class="bb">Prevention With Positive</td>
  </tr>
  <tr class="odd">
    <td>CCC No</td>
    <td><input type="number" name="ccc_no" list="ccc" required>
	<datalist id="ccc">    </datalist>	</td>
    <td>Sex</td>
    <td><label>
      <select name="sex" id="sex"  required="required">
        <option value="">-- Select Sex --</option>
        <option value="male">Male</option>
        <option value="female">Female</option>
      </select>
    </label></td>
  </tr>
  <tr class="even">
    <td>Date Of Birth</td>
    <td>
      <input type="date" name="dob" required="required" />    </td>
    <td>Age</td>
    <td><input type="number" name="age" maxlength="3" min="0" max="140" onchange="myFunction(this.value)" /></td>
  </tr>
  <tr  bgcolor="#c4c6d4">
    <td colspan="4" class="bb">Appointment</td>
  </tr>
  <tr class="odd">
    <td>Visit Date</td>
    <td><label>
      <input type="date" name="visit_date" required="required" />
    </label></td>
    <td>Visit Type</td>
    <td><label>New
      <input name="visit_type" type="radio" value="n" required="required" />
      </label>
        <label>Revisit
          <input name="visit_type" type="radio" value="r" required="required" />
      </label></td>
  </tr>
  <tr class="even">
    <td>Scheduled Visit </td>
    <td><label>Yes
      <input name="scheduled_visit" type="radio" value="y"/>
      </label>
        <label>No
          <input name="scheduled_visit" type="radio" value="n"/>
      </label></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr  bgcolor="#c4c6d4">
    <td colspan="4" class="bb">Medication and Enrollment </td>
  </tr>
  <tr class="odd">
    <td>Prophylaxis</td>
    <td><label>None
      <input name="prophylaxis" type="radio" value=" " required="required" />
      </label>
        <label>CTX
          <input name="prophylaxis" type="radio" value="ctx" required="required" />
        </label>
        <label>Dapson
          <input name="prophylaxis" type="radio" value="dapson" required="required" />
        </label>    </td>
    <td>Client Type 
      <label></label><label></label></td>
    <td><label></label>
    <label>Pre-ART Client
        <input name="pre_art" type="radio" value="y" required="required" />
</label>
      <label>ART Client
      <input name="pre_art" type="radio" value="n" required="required" />
      </label></td>
  </tr>
  <tr class="even">
    <td>Enrolled In Care
      <label></label>
      <label>Today</label></td>
    <td><label></label><label>Yes
        <input name="enrolled_in_care" type="radio" value="y" required="required" />
</label>
      <label>No
      <input name="enrolled_in_care" type="radio" value="n" required="required" />
      </label></td>
    <td>Started On ART Today </td>
    <td><label></label><label></label><label>Yes
        <input name="started_on_art" type="radio" value="y" required="required" />
</label>
      <label>No
      <input name="started_on_art" type="radio" value="n" required="required" />
      </label></td>
  </tr>
  <tr  bgcolor="#c4c6d4">
    <td colspan="4" class="bb">TB Screening and Treatment </td>
  </tr>
  <tr class="odd">
    <td>Screened TB </td>
    <td><label>Yes
        <input name="tb_screening" type="radio" value="y" required>
    </label>
      <label>No
      <input name="tb_screening" type="radio" value="n" required>
      </label></td>
    <td>Started IPT </td>
    <td><label>Yes
        <input name="started_ipt" type="radio" value="y">
    </label>
      <label>No
      <input name="started_ipt" type="radio" value="n">
      </label></td>
  </tr>
  <tr  bgcolor="#c4c6d4">
    <td colspan="4" class="bb">Prevention With Positive</td>
  </tr>
  <tr class="odd">
    <td>PWP Modern Contraceptive </td>
    <td><label>Yes
        <input name="pwp_contraceptive" type="radio" value="y">
    </label>
      <label>No
      <input name="pwp_contraceptive" type="radio" value="n">
      </label></td>
    <td>PWP Provided With Condoms </td>
    <td><label>Yes
        <input name="pwp_condoms" type="radio" value="y">
    </label>
      <label>No
      <input name="pwp_condoms" type="radio" value="n">
      </label></td>
  </tr>
  <tr bgcolor="#c4c6d4">
    <td colspan="4" class="bb">Nutrition</td>
  </tr>
  <tr class="odd">
    <td>Nutrition accessed </td>
    <td><label>Yes
        <input name="nutrition_accessed" type="radio" value="y">
    </label>
      <label>No
      <input name="nutrition_accessed" type="radio" value="n">
      </label></td>
    <td>Nutrition Malnorished </td>
    <td><label>Yes
        <input name="nutrition_malnorished" type="radio" value="y">
    </label>
      <label>No
      <input name="nutrition_malnorished" type="radio" value="n">
      </label></td>
  </tr>
  <tr class="even">
    <td>Nutrition food by prescription </td>
    <td><label>Yes
        <input name="nutrition_food_prescribed" type="radio" value="y">
    </label>
      <label>No
      <input name="nutrition_food_prescribed" type="radio" value="n">
      </label></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr  bgcolor="#c4c6d4">
    <td colspan="4" class="bb">Cancer</td>
  </tr>
  <tr class="odd">
    <td>Screened for Cancer </td>
    <td><label>Yes
        <input name="cancer_screening" type="radio" value="y">
    </label>
      <label>No
      <input name="cancer_screening" type="radio" value="n">
      </label></td>
    <td>Pregnant</td>
    <td><label>Yes
        <input name="pregnant" type="radio" value="y" />
    </label>
      <label>No
      <input name="pregnant" type="radio" value="n" />
      </label></td>
  </tr>
  <tr  bgcolor="#c4c6d4">
    <td colspan="4" class="bb">Next <span class="even">Appointment</span> Date  </td>
  </tr>
  <tr class="odd">
    <td class="even">TCA</td>
    <td class="even"><input type="date" name="tca" required="required" /></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr class="">
    <td colspan="4"><label>
      <div align="center">
        <input type="submit" name="Submit" value="Save" />
        </div>
    </label></td>
    </tr>
</table>

</form>
