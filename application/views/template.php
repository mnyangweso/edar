<html>
<head>
<title>MOH - DAR</title>
<link href="<?=base_url()?>assets/icons/favicon.ico" rel="shortcut icon" type="image/x-png">
<link href="<?=base_url()?>assets/css/shrink.css" rel="stylesheet" type="text/css" media="all">

<link href="<?=base_url()?>assets/js/jquery-ui/jquery-ui.css" rel="stylesheet" type="text/css" media="screen" />
<link href="<?=base_url()?>assets/css/jquery_easyui/icon.css" rel="stylesheet" type="text/css" media="all">
<link href="<?=base_url()?>assets/css/jquery_easyui/easyui.css" rel="stylesheet" type="text/css" media="all">
<link href="<?=base_url()?>assets/css/jquery_easyui/linkbutton.css" rel="stylesheet" type="text/css" media="all">

<script type="text/javascript" src="<?=base_url()?>assets/js/jquery-2.1.0.js"></script>
<script src="<?=base_url()?>assets/js/jquery-ui/jquery-ui.js"></script> 
<script type="text/javascript" src="<?=base_url()?>assets/js/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/zebra_datepicker.js"></script>
<!-- <script type="text/javascript" src="<?=base_url()?>assets/js/admin.js"></script> -->
<script type="text/javascript" src="<?=base_url()?>assets/js/moment.js"></script>


 <meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content="pbs, cms, pbscms,  control panel,  cp,  admin">
<!-- <meta http-equiv="refresh" content="50"> -->
<!-- <link href="../../../../assets/css/admin.css" rel="stylesheet" type="text/css"> -->
	<?php if (isset($tiny_mce)):?>	
		<?= $tiny_mce;?>
	<?php endif; ?>


<style>
		#mnupanel.panel-body{
			background:#f0f0f0; margin:auto;
		}
		#mnupanel .panel-header{
			background:#fff url("<?=base_url()?>assets/images/panel_header_bg.gif") no-repeat top right; 
		}
		#mnupanel .panel-tool-collapse{
			background:url("<?=base_url()?>assets/images/arrow_up.gif") no-repeat 0px -3px; 
		}
		#mnupanel .panel-tool-expand{
			background:url("<?=base_url()?>assets/images/arrow_down.gif") no-repeat 0px -3px; 
		}

		.menupanel{width:96%;height:auto;padding:20px;margin:auto;}
/*
		.easyui-panel a:link, .easyui-panel a:visited, .easyui-panel a:hover, .easyui-panel a:active{
			text-transform:none; color:teal; font-weight:bold; text-decoration:none; display:block; font-size:15px;padding:10px;
			line-height: 25px; height:25px; width:80%; margin:auto;	
		}

	Tel No. 0733698007/ 0720883995
 
Email: mwa2ngih@yahoo.com
 
emwangi@pedaids.org

*/		

		
	
	.f-font {font-family:  "Univers LT 57 Condensed",Garamond,GlasgowSerial-Heavy,Eras-Bold,"Chianti BT", "Chianti XBd BT"; font-size:18px;}
	.style1 {font-family: }

	.mnbtn{font-size:22px; width:200px;}

	ul.horizontal{
	margin:0;
	padding:0;
	}

	ul.horizontal li{
	display:block;
	width: 80%;
	float:left;
	margin:9px;
	height:20px;
	line-height:20px;
	font-weight:bold;
	font-size:11px;
	padding:0;
	color:teal;
	text-transform: uppercase;
	}

	.lin{
	display:block;
	width: 98%;
	float:left;
	margin:2px;
	height:20px;
	line-height:20px;
	font-weight:bold;
	font-size:12px;
	
	color:teal;
	text-transform: uppercase;
	background: transparent;
	}
	
	.lin > a, .lin > a:active, .lin > a:visited, .lin > a:hover {
		font-weight:bold; font-size:12px; color:teal; font-style:normal; text-transform:uppercase; text-decoration:none;
	}

	
</style>




</head>



<body class="easyui-layout" style="padding:0; margin:0;">
	<div data-options="region:'north',border:true, collapsible:true" style=" 
	margin:0; padding:0; 
	overflow:hidden;
   //background: transparent;
  
  background: -webkit-linear-gradient(#51bfd8,#30a5c8, #0a9fbf, #30a5c8, #51bfd8);
 //background:url(<?=base_url()?>assets/images/admin/icapbg.png) repeat-x;
  border: 1px solid #149fbe;
  transition: all 0.3s ease-out;
  box-shadow:
    inset 0 2px 3px rgba(255,255,255,0.5),
    0 0 1px rgba(0,0,0,0.3),
    0 0 4px 1px rgba(0,0,0,0.2);

">
			<div style="width:100%; height:60px; padding:5px 0px; margin:0; display: flex;  justify-content: space-between;">
				<img src="<?=base_url()?>assets/images/logoss/icap-white.png">	
				<img src="<?=base_url()?>assets/images/logoss/moh-logo-kenya.png">
				<img src="<?=base_url()?>assets/images/logoss/e-dar.png">	
				<img src="<?=base_url()?>assets/images/logoss/cdc-kenya.png">	
				<img src="<?=base_url()?>assets/images/logoss/pepfar-kenya.png">			  
			</div>
			<div class="menubar" style="width:100%; height:30px;" >
				<table border="0" cellspacing="2" cellpadding="2"  width="100%" style="color: #fff;font-weight: bold;">
				<td> <a href="#" class="easyui-menubutton mnbtn" data-options="menu:'#mm1',iconCls:'icon-edit'">Data Entry</a> </td>
				<td> <a href="#" class="easyui-menubutton mnbtn" data-options="menu:'#mm2',iconCls:'icon-large-smartart'">Reports</a> </td>
				<td> <a href="#" class="easyui-menubutton mnbtn" data-options="menu:'#mm3',iconCls:'icon-system'" style="font-size:18px !important">System</a> </td>
			</table>
			</div>

</div>

		</div>
	</div>


	<div id="mm1" style="width:260px;">		
		<div data-options="iconCls:'icon-edit'"><a href="<?=base_url()?>">Data Entry</a></div>
		<div data-options="iconCls:'icon-edit'"><a href="<?=base_url()?>entries/show">Edit Entries</a></div>
		<div data-options="iconCls:'icon-edit'"><a href="<?=base_url()?>entries/search">Search Entries</a></div>
	</div>
	
	<div id="mm2" style="width:260px;">
		<div data-options="iconCls:'icon-edit'"><a href="#"> <a href="<?=base_url()?>reports/moh366"> MOH - DAR (2010)</a></div>
		<div data-options="iconCls:'icon-edit'"> <a href="<?=base_url()?>reports/moh366_new"> MOH - DAR (2015) </a> </div>
		<div data-options="iconCls:'icon-edit'"> <a href="<?=base_url()?>reports/moh731"> MOH - 731 (2010) </a> </div>
		<div data-options="iconCls:'icon-edit'"> <a href="<?=base_url()?>reports/moh731_new">MOH - 731 (2015) </a> </div>
		<div class="menu-sep"></div>
		<div data-options="iconCls:'icon-edit'"> <a href="<?=base_url()?>reports/act">MOH - ACT </a> </div>
		<div data-options="iconCls:'icon-edit'"> <a href="<?=base_url()?>reports/linelist">PATIENT INFORMATION  </a> </div>
	</div>

	<div id="mm3" style="width:260px;">
		<div data-options="iconCls:'icon-edit'"> <a href="<?=base_url()?>auth/change_password"> Change Password </a> </div>
		<div data-options="iconCls:'icon-edit'"> <a href="<?=base_url()?>admin/users"> Manage Users </a> </div>
		<div data-options="iconCls:'icon-edit'"> <a href="<?=base_url()?>admin/su"> Facility Details </a> </div>
		<div data-options="iconCls:'icon-edit'"> <a href="<?=base_url()?>auth/logout"> Log Out  </a> </div>
	</div>

	
		<div id="mnupanel" data-options="region:'west',title:'Menu Options',split:true,maxWidth:200,collapsed:true" style="width:200px;margin:auto;padding:0;background-color:#7190E0; text-align:center; ">
			<br/>
			<div class="easyui-panel" title="Data Entry" collapsible="true" collapsed="false">
					<div class="lin"><a href="<?=base_url()?>">Data Entry </a></div>
					<div class="lin"><a href="<?=base_url()?>entries/show">EDIT  ENTRIES </a></div>
					<div class="lin"><a href="<?=base_url()?>entries/search">SEARCH  ENTRIES </a></div>
			</div>
						
			<br/>
			<div class="easyui-panel" title="Reports" collapsible="true" collapsed="false">
					<div class="lin"> <a href="<?=base_url()?>reports/moh366">MOH-DAR (2010)</a></div>
					<div class="lin"> <a href="<?=base_url()?>reports/moh366_new">MOH-DAR (2015)</a></div>
					<div class="lin"> <a href="<?=base_url()?>reports/moh731">MOH-731 (2010)</a></div>
					<div class="lin"> <a href="<?=base_url()?>reports/moh731_new">MOH-731 (2015))</a></div>	
					<div class="lin"> <a href="<?=base_url()?>reports/act">ACT </a> </div>	
					<div class="lin"> <a href="<?=base_url()?>reports/appointments">APOINTMENTS </a></div>
					<!--<div class="lin"> <a href="<?=base_url()?>reports/linelist">PATIENT INFORMATION  </a> </div> -->	
			</div>
			
	
		</div>

		

		<!----------------------------------------------------------------------------------------------------->
		<div data-options="region:'center',title:'<?=$title?>',iconCls:'icon-ok'" style="margin:0;padding:0;background:#e7f3f8"> 
			
			<div id="controls-container">
	

	<div id="content" style="width:100%; height:auto; margin:auto; clear:both; overflow:hidden !important; position:relative; font-size:10px; ">
	<!--content start -->
	<form method="post" id="mike" >
	<br/>
	<?php if ($this->session->flashdata('flash')):?>
		<div id="flash" class="<?=$this->session->flashdata('flashtype');?>" title="click if u want to close this message">
			<span> </span> <?=$this->session->flashdata('flashmessage');?>
		</div>
	<?php endif; ?>
	<br/>
	<?=$content?>
			
	<!-- <p class="footer"> Page rendered in <strong>{elapsed_time}</strong>  Memory used  is <strong> {memory_usage}  </strong>seconds</p>-->
	</form>
	<!-- content end -->
	
	</div>
		
	</div>
<!-- end controls-container-->
		
	
		</div>
		<!----------------------------------------------------------------------------------------------------->

	<div data-options="region:'south',split:false" style="height:30px;background-color:#016596;
border: 1px solid rgba(0,0,0,0.1);
  box-shadow: 
    inset 0 2px 3px rgba(255,255,255,0.3),
    inset 0 -2px 3px rgba(0,0,0,0.3),
    0 1px 1px rgba(255,255,255,0.9);
	
 width:99.8%; display: table;
 min-height:30px; max-height:30px;
font-weight:bold; color:#fff; font-style:'GlasgowSerial-Heavy'" class="f-font">
<div style="width:auto; display:table-cell;line-height:30px;text-align:left;">Copyright 2016 &copy; ICAP Kenya. All rights reserved</div> 
<div style="width:auto; display:table-cell;line-height:30px;text-align:center;"> E-DAR Version 0.2</div>
<div style="width:auto; display:table-cell;line-height:30px;text-align:right;"> E-DAR Version 0.2</div>

</div>  

</body>
</html>

<script language="javascript">

// JavaScript Document




$(document).ready(function() {

		
      

	
	 	
    		$('#dob').Zebra_DatePicker({
  		format: 'd-m-Y', direction: -1,
		onSelect:function(str1,str2,date,$el){
		document.getElementById('age').value="";
		//document.getElementById('age').disabled=true;
		$('#age').prop('readonly', true);
		 
		}
		});
		
		
		
		$('#visit_date').Zebra_DatePicker({
  			format: 'd-m-Y',direction: -1,
				onSelect:function(s1,s2,date,$el){
					var ag = document.getElementById('age').value;
					var n=document.getElementById('dob').value;
	///////////////////////////////////////////////////////////////////////////////////////
	
					if(n.length < 1 && document.getElementById("age").disabled == true ){
						alert('Please pick a date in the Date of Birth field');
						var zdp = $('#visit_date').data('Zebra_DatePicker');
						zdp.clear_date();
						window.setTimeout(function () { 
							document.getElementById('dob').focus(); 
						}, 0);
						return false;											
					}
					
					
					if(ag.length < 1 && document.getElementById("dob").disabled == true ){
						alert('Please fill the age of the patient in the age field');
						var zdp = $('#visit_date').data('Zebra_DatePicker');
						zdp.clear_date();
						window.setTimeout(function () { 
							document.getElementById('age').focus(); 
						}, 0);
						return false;											
					}
					
					if(ag.length < 1 && n.length < 1 && document.getElementById("dob").disabled == false && document.getElementById("dob").disabled == false ){
							alert('Please pick a date in the Date of Birth  or The Age field');
						var zdp = $('#visit_date').data('Zebra_DatePicker');
						zdp.clear_date();
						window.setTimeout(function () { 
							document.getElementById('dob').focus(); 
						}, 0);
						return false;
					}

					
					if( n.length > 1 && document.getElementById("dob").disabled == false && s1.length > 1 ){
						var zdp = $('#visit_date').data('Zebra_DatePicker');
						var date1 = document.getElementById('dob').value;
		
						var vd = moment(s1, ["DD-MM-YYYY"]); 
						var dd = moment(date1, ["DD-MM-YYYY"]);
						//alert("moment:"+ vd.diff(dd, 'days') < 0);
						if(vd.diff(dd, 'days') < 0 ){
							alert('Date of Birth must come earlier than the visit date');
							zdp.clear_date();
							window.setTimeout(function () { 
								document.getElementById('dob').focus(); 
							}, 0);	
			
							return false;
						}

					if(n.length > 1 && s1.length > 1 && ag.length < 1 ){
						var zdp = $('#visit_date').data('Zebra_DatePicker');
						var date1 = document.getElementById('dob').value;
		
						var vd = moment(s1, ["DD-MM-YYYY"]); 
						var dd = moment(date1, ["DD-MM-YYYY"]);
				
						var mag = vd.diff(dd, 'years');
					
						if( mag > -1 ){

						$('#age').val(mag);
						getElementById('dob').value=mag;
						//alert('moment :'+mag);
						return true;

						}else return false;

										
					}

						var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
						var zx = Math.floor( diffDays/366);
						if(isNaN(zx))
							zx = 0;	
						$('#age').val(zx);
						$('#age').prop('readonly', true);
						return true;					
						
					}
	
	//////////////////////////////////////////////////////////////////////////////////////
					
					var parts = s1.split("-");					
					var dob = '15-06'+(ag-parts[2]);
					//alert('xxxxx'+ag);
					//alert('kkkkkk'+$("#dob").attr('readonly')); 		
					//alert('kkkkkkxxxxxxx'+document.getElementById("dob").readOnly);
					document.getElementById('dob').removeAttribute("disabled");
					document.getElementById('dob').setAttribute("disabled", false);	
					document.getElementById('dob').value=dob;
					$('#dob').removeAttr('disabled');
					$('#dob').prop( 'disabled', false );
					$('#dob').prop('readonly', true);
					//$('#dob').removeClass( 'req');
							
				}
		});
		
		$('#tca').Zebra_DatePicker({
  		format: 'd-m-Y',
		onSelect:function(s1,s2,date,$el){
			var zdp = $('#tca').data('Zebra_DatePicker');
			var date1 = document.getElementById('visit_date').value;
									
				var td = moment(s1, ["DD-MM-YYYY"]); 
				var vd = moment(date1, ["DD-MM-YYYY"]);
				
				if(td.diff(vd, 'days') < 0 ){
					alert('Visit Date must come  earlier than the Next Appointment Date');
					zdp.clear_date();
					window.setTimeout(function () { 
					document.getElementById('tca').focus(); 
				}, 0);	
				return false;
			}

						
			return true;	
			
		}
		});

		

		/*
		$(function(){
		var p = $('body').layout('panel','west').panel({
		onCollapse:function(){
			var title = $('body').layout('panel','west').panel('options').title;  // get the west panel title
			var p = $('body').data('layout').panels['expandWest'];  // the west expand panel
			p.html('<div style="-moz-transform: rotate(90deg);padding:6px 2px;-ms-transform: rotate(90deg);-webkit-transform: rotate			(90deg); font-size:13px; font-weight:bold;">'+title+'</div>');
			}
			});
		});
		*/

		/*
		$(function(){
			var p = $('body').layout('panel','west').panel({
				onCollapse:function(){    
			    		localStorage.setItem('wstate', 'false');                             
				}
	    		});
		});
		*/
		/*

		$(function(){

			
			//var panel = $('body').layout('panel', 'west');    // get the west panel
			var myLayout = $('body').layout();
			panel.panel({
			    onBeforeCollapse:function(){
				// return false will stop the collapse action
				alert('I have been expanded before');
			    },

			    onCollapse:function(){
				alert('after west panel collapsed');
			    },
		
			    onResize: function(height,width) {
			    	alert('resized');
			    }
			});
		});
		*/
		/*
		$(document).ready(function() {
		    var panel = $('#main-layout').layout('panel', 'south'),
			hsize = localStorage.getItem('SouthPanel_h');

		    panel.panel({
			height: hsize > 0 ? hsize : 50,
			onResize: function(height,width) {
			    localStorage.setItem('SouthPanel_h', height);
			}
		    });
		});
	*/

 });
 
 
function myFunction(val) {
	document.getElementById('dob').value="";
	document.getElementById('dob').disabled=true;
	$("#dob").css({ "cursor": "not-allowed" }) ;  
}

function femaleCheck(val){
	if(val=='male'){
		
		$("[name=pregnant][value=n]").prop('checked', true);
		$("[name=cancer_screening][value=n]").prop('checked', true);
	}
	
}




$(function () {
    $('#ccc_no').change(function () {
	        $.ajax({
            type: 'POST',
            url: "<?php echo base_url().'entries/entries/myajax/'?>"+$(this).val(),
            data: {
                ccc_no: $(this).val()
            },
            dataType: 'json',
            success: function (data) //on recieve of reply
            {	
			   if (data.error == "true") 
			   {
			   alert("An error occurred: " & result.errorMessage);
			   }
				var obj = data;
				
		 $('#dob').val(obj[0].dob);
		 $('#sex').val(obj[0].sex);
		 
		 var pre = obj[0].pre_art;
		 
			 if(pre == 't')
			 	$("[name=pre_art][value=y]").prop('checked', true);
			 else
			  	$("[name=pre_art][value=n]").prop('checked', true);
            },
		error: function (xhr, ajaxOptions, thrownError) {
		//alert(xhr.status);
		//alert(thrownError);
	      }
        });
    });  

}); 

function pgMale(val){
	if($("#sex option:selected").val()=='male'){
	alert('Cant have a pregnant male');
	$("[name=pregnant][value=n]").prop('checked', true);
	return false;
	}
}

function preARTcheck(){
$('[name=pre_art][value=n]').prop('checked', true);
}
</script>

