<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|MY CONFIG
|
*/
$config['author'] = 'Michael Adambo Nyangweso';
$config['company'] = 'ICAP Kenya';
$config['version'] = 1;
$config['major_version'] = 0;
$config['minor_version'] = 0;

//$config['province'] = 'Coast';
//$config['province'] = 'Central';
//$config['province'] = 'North Eastern';
$config['province'] = 'Nyanza';
//$config['province'] = 'Nairobi';
//$config['province'] = 'Eastern';
//$config['province'] = 'Western';
//$config['province'] = 'Rift Valley';
