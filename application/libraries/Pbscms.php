<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pbscms {
	
	function Pbscms()
	{	
		$this->ci =& get_instance();			
		log_message('debug', "Pbs Class Initialized");
	}
	
	function _check_logged(){
        if(!$this->ci->session->userdata('logged')=="yes"){
			$this->ci->session->set_flashdata('flash', 'true');
			$this->ci->session->set_flashdata('flashtype', 'warning');
			$this->ci->session->set_flashdata('flashmessage', 'You have to login first to access some pages');
			redirect('auth');			
		}
	}//end function _check_logged
	
	
	function _check_rlogged(){
        if( $this->ci->session->userdata('logged') != "yes" ){
			$this->ci->session->set_flashdata('flash', 'true');
			$this->ci->session->set_flashdata('flashtype', 'warning');
			$this->ci->session->set_flashdata('flashmessage', 'You have to login first to access some pages');
			redirect('auth');			
		}
		
		if($this->ci->session->userdata('role_id') > 2){
			$this->ci->session->set_flashdata('flash', 'true');
			$this->ci->session->set_flashdata('flashtype', 'warning');
			$this->ci->session->set_flashdata('flashmessage', 'You have to login as administrator to access somee of the resource');
			redirect('auth');			
		}
		
		
	}//e
	
	function _islogged(){
        if($this->ci->session->userdata('logged')=="yes")
        	return 'true';
        else
        	return 'false';
	}//end function _islogged
	
	function _get_user_role_id(){		
       return $this->ci->session->userdata('role_id');
	}//end function user_role_id

	function _status($pid){
		$this->ci->db->order_by('fup_status_date','desc');
		$this->ci->db->limit(1);
		$status = $this->ci->db->get_where('tbllost_to_followup',array('patient_id'=> $pid ))->row();
		if(sizeof($status)==0){
			$ss = "select next_visit_date,(SELECT DATE_PART('day',NOW() - next_visit_date::timestamp) AS days_past) from tblvisit_information where patient_id=$pid order by next_visit_date desc limit 1";
			
			$ssv_num = $this->ci->db->query($ss)->num_rows();
			if($ssv_num==0)
				return "No Visit";
			
			$ssv = $this->ci->db->query($ss)->row()->days_past;
			
			if($ssv > 0 && $ssv < 90)
				return "Defaulter";
			else if($ssv > 90)
				return "Lost To Follow Up";
			else
				return "In Care";
		}else{
			//print_r($status); die();
			if($status->fup_status==1)
				return "Transfer Out";
			if($status->fup_status==2)
				return "Dead";
		}		
	}

	function _vstatus($pid){
		$this->ci->db->order_by('fup_status_date','desc');
		$this->ci->db->limit(1);
		$status = $this->ci->db->get_where('tbllost_to_followup',array('patient_id'=> $pid,'isactive' => 'false' ))->row();
		
		
			print_r($status); die();
			if($status->fup_status==1)
				return "Transfer Out";
			if($status->fup_status==2)
				return "Dead";
				
	}

	/**********************************************************************
					 menus 
	**********************************************************************/
	function _menAdminOnly(){
		if( $this->ci->session->userdata('role_id') <= 2 && $this->ci->session->userdata('logged') != "yes" ){
			$this->ci->session->set_flashdata('flash', 'true');
			$this->ci->session->set_flashdata('flashtype', 'warning');
			$this->ci->session->set_flashdata('flashmessage', 'You have to login first to access some pages');
			redirect($_SERVER['HTTP_REFERER']);			
		}
	}


	function _ishod(){	

	if($this->ci->session->userdata('role_id') == 2 || $this->ci->session->userdata('role_id')== 4 )
		   {
				$this->ci->session->set_flashdata('flash', 'true');
				$this->ci->session->set_flashdata('flashtype', 'warning');
				$this->ci->session->set_flashdata('flashmessage', 'You have to be an admin or hod to access this page');
				redirect($_SERVER['HTTP_REFERER']);
		   }

		
	}//end function hod
	
	function _ishod_booked(){	

	if($this->ci->session->userdata('role_id')== 4 )
		   {
				$this->ci->session->set_flashdata('flash', 'true');
				$this->ci->session->set_flashdata('flashtype', 'warning');
				$this->ci->session->set_flashdata('flashmessage', 'You have to be an admin or hod to access this page');
				redirect($_SERVER['HTTP_REFERER']);
		   }

		
	}//end function hod

	function _check_permissions()
	{
			if(!$this->ci->session->userdata('logged')=="yes"){
			$this->ci->session->set_flashdata('flash', 'true');
			$this->ci->session->set_flashdata('flashtype', 'warning');
			$this->ci->session->set_flashdata('flashmessage', 'You have to login first to access some pages');
			redirect('users/login', 'refresh');
			}
			
			
			if($this->ci->session->userdata('role_id') != 1 ){
			$this->ci->session->set_flashdata('flash', 'true');
			$this->ci->session->set_flashdata('flashtype', 'warning');
			$this->ci->session->set_flashdata('flashmessage', 'You dont have enough priveledges to go to this section.');			
			redirect('tasks/add');
			}
			
			
			
	}
	
	function _get_username($varid)
	{
		$arr = array();
		$sql = "username from users where id = ".$varid." and isactive = true";
		$this->ci->db->select($sql);
		$query = $this->ci->db->get();
		$rowdata = $query->row_array();
		$username =  $rowdata['username'];
		return $username;		
	}

	function _retvalue($varid)
	{
		if($varid == 'f' or $varid == '0')
			return 'No';	
		else
			return 'Yes';	
	}
	
	
	function _divzero($a,$b)
	{
		if($b==0)
			return 0;	
		else
			return round( ( ($a/$b) * 100 ), 0);	
	}


	function _trigger_sum($st,$et,$rt,$cat){
		
		$sql = "SELECT (cast('".$et."' as timestamp)-cast('".$st."' as timestamp)) as ts";
		$query = $this->ci->db->query($sql);
		$ts = $query->row(0);

		if($rt=='rpd'){
		$query = $this->ci->db->query("select extract(day from interval '".$ts->ts."')as day");
		$day = $query->row(0);
		$day = $day->day;
		
		$query = $this->ci->db->query("select rpd from categories where id='".$cat."'");
		$amount = $query->row(0);
		$amount = $amount->rpd;

		return $day*$amount;
		}
	
	}
	
	




	function _retfield($tbl,$col,$val){
		if($val== null)
			return 'none';

		$qstr = "select ".$col." from ".$tbl." where id = '".$val."'";
		$query = $this->ci->db->query($qstr);
		//$query = $this->ci->db->get_where( $tbl,array('id' => $val) );
		$row = $query->row();
		$newid = $row->$col;
		if($newid != '' || $newid != null )
		return $newid;
		else
		return ' ';

	}
	
	function _retfield2($tbl,$col,$val,$rcol){
		if($val== null)
			return '';

		$qstr = "select ".$col." from ".$tbl." where ".$rcol." = '".$val."'";
		$query = $this->ci->db->query($qstr);
		//$query = $this->ci->db->get_where( $tbl,array('id' => $val) );
		$row = $query->row();
		$newid = $row->$col;
		if($newid != '' || $newid != null )
		return $newid;
		else
		return ' ';

	}

	function getid($table,$column,$column_value)	
	{
		$query = $this->ci->db->get_where( $table,array($column => $column_value) );
		$row = $query->row();
		$newid = $row->id;
		return $newid;
	}
	
	function _is_validDate($day, $month, $year)
    {
        if($year < 0 || $year > 9999) {
            return false;
        }
        if(!checkdate($month,$day,$year)) {
            return false;
        }
        return true;
    } 
    
    function _time_day_diff($start_var,$end_var,$t)
    {
    			$str_start = strtotime($start_var); // The start date becomes a timestamp 
                $str_end = strtotime($end_var); // The end date becomes a timestamp 
                $nseconds = $str_end - $str_start; // Number of seconds between the two dates 
                $nminutes = ($nseconds / 60);
                $nhours = ($nseconds / 3600);
                $ndays = round($nseconds / 86400); // One day has 86400 seconds 
                //$nseconds = $nseconds % 86400; // The remainder from the operation 
                
                if($t=='sec')
                	return $nseconds;
                if($t=='min')
                	return $nminutes;
                if($t=='hrs')
                	return $nhours;
                if($t=='day')
                	return $ndays;        
    }
    
	
	function _str_indent($string,$chars=4,$char=" ")
	{
		return preg_replace('!^!m',str_repeat($char,$chars),$string);
	}
	
	function _str_nl2br($string)
	{
		return nl2br($string);
	}
	
	function _str_spacify($string, $spacify_char = ' ')
	{
		return implode($spacify_char,
		               preg_split('//', $string, -1, PREG_SPLIT_NO_EMPTY));
	}

	/********************************************************************************************************/

	function _export($ser_data,$ftype,$filename,$landscape,$papersize)
	{
		$ppp = unserialize( gzinflate( base64_decode ( $ser_data ) ) );
						
		if($ftype== 'pdf'){
			$mypdf = pdf_create($ppp, "",false,$papersize,$papersize);
	     		force_download($filename.".pdf", $mypdf);
		}

		if($ftype== 'excel'){					
			$filename = "$filename.xls";			 
			header("Content-type: application/octet-stream");
			header("Content-Disposition: attachment; filename=$filename");
			header("Pragma: no-cache");
			header("Expires: 0");
			force_download($filename, $ppp);
     		}
	}


}//end of class
