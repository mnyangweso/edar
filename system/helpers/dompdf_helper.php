<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

 function __construct()
   {
        $CI = & get_instance();
        log_message('Debug', 'dompdf class is loaded.');
        ini_set("memory_limit","64M");
    }
 

function pdf_create($html, $filename='', $stream=TRUE, $lanpot="landscape",$papersize="a4") 
{
	require_once(BASEPATH."helpers/dompdf/dompdf_config.inc.php"); 
//  require_once("dompdf/dompdf_config.inc.php");
	
	$dompdf = new DOMPDF();
	$dompdf->set_paper($papersize, $lanpot); 
	$dompdf->load_html($html);
	$dompdf->render();
	if ($stream) {
		$dompdf->stream($filename.".pdf");
	}else
	 return $dompdf->output();
	//write_file("/tmp/$filename.pdf", $dompdf->output());
}
?>
